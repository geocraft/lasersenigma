package eu.lasersenigma.items;

import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LasersColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * The items used in this plugin
 */
public enum Item {
    EMPTY(ItemType.BLOCK, null, true, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "AIR");
        }
    }),
    /**
     * ************************** MAIN SHORTCUTBAR ***************************
     */
    MAIN_SHORTCUTBAR_PLACE_COMPONENT(ItemType.HEAD, "place_component", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "18ee67d7a5b675b57d1b7c7ae79280d2c19828e582f07339cfb3ed9b90ff1236");
        }
    }),
    MAIN_SHORTCUTBAR_AREA_CONFIG(ItemType.HEAD, "area_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d8dae062b58056e5d6b7c3b121f6c788957f20adefee622d8cf4012dcc83865d");
        }
    }),
    MAIN_SHORTCUTBAR_STATS_MENU(ItemType.HEAD, "stats_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "532a5838fd9cd4c977f15071d6997ff5c7f956074a2da571a19ccefb03c57");
        }
    }),
    MAIN_SHORTCUTBAR_CREATE_AREA(ItemType.HEAD, "create_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8e88ead488ff68d35b61910c927a890ebc2c7605d76a1d13b5edb715e68a2cf3");
        }
    }),
    MAIN_SHORTCUTBAR_DELETE_AREA(ItemType.HEAD, "delete_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "4027ad3fefdcc69be5cea8db596f6cfc963d1af7f9007d62bbf569f522ee77d9");
        }
    }),
    MAIN_SHORTCUTBAR_ARMOR_MENU(ItemType.BLOCK, "armor_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "DIAMOND_CHESTPLATE");
        }
    }),
    MAIN_SHORTCUTBAR_CLOSE(ItemType.HEAD, "close_edition_mode", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    //COMPONENTS Laser Manipulation
    LASER_SENDER(ItemType.HEAD, "laser_sender", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "e1284368eb8649e980bed6c66565dea312d262cf5a271935de24dc361337d9de");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.LASER_SENDER);
        }
    }),
    LASER_RECEIVER(ItemType.HEAD, "laser_receiver", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "76da199506013166eed92be1aabf2b12b6a2be525f7e6b8f4bf0650404774564");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.LASER_RECEIVER);
        }
    }),
    MIRROR_SUPPORT(ItemType.HEAD, "mirror_support", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52baeb4a35da8a85d14bdccf7184f5545088f954da55144f235c2983fdb8e05b");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MIRROR_SUPPORT);
        }
    }),
    PRISM(ItemType.HEAD, "prism", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "707ce43734cbefbb3da03f9ac1f01a3df456cee231550bd2dd42585594f9");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.PRISM);
        }
    }),
    CONCENTRATOR(ItemType.HEAD, "concentrator", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6973f5f3b602437ef784d4c6a1752e194beb1b1737b11a33e27488d54de6a");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.CONCENTRATOR);
        }
    }),
    REFLECTING_SPHERE(ItemType.HEAD, "reflecting_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d8c53bce8ae58dc692493481909b70e11ab7e9422d9d87635123d076c7133e");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.REFLECTING_SPHERE);
        }
    }),
    FILTERING_SPHERE(ItemType.HEAD, "filtering_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3e1e5c81fb9d64b74996fd171489deadbb8cb772d52cf8b566e3bc102301044");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.FILTERING_SPHERE);
        }
    }),
    ATTRACTION_SPHERE(ItemType.HEAD, "attraction_repulsion_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "89375d805291bac7d045c5c0448ab7f5599fa6d9a8caeae4bd251d586e2c8");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.ATTRACTION_REPULSION_SPHERE);
        }
    }),
    WHITE_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    //COMPONENT Detection items
    REDSTONE_SENSOR_ACTIVATED(ItemType.HEAD, "redstone_sensor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfd12f99a72bbbd19b37e1f328d9e09718066159374b6a1414b8698ce37032db");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.REDSTONE_SENSOR);
        }
    }),
    REDSTONE_WINNER_BLOCK(ItemType.BLOCK, "winner_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "REDSTONE_BLOCK");
            put(ItemAttribute.ENCHANT, true);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.REDSTONE_WINNER_BLOCK);
        }
    }),
    DISAPPEARING_WINNER_BLOCK(ItemType.BLOCK, "disappearing_winner_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "OBSIDIAN");
            put(ItemAttribute.ENCHANT, true);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.DISAPPEARING_WINNER_BLOCK);
        }
    }),
    APPEARING_WINNER_BLOCK(ItemType.BLOCK, "appearing_winner_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "BARRIER");
            put(ItemAttribute.ENCHANT, true);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.APPEARING_WINNER_BLOCK);
        }
    }),
    MUSIC_BLOCK(ItemType.HEAD, "music_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52c3b21ad36723d0aa3b75a239d427b34e1ac24c57c6a515c703ae47ce48485");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MUSIC_BLOCK);
        }
    }),
    LOCK_CLOSED(ItemType.HEAD, "lock", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8f22b97797672c2a723910f8fd03ea4e05723d956d61931c36ca546510705674");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.LOCK);
        }
    }),
    ELEVATOR(ItemType.HEAD, "elevator", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2f6557ef29e13481ef0e977720657adb241505601414a32c4d276a3a017e750");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.ELEVATOR);
        }
    }),
    ELEVATOR_SECOND_LOCATION(ItemType.HEAD, "elevator_second_location", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2f6557ef29e13481ef0e977720657adb241505601414a32c4d276a3a017e750");
        }
    }),
    CALL_BUTTON(ItemType.HEAD, "elevator_call_button", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9c928553e6780294c307cc4a16d32cc07b57ec57c7ffe3d2358a530f13441ce9");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.CALL_BUTTON);
        }
    }),
    //Mirrors
    MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.LIGHT_BLUE);
        }
    }),
    BLACK_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    MIRROR_CHEST(ItemType.BLOCK, "mirror_chest", false, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "TRAPPED_CHEST");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MIRROR_CHEST);
        }
    }),
    //MISC Reflecting sphere
    REFLECTING_SPHERE_WHITE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ce236e3de8c164d2dff1c4a4ee38ddd6f7f0c0b0472e6abe14213429896a34e7");
        }
    }),
    REFLECTING_SPHERE_RED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "884e92487c6749995b79737b8a9eb4c43954797a6dd6cd9b4efce17cf475846");
        }
    }),
    REFLECTING_SPHERE_GREEN(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "83a2768de2af52b27c7bc1416dd535197b65158422673da9d3b0e61d49f33662");
        }
    }),
    REFLECTING_SPHERE_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8c8a824b9b94939af2e8ce188482618dad933ae5dd671a7b9ac999d3a3b61a5");
        }
    }),
    REFLECTING_SPHERE_YELLOW(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "211ab3a1132c9d1ef835ea81d972ed9b5cd8ddff0a07c55a749bcfcf8df5");
        }
    }),
    REFLECTING_SPHERE_MAGENTA(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "70cd83be2b45225497325d958543465fd56bd65cd94a97e8fdfb1c1ec93b08f");
        }
    }),
    REFLECTING_SPHERE_LIGHT_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "126e346287a21dbfca5b58c142d8d5712bdc84f5b75d4314ed2a83b222effa");
        }
    }),
    //MISC Filtering sphere
    FILTERING_SPHERE_WHITE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "caf039bec1fc1fb75196092b26e631f37a87dff143fc18297798d47c5eaaf");
        }
    }),
    FILTERING_SPHERE_RED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "533a5bfc8a2a3a152d646a5bea694a425ab79db694b214f156c37c7183aa");
        }
    }),
    FILTERING_SPHERE_GREEN(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c607cffcd7864ff27c78b29a2c5955131a677bef6371f88988d3dcc37ef8873");
        }
    }),
    FILTERING_SPHERE_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d1137b9bf435c4b6b88faeaf2e41d8fd04e1d9663d6f63ed3c68cc16fc724");
        }
    }),
    FILTERING_SPHERE_YELLOW(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "41139b3ef2e4c44a4c983f114cbe948d8ab5d4f879a5c665bb820e7386ac2f");
        }
    }),
    FILTERING_SPHERE_MAGENTA(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3dd0115e7d84b11b67a4c6176521a0e79d8ba7628587ae38159e828852bfd");
        }
    }),
    FILTERING_SPHERE_LIGHT_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cb9b2a4d59781d1bec2d8278f23985e749c881b72d7876c979e71fda5bd3c");
        }
    }),
    //MISC Repulsion sphere
    REPULSION_SPHERE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5bfa6eab81e21bdbfbb987ed695a4596ed33e77510908dadf696912f9423f75");
        }
    }),
    //MISC Music block
    MUSIC_BLOCK_LOOP_N_KEEP_PLAYING_ON_EXIT(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3be46fe5bd5c9a55836b3cecac430b99c77929f1d6ca81907d343d6ee297");
        }
    }),
    MUSIC_BLOCK_KEEP_PLAYING_ON_EXIT(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "607a6971d0cba23e409ec5283186e3b29982dc5913174c37afd916022ee6b");
        }
    }),
    MUSIC_BLOCK_LOOP(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dcc1de7b06fd5ebf4dae636abf04b1b1f28a58c176e6ce4f177a8a2c512c4d");
        }
    }),
    //MISC Mirror Support
    MIRROR_SUPPORT_BLOCKED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8eff7a46d3b15eebd7106a6b7f91c78718c7886b0abd3cd0505892499ca37daf");
        }
    }),
    //MISC Laser Sender
    LASER_SENDER_CONDITIONNAL_REDSTONE_ACTIVATED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "18ee67d7a5b675b57d1b7c7ae79280d2c19828e582f07339cfb3ed9b90ff1236");
        }
    }),
    LASER_SENDER_CONDITIONNAL_REDSTONE_DEACTIVATED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a8e6f84638516f3fc2648d527336c795362a5f7914bc5db5d41365dd2655e018");
        }
    }),
    LASER_SENDER_CONDITIONNAL_RANGE_ACTIVATED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "85cccd949da6b6c2d6f02844381a4114938a9ce4d8f633e6415ee6ca48d84");
        }
    }),
    LASER_SENDER_CONDITIONNAL_RANGE_DEACTIVATED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "55469ce2db16902aab78c3a963664b9f9b78e58536ad19dd3b9c2ab4d40456f");
        }
    }),
    //MISC Laser Receiver
    LASER_RECEIVER_DEACTIVATED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d444b5110df0ae37567f21432821184d64f8c4d9bf626e4ecb5b2369d9a6d290");
        }
    }),
    //MISC Redstone sensor
    REDSTONE_SENSOR_DEACTIVATED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d6df83c2c3a9ffca434f1a86a697618dcdda09427597c7638155858cc348a7");
        }
    }),
    //MISC Lock
    LOCK_OPENED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    //MISC KeyChest
    KEY_CHEST_1(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2"); //WHITE
        }
    }),
    KEY_CHEST_2(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a89b4c5e119a61773dd52d36b572bc0f9548d984ce0841417a301a65351a768d"); //RED
        }
    }),
    KEY_CHEST_3(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfcffe1445e7505446ba4c07575407122a835128a992a77646c032451b94b81b"); //YELLOW
        }
    }),
    KEY_CHEST_4(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "be4baf7377e13742f04407167e2130378c518cbbe7c3e0f98437bf5357dc3db8"); //BLUE
        }
    }),
    KEY_CHEST_5(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a16d8abc0114c6d6e4592a84296bd09bd73356e98889905b527137f479340b57"); //ORANGE
        }
    }),
    KEY_CHEST_6(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "19d8f04d6501c2fb82f83996580c67d605f31f2e8340da32fc1596837c124641"); //PURPLE
        }
    }),
    KEY_CHEST_7(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9a29bb965d1064c5624caae7f7afb4d7882a78458dd3d6998c7512e01118539d"); //GREEN
        }
    }),
    KEY_CHEST_8(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a0e6af393ad3a80158e6ed235480b0bfc06ff4dfa0ddd0f9d057c532614a4a9"); //BLACK
        }
    }),
    KEY_CHEST_9(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c2b4b1712bce87ce30c64ae7ba2cac985e1e5a174ad1e0915ee0b2e9eb03d07f"); //RED 2
        }
    }),
    KEY_CHEST_10(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2ca6883e09c94d2d0efe343fc089814947d5b16bcb90a9255d3d47c750e8639b"); //RED 3
        }
    }),
    KEY_CHEST_11(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "67b9315eec646c01639007498060f639f21897a4990a562476a8991b9f93b93b"); //RED 4
        }
    }),
    KEY_CHEST_12(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "afdc21340ed4887ed1cc59d9025a0bcbe62524d94a581f69932e8c9b6196a665"); //YELLOW 2
        }
    }),
    KEY_CHEST_13(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a20c561fc895425fe4534ef932c1f03340505872f69e24c8c548467b86647602"); //YELLOW 3
        }
    }),
    KEY_CHEST_14(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6fcb6c6d342d96b8fc6e50be2ee8f3bfffb57779e100fc0bf46e0b03b17d0681"); //BLUE 2
        }
    }),
    KEY_CHEST_15(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f4d3e99197a8080248c0abf0a75e616285d489ad2bb5bd1088c6828b9b20be58"); //BLUE 3
        }
    }),
    KEY_CHEST_16(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "561c46bf6e5d5ebc31654dfc4fef2f6bfb4981bef8c1008a7e7ab425b9c49813"); // BLUE 4
        }
    }),
    KEY_CHEST_17(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "65d44f08b95edf08982c68f0f6b056fb3b57a9cb02265bbb393f75fe50457fcb"); //BLUE 5
        }
    }),
    KEY_CHEST_18(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c9ca87a3319d7628442cd2e1a22594487e3cbeb05de31bd3bea89591d82b52c2"); //ORANGE 2
        }
    }),
    KEY_CHEST_19(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "39b8bee816fead9ef1ea70ba580327734f94c58b231d57737ca75a9dcdee10d9"); //PURPLE 2
        }
    }),
    KEY_CHEST_20(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c2a2459c8c2bed9e1d54b9d2d918a4e0d7384b3cd90bd657aa728c80ec4bfed0"); //PURPLE 3
        }
    }),
    KEY_CHEST_21(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "e233a0e752fad0560570588de50b0d951ba0038af871ec4fe6b4a8ec01d5ed08"); //PURPLE 4
        }
    }),
    KEY_CHEST_22(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5eac9392ebaab58b88c7528cdb3a62678e4dcb4d0dcc24cf287fb8178feaed62"); //GREEN 2
        }
    }),
    KEY_CHEST_23(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5f1d806d1c4cd0b1ddd0bea39f48e8d7475069a6d79d8e06becc12a93a3f3512"); //GREEN 3
        }
    }),
    KEY_CHEST_24(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "982bada50efa8cbd8758c226c7712158c6fca03eaa87e19c5c231d4feda8a2c6"); //GREEN 4
        }
    }),
    KEY_CHEST_25(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dda260fdc05722eebaab390aebc2f9392134440f3b1da5f30e5fe671238a8abb"); //GREY
        }
    }),
    //MISC Meltable clay
    RED_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.RED);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    GREEN_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    BLUE_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    YELLOW_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    MAGENTA_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    LIGHT_BLUE_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.LIGHT_BLUE);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    //Glasses
    WHITE_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.LIGHT_BLUE);
        }
    }),
    BLACK_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    //Glass panes
    WHITE_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.LIGHT_BLUE);
        }
    }),
    BLACK_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    //CONCRETE POWDER
    WHITE_CONCRETE_POWDER(ItemType.BLOCK, "mirror_block", true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.LIGHT_BLUE);
        }
    }),
    BLACK_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    /**
     * ************************ COMPONENT SHORTCUTBAR *************************
     */
    COMPONENT_SHORTCUTBAR_COLOR_LOOPER(ItemType.HEAD, "color_looper", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c227670d148794915304827b0eb03eff273ca153f874db5e9094d1cdbb6258a2");
        }
    }),
    COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR(ItemType.HEAD, "retrieve_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_SHORTCUTBAR_PLACE_MIRROR(ItemType.HEAD, "place_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE(ItemType.HEAD, "increase_sphere_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3213c1a420d11a06a74c3db98b21332b7493a8c5622d558003186882a1b99f56");
        }
    }),
    COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE(ItemType.HEAD, "decrease_sphere_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "10d4aeb26aa17345f9bdab1f199c27daf6efb1fd71546f23d35229f612f37328");
        }
    }),
    COMPONENT_SHORTCUTBAR_MOD_LOOPER(ItemType.HEAD, "mod_looper", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8ae52ae8c98ac19fd07637a469ffa256ab0b3b10ece6243186188ba38df154");
        }
    }),
    COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB(ItemType.HEAD, "increase_mirror_nb", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB(ItemType.HEAD, "decrease_mirror_nb", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_SHORTCUTBAR_PREV_SONG(ItemType.HEAD, "previous_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c4de72d37c2f5ba374bf36fd1d6b4789c9dcb3b8279ba037ff33684282859c89");
        }
    }),
    COMPONENT_SHORTCUTBAR_NEXT_SONG(ItemType.HEAD, "next_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "28f8139707f4fb341d52d2c3e077b2314617d754985784fc13fd8a978a3f74d2");
        }
    }),
    COMPONENT_SHORTCUTBAR_SONG_LOOP(ItemType.HEAD, "song_loop_toggle", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "aba40e063fae1ba19599a72426aa0d5ec347c70b8ebe4390dcf7da7fbc5ca5ec");
        }
    }),
    COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT(ItemType.HEAD, "song_stop_on_exit_toggle", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5a4c4ad32980ebd1dd975b9c2ba57ade092f5ae6e29534ebbfd1c9de19e69ebb");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_LEFT(ItemType.HEAD, "rotation_left", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_RIGHT(ItemType.HEAD, "rotation_right", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_UP(ItemType.HEAD, "rotation_up", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ccbf9883dd359fdf2385c90a459d737765382ec4117b04895ac4dc4b60fc");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_DOWN(ItemType.HEAD, "rotation_down", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "72431911f4178b4d2b413aa7f5c78ae4447fe9246943c31df31163c0e043e0d6");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN(ItemType.HEAD, "range_increase_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "47456b506ee3dcab215f33cb93a9edd9749f3e2ff8a4893426e1784692de8b5");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN(ItemType.HEAD, "range_decrease_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX(ItemType.HEAD, "range_increase_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX(ItemType.HEAD, "range_decrease_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dbe6713f20a9c2de43803b90aefa4db9a6bf89915d7c1590f77a7e10df3a3e54");
        }
    }),
    COMPONENT_SHORTCUTBAR_ADD_KEYCHEST(ItemType.HEAD, "add_key_chest", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2");
        }
    }),
    COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU(ItemType.HEAD, "teleport_to_key_chests_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER(ItemType.HEAD, "select_key_number_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK(ItemType.HEAD, "teleport_to_lock", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_SHORTCUTBAR_MENU(ItemType.HEAD, "menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "532a5838fd9cd4c977f15071d6997ff5c7f956074a2da571a19ccefb03c57");
        }
    }),
    COMPONENT_SHORTCUTBAR_CLOSE(ItemType.HEAD, "close_component_edition", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    COMPONENT_SHORTCUTBAR_CREATE_FLOOR(ItemType.HEAD, "create_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "7d4acfbd59bdfec5fb7a08adb735206e6db64c1ddb6441a3b5216e2a82d87563");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR(ItemType.HEAD, "go_to_ground_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "adb0a7a3fe5bc3ab66deedf9eb7ef632c403016f9a7e4473786aa8fe50c157ac");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR(ItemType.HEAD, "go_to_previous_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b6d0fce17905926dad6d7ca9ddf2f03b4b943384f351011a5597ecb2a9dca230");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR(ItemType.HEAD, "go_to_next_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "57190addde4ccc5246980ad4869344c22361183492e0b1c7809cc30e3c0e1cc9");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR(ItemType.HEAD, "go_to_top_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1f7e77b19f9e30fd8519ec85eb7ef2b71c28ecbc3864b88c2f7228ca4dc907fe");
        }
    }),
    COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS(ItemType.HEAD, "create_call_buttons", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9c928553e6780294c307cc4a16d32cc07b57ec57c7ffe3d2358a530f13441ce9");
        }
    }),
    /**
     * ************************** COLOR MENU ***************************
     */
    WHITE(ItemType.HEAD, "white", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "366a5c98928fa5d4b5d5b8efb490155b4dda3956bcaa9371177814532cfc");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    BLACK(ItemType.HEAD, "black", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "967a2f218a6e6e38f2b545f6c17733f4ef9bbb288e75402949c052189ee");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    RED(ItemType.HEAD, "red", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5fde3bfce2d8cb724de8556e5ec21b7f15f584684ab785214add164be7624b");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN(ItemType.HEAD, "green", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a7695f96dda626faaa010f4a5f28a53cd66f77de0cc280e7c5825ad65eedc72e");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE(ItemType.HEAD, "blue", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b8afa1555e9f876481e3c4299ec6e91d22b4075e67e58ef80dcd190ace6519f");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW(ItemType.HEAD, "yellow", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c641682f43606c5c9ad26bc7ea8a30ee47547c9dfd3c6cda49e1c1a2816cf0ba");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA(ItemType.HEAD, "magenta", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "205c17650e5d747010e8b69a6f2363fd11eb93f81c6ce99bf03895cefb92baa");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE(ItemType.HEAD, "light_blue", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "07c78f3ee783feecd2692eba54851da5c4323055ebd2f683cd3e8302fea7c");
            put(ItemAttribute.COLOR, LasersColor.LIGHT_BLUE);
        }
    }),
    /**
     * *********************** ARMOR CONFIGURATION MENU ***********************
     */
    ARMOR_CONFIG_REFLECTION_HEADER(ItemType.BLOCK, "armor_config_reflection_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    ARMOR_CONFIG_REFLECTION_UNCHECKED(ItemType.HEAD, "amour_config_reflection_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_REFLECTION_CHECKED(ItemType.HEAD, "armor_config_reflection_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_FOCUS_HEADER(ItemType.HEAD, "armor_config_focus_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6973f5f3b602437ef784d4c6a1752e194beb1b1737b11a33e27488d54de6a"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_FOCUS_UNCHECKED(ItemType.HEAD, "armor_config_focus_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_FOCUS_CHECKED(ItemType.HEAD, "armor_config_focus_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_PRISM_HEADER(ItemType.HEAD, "armor_config_prism_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "707ce43734cbefbb3da03f9ac1f01a3df456cee231550bd2dd42585594f9"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_PRISM_UNCHECKED(ItemType.HEAD, "armor_config_prism_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_PRISM_CHECKED(ItemType.HEAD, "armor_config_prism_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_DAMAGE_HEADER(ItemType.BLOCK, "armor_config_no_damage_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "DIAMOND_HELMET");
        }
    }),
    ARMOR_CONFIG_NO_DAMAGE_UNCHECKED(ItemType.HEAD, "armor_config_no_damage_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_DAMAGE_CHECKED(ItemType.HEAD, "armor_config_no_damage_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_BURNING_HEADER(ItemType.HEAD, "armor_config_no_burning_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "70a44d51ecc79f694cfd60228c88428848ca618e36a659a416e9246d841aec8");
        }
    }),
    ARMOR_CONFIG_NO_BURNING_UNCHECKED(ItemType.HEAD, "armor_config_no_burning_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_BURNING_CHECKED(ItemType.HEAD, "armor_config_no_burning_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_KNOCKBACK_HEADER(ItemType.BLOCK, "armor_config_no_knockback_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "PISTON_BASE");
        }
    }),
    ARMOR_CONFIG_NO_KNOCKBACK_UNCHECKED(ItemType.HEAD, "armor_config_no_knockback_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_KNOCKBACK_CHECKED(ItemType.HEAD, "armor_config_no_knockback_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_HEADER(ItemType.BLOCK, "armor_config_durability_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "ANVIL"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_UNCHECKED(ItemType.HEAD, "armor_config_durability_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_CHECKED(ItemType.HEAD, "armor_config_durability_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_DECREMENT(ItemType.HEAD, "armor_config_durability_decrement", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_INCREMENT(ItemType.HEAD, "armor_config_durability_increment", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_VALIDATE(ItemType.HEAD, "armor_config_validate", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6"); // TODO: Need to change
        }
    }),
    /**
     * *********************** AREA CONFIGURATION MENU ************************
     */
    AREA_CONFIG_MIRROR_SUPPORT(ItemType.HEAD, "area_config_mirror_support", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52baeb4a35da8a85d14bdccf7184f5545088f954da55144f235c2983fdb8e05b");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_mirror_support_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_mirror_support_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_mirror_support_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_mirror_support_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_CONCENTRATOR(ItemType.HEAD, "area_config_concentrator", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6973f5f3b602437ef784d4c6a1752e194beb1b1737b11a33e27488d54de6a");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_concentrator_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_concentrator_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_concentrator_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_concentrator_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_SENDER(ItemType.HEAD, "area_config_laser_sender", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "18ee67d7a5b675b57d1b7c7ae79280d2c19828e582f07339cfb3ed9b90ff1236");
        }
    }),
    AREA_CONFIG_LASER_SENDER_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_sender_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_SENDER_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_sender_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_SENDER_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_sender_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_SENDER_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_sender_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER(ItemType.HEAD, "area_config_laser_receiver", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "76da199506013166eed92be1aabf2b12b6a2be525f7e6b8f4bf0650404774564");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_receiver_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_receiver_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_receiver_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_receiver_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_GRAVITY_SPHERE(ItemType.HEAD, "area_config_gravity_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "89375d805291bac7d045c5c0448ab7f5599fa6d9a8caeae4bd251d586e2c8");
        }
    }),
    AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_gravity_sphere_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_gravity_sphere_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_HORIZONTAL(ItemType.HEAD, "area_config_horizontal", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e");
        }
    }),
    AREA_CONFIG_VERTICAL(ItemType.HEAD, "area_config_vertical", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ccbf9883dd359fdf2385c90a459d737765382ec4117b04895ac4dc4b60fc");
        }
    }),
    AREA_CONFIG_VICTORY_AREA_ADD(ItemType.HEAD, "area_config_victory_area_add", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "271c13a347c57aaca5b81aab47cc9ce093f3eab77f23b3c8aad0d20cca1a9a18");
        }
    }),
    AREA_CONFIG_VICTORY_AREA_DELETE(ItemType.HEAD, "area_config_victory_area_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "77ff15546e9aa76e4d813998e73339830dfd444f28a3422cf95143abfbbab79");
        }
    }),
    AREA_CONFIG_CHECKPOINT_DEFINE(ItemType.HEAD, "area_config_checkpoint_set", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ffc0caab7753228b4719a731901ece69a99b55b8478a72543f268d43f67c676c");
        }
    }),
    AREA_CONFIG_CHECKPOINT_DELETE(ItemType.HEAD, "area_config_checkpoint_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb6d4db4847abafea73333f891e7619b23ae42c14fd93d853cda4112449e7343");
        }
    }),
    AREA_CONFIG_MOD_VICTORY_AREA(ItemType.HEAD, "area_config_mod_victory_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "271c13a347c57aaca5b81aab47cc9ce093f3eab77f23b3c8aad0d20cca1a9a18");
        }
    }),
    AREA_CONFIG_MOD_VICTORY_AREA_CHECKED(ItemType.HEAD, "area_config_mod_victory_area_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_VICTORY_AREA_UNCHECKED(ItemType.HEAD, "area_config_mod_victory_area_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_LASER_RECEIVERS(ItemType.HEAD, "area_config_mod_laser_receiver", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "76da199506013166eed92be1aabf2b12b6a2be525f7e6b8f4bf0650404774564");
        }
    }),
    AREA_CONFIG_MOD_LASER_RECEIVERS_CHECKED(ItemType.HEAD, "area_config_mod_laser_receiver_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_LASER_RECEIVERS_UNCHECKED(ItemType.HEAD, "area_config_mod_laser_receiver_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_CHECKED(ItemType.HEAD, "area_config_mod_redstone_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_UNCHECKED(ItemType.HEAD, "area_config_mod_redstone_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_PLAYERS(ItemType.HEAD, "area_config_mod_players", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "eb7af9e4411217c7de9c60acbd3c3fd6519783332a1b3bc56fbfce90721ef35");
        }
    }),
    AREA_CONFIG_MOD_PLAYERS_CHECKED(ItemType.HEAD, "area_config_mod_players_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_PLAYERS_UNCHECKED(ItemType.HEAD, "area_config_mod_players_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_SENSORS(ItemType.HEAD, "area_config_mod_redstone_sensors", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfd12f99a72bbbd19b37e1f328d9e09718066159374b6a1414b8698ce37032db");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_SENSORS_CHECKED(ItemType.HEAD, "area_config_mod_redstone_sensors_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_SENSORS_UNCHECKED(ItemType.HEAD, "area_config_mod_redstone_sensors_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_LOCKS(ItemType.HEAD, "area_config_mod_locks", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    AREA_CONFIG_MOD_LOCKS_CHECKED(ItemType.HEAD, "area_config_mod_locks_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_LOCKS_UNCHECKED(ItemType.HEAD, "area_config_mod_locks_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    /**
     * ********************* VICTORY AREA SHORTCUT BAR *********************
     */
    VICTORY_AREA_SHORTCUTBAR_SELECT(ItemType.HEAD, "victory_area_shortcutbar_select", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "271c13a347c57aaca5b81aab47cc9ce093f3eab77f23b3c8aad0d20cca1a9a18");
        }
    }),
    VICTORY_AREA_SHORTCUTBAR_CLOSE(ItemType.HEAD, "victory_area_shortcutbar_close", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    /**
     * ************************** COMPONENT MENU ***************************
     */
    COMPONENT_MENU_COLOR(ItemType.HEAD, "component_menu_color", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c227670d148794915304827b0eb03eff273ca153f874db5e9094d1cdbb6258a2");
        }
    }),
    COMPONENT_MENU_RETRIEVE_MIRROR(ItemType.HEAD, "component_menu_retrieve_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_MENU_PLACE_MIRROR(ItemType.HEAD, "component_menu_place_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_MENU_MIRROR_FREE(ItemType.HEAD, "component_menu_mirror_support_free", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52baeb4a35da8a85d14bdccf7184f5545088f954da55144f235c2983fdb8e05b");
        }
    }),
    COMPONENT_MENU_MIRROR_FREE_CHECKED(ItemType.HEAD, "component_menu_mirror_support_free_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MIRROR_FREE_UNCHECKED(ItemType.HEAD, "component_menu_mirror_support_free_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MIRROR_BLOCKED(ItemType.HEAD, "component_menu_mirror_support_blocked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8eff7a46d3b15eebd7106a6b7f91c78718c7886b0abd3cd0505892499ca37daf");
        }
    }),
    COMPONENT_MENU_MIRROR_BLOCKED_CHECKED(ItemType.HEAD, "component_menu_mirror_support_blocked_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MIRROR_BLOCKED_UNCHECKED(ItemType.HEAD, "component_menu_mirror_support_blocked_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_INCREASE_MIRROR_CHEST(ItemType.HEAD, "component_menu_mirror_chest_increase", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_MENU_DECREASE_MIRROR_CHEST(ItemType.HEAD, "component_menu_mirror_chest_decrease", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_MENU_DECREASE_SPHERE_SIZE(ItemType.HEAD, "component_menu_decrease_attraction_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "10d4aeb26aa17345f9bdab1f199c27daf6efb1fd71546f23d35229f612f37328");
        }
    }),
    COMPONENT_MENU_INCREASE_SPHERE_SIZE(ItemType.HEAD, "component_menu_increase_attraction_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3213c1a420d11a06a74c3db98b21332b7493a8c5622d558003186882a1b99f56");
        }
    }),
    COMPONENT_MENU_MOD_ATTRACTION(ItemType.HEAD, "component_menu_mod_attraction", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "89375d805291bac7d045c5c0448ab7f5599fa6d9a8caeae4bd251d586e2c8");
        }
    }),
    COMPONENT_MENU_MOD_ATTRACTION_CHECKED(ItemType.HEAD, "component_menu_mod_attraction_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_ATTRACTION_UNCHECKED(ItemType.HEAD, "component_menu_mod_attraction_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_REPULSION(ItemType.HEAD, "component_menu_mod_repulsion", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5bfa6eab81e21bdbfbb987ed695a4596ed33e77510908dadf696912f9423f75");
        }
    }),
    COMPONENT_MENU_MOD_REPULSION_CHECKED(ItemType.HEAD, "component_menu_mod_repulsion_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_REPULSION_UNCHECKED(ItemType.HEAD, "component_menu_mod_repulsion_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_PREV_SONG(ItemType.HEAD, "component_menu_prev_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c4de72d37c2f5ba374bf36fd1d6b4789c9dcb3b8279ba037ff33684282859c89");
        }
    }),
    COMPONENT_MENU_NEXT_SONG(ItemType.HEAD, "component_menu_next_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "28f8139707f4fb341d52d2c3e077b2314617d754985784fc13fd8a978a3f74d2");
        }
    }),
    COMPONENT_MENU_SONG_STOP_ON_EXIT(ItemType.HEAD, "component_menu_song_stop_on_exit", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5a4c4ad32980ebd1dd975b9c2ba57ade092f5ae6e29534ebbfd1c9de19e69ebb");
        }
    }),
    COMPONENT_MENU_SONG_LOOP(ItemType.HEAD, "component_menu_song_loop", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "aba40e063fae1ba19599a72426aa0d5ec347c70b8ebe4390dcf7da7fbc5ca5ec");
        }
    }),
    COMPONENT_MENU_SONG_LOOP_CHECKED(ItemType.HEAD, "component_menu_song_loop_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_SONG_LOOP_UNCHECKED(ItemType.HEAD, "component_menu_song_loop_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_SONG_STOP_ON_EXIT_CHECKED(ItemType.HEAD, "component_menu_song_stop_on_exit_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_SONG_STOP_ON_EXIT_UNCHECKED(ItemType.HEAD, "component_menu_song_stop_on_exit_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_RANGE_INCREASE_MIN(ItemType.HEAD, "component_menu_range_increase_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "47456b506ee3dcab215f33cb93a9edd9749f3e2ff8a4893426e1784692de8b5");
        }
    }),
    COMPONENT_MENU_RANGE_INCREASE_MAX(ItemType.HEAD, "component_menu_range_increase_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458");
        }
    }),
    COMPONENT_MENU_RANGE_DECREASE_MIN(ItemType.HEAD, "component_menu_range_decrease_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186");
        }
    }),
    COMPONENT_MENU_RANGE_DECREASE_MAX(ItemType.HEAD, "component_menu_range_decrease_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dbe6713f20a9c2de43803b90aefa4db9a6bf89915d7c1590f77a7e10df3a3e54");
        }
    }),
    COMPONENT_MENU_UP(ItemType.HEAD, "component_menu_up", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ccbf9883dd359fdf2385c90a459d737765382ec4117b04895ac4dc4b60fc");
        }
    }),
    COMPONENT_MENU_DOWN(ItemType.HEAD, "component_menu_down", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "72431911f4178b4d2b413aa7f5c78ae4447fe9246943c31df31163c0e043e0d6");
        }
    }),
    COMPONENT_MENU_LEFT(ItemType.HEAD, "component_menu_left", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");
        }
    }),
    COMPONENT_MENU_RIGHT(ItemType.HEAD, "component_menu_right", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e");
        }
    }),
    COMPONENT_MENU_MOD_PERMANENTLY_ENABLED(ItemType.HEAD, "component_menu_mod_permanently_enabled", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "e1284368eb8649e980bed6c66565dea312d262cf5a271935de24dc361337d9de");
        }
    }),
    COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_CHECKED(ItemType.HEAD, "component_menu_mod_permanently_enabled_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_UNCHECKED(ItemType.HEAD, "component_menu_mod_permanently_enabled_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_LASER_RECEIVERS(ItemType.HEAD, "component_menu_mod_laser_receiver", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "76da199506013166eed92be1aabf2b12b6a2be525f7e6b8f4bf0650404774564");
        }
    }),
    COMPONENT_MENU_MOD_LASER_RECEIVERS_CHECKED(ItemType.HEAD, "component_menu_mod_laser_receiver_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_LASER_RECEIVERS_UNCHECKED(ItemType.HEAD, "component_menu_mod_laser_receiver_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE(ItemType.BLOCK, "component_menu_mod_redstone", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "REDSTONE");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_CHECKED(ItemType.HEAD, "component_menu_mod_redstone_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_UNCHECKED(ItemType.HEAD, "component_menu_mod_redstone_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_PLAYERS(ItemType.HEAD, "component_menu_mod_players", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "eb7af9e4411217c7de9c60acbd3c3fd6519783332a1b3bc56fbfce90721ef35");
        }
    }),
    COMPONENT_MENU_MOD_PLAYERS_CHECKED(ItemType.HEAD, "component_menu_mod_players_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_PLAYERS_UNCHECKED(ItemType.HEAD, "component_menu_mod_players_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_SENSORS(ItemType.HEAD, "component_menu_mod_redstone_sensors", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfd12f99a72bbbd19b37e1f328d9e09718066159374b6a1414b8698ce37032db");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_SENSORS_CHECKED(ItemType.HEAD, "component_menu_mod_redstone_sensors_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_SENSORS_UNCHECKED(ItemType.HEAD, "component_menu_mod_redstone_sensors_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_LOCKS(ItemType.HEAD, "component_menu_mod_locks", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    COMPONENT_MENU_MOD_LOCKS_CHECKED(ItemType.HEAD, "component_menu_mod_locks_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_LOCKS_UNCHECKED(ItemType.HEAD, "component_menu_mod_locks_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_ADD_KEY_CHEST(ItemType.HEAD, "component_menu_add_key_chest", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_KEY_CHESTS(ItemType.HEAD, "component_menu_delete_all_key_chests", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "36b550aa9a72e911c5d53fa5501c30411897628ff642cc131da12a13cb3f0bfc");
        }
    }),
    COMPONENT_MENU_DELETE_MY_KEYS(ItemType.HEAD, "component_menu_delete_my_keys", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f4f8c46906b56c0d9306673f91bda911b9117924b0c4765346afd1dbd5f3bcc4");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_PLAYERS_KEYS(ItemType.HEAD, "component_menu_delete_all_players_keys", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "11778a7e91052b6a3bd1c50545216c65b8e434dc96c9ae94009e346ac7618305");
        }
    }),
    COMPONENT_MENU_TELEPORT_TO_KEY_CHESTS_MENU(ItemType.HEAD, "component_menu_teleport_to_keys_chests_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_MENU_TELEPORT_TO_LOCK(ItemType.HEAD, "component_menu_teleport_to_lock", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_MENU_SELECT_KEY_NUMBER(ItemType.HEAD, "component_menu_select_key_number_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    COMPONENT_MENU_GO_TO_GROUND_FLOOR(ItemType.HEAD, "component_menu_go_to_ground_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "adb0a7a3fe5bc3ab66deedf9eb7ef632c403016f9a7e4473786aa8fe50c157ac");
        }
    }),
    COMPONENT_MENU_GO_TO_PREVIOUS_FLOOR(ItemType.HEAD, "component_menu_go_to_previous_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b6d0fce17905926dad6d7ca9ddf2f03b4b943384f351011a5597ecb2a9dca230");
        }
    }),
    COMPONENT_MENU_GO_TO_NEXT_FLOOR(ItemType.HEAD, "component_menu_go_to_next_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "57190addde4ccc5246980ad4869344c22361183492e0b1c7809cc30e3c0e1cc9");
        }
    }),
    COMPONENT_MENU_GO_TO_TOP_FLOOR(ItemType.HEAD, "component_menu_go_to_top_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1f7e77b19f9e30fd8519ec85eb7ef2b71c28ecbc3864b88c2f7228ca4dc907fe");
        }
    }),
    COMPONENT_MENU_AS_HIGH_AS_POSSIBLE(ItemType.HEAD, "component_menu_go_as_high_as_possible", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cf2ec4e127be4a74a1e255162893c73e3e5df2e7886a2ac4f13ef0ec84ec88ae");
        }
    }),
    COMPONENT_MENU_CREATE_FLOOR(ItemType.HEAD, "component_menu_create_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "7d4acfbd59bdfec5fb7a08adb735206e6db64c1ddb6441a3b5216e2a82d87563");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_FLOORS(ItemType.HEAD, "component_menu_delete_all_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dc95661ca7e5126157603c4c456e68410706e4397e4df6ea70cdf782dd7c4686");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_CALL_BUTTONS(ItemType.HEAD, "component_menu_delete_all_call_button", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8a29357802a9d66f8e7ebf4ce8c2ef6d06fd1f027ddc9bb77f1ffbda3536108b");
        }
    }),
    COMPONENT_MENU_TELEPORT_INSIDE_ELEVATOR(ItemType.HEAD, "component_menu_teleport_inside_elevator", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU(ItemType.HEAD, "component_menu_scheduled_actions_open_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ba10da526e5111cfb6e3ebd47693e162dd52d41a2182028daa7c2b19aa3143");
        }
    }), COMPONENT_MENU_LIGHT_LEVEL_OPEN_MENU(ItemType.HEAD, "component_menu_light_level_open_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ed3d9fed0307399ab61510e9c04103330045d87453afde614af11adc19effdff");
        }
    }),
    // LIGHT LEVEL
    LIGHT_LEVEL_DECREMENT(ItemType.HEAD, "light_level_decrement", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186");
        }
    }),
    LIGHT_LEVEL_INCREMENT(ItemType.HEAD, "light_level_increment", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458");
        }
    }),
    // SCHEDULED ACTIONS
    SCHEDULED_ACTIONS_WAIT(ItemType.HEAD, "action_wait", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8477dafc8c9ea0799633381793866d146c9a39fad4c6684e7117d97e9b6c3");
        }
    }),
    SCHEDULED_ACTIONS_START_SAVING_ACTIONS(ItemType.HEAD, "action_main_menu_save_start", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ff364199440eada585ee475dfdf2e4f74feb3ccfeef7c48a991ab22020f7ff37");
        }
    }),
    SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE(ItemType.HEAD, "action_main_menu_save_end", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c1cadffb521f8041467167ad426f3b21fd3e4be57a2bbeade2ece1987f640ef6");
        }
    }),
    SCHEDULED_ACTIONS_OPEN_EDITION_MENU(ItemType.HEAD, "action_main_menu_open_edition_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "af0af4c16c5ed0d059e5fc27a84779a9cd41ae74c29974ac47a8cc68df12726");
        }
    }),
    SCHEDULED_ACTIONS_CLEAR(ItemType.HEAD, "action_main_menu_clear_actions", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ee37aac354f866d8b41b723518de1556cccc3ae39c4e8efdeef49af188ed9f21");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_NEW_LEFT(ItemType.HEAD, "action_edit_list_add_new_before", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "62e730e1e4f13fd58312fd00603efb126765e47858efc2a6b7dec3bb5dc28dac");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_NEW_RIGHT(ItemType.HEAD, "action_edit_list_add_new_after", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "62e730e1e4f13fd58312fd00603efb126765e47858efc2a6b7dec3bb5dc28dac");
        }
    }),
    BLACK_EMPTY(ItemType.HEAD, "-", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d44c165a14eb13df6b07c85f209c79e2008e0b67ad0b16093b000ed012ccf289");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_DELETE(ItemType.HEAD, "action_edit_list_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ee37aac354f866d8b41b723518de1556cccc3ae39c4e8efdeef49af188ed9f21");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_MOVE_LEFT(ItemType.HEAD, "action_edit_list_move_left", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cdc9e4dcfa4221a1fadc1b5b2b11d8beeb57879af1c42362142bae1edd5");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_MOVE_RIGHT(ItemType.HEAD, "action_edit_list_move_right", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "956a3618459e43b287b22b7e235ec699594546c6fcd6dc84bfca4cf30ab9311");
        }
    }),
    SCHEDULED_ACTION_EDIT_MORE_DELAY(ItemType.HEAD, "action_edit_one_more_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "e02fa3b2dcb11c6639cc9b9146bea54fbc6646d855bdde1dc6435124a11215d");
        }
    }),
    SCHEDULED_ACTION_EDIT_VERY_MORE_DELAY(ItemType.HEAD, "action_edit_one_very_more_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dd993b8c13588919b9f8b42db065d5adfe78af182815b4e6f0f91ba683deac9");
        }
    }),
    SCHEDULED_ACTION_EDIT_LESS_DELAY(ItemType.HEAD, "action_edit_one_less_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "74133f6ac3be2e2499a784efadcfffeb9ace025c3646ada67f3414e5ef3394");
        }
    }),
    SCHEDULED_ACTION_EDIT_VERY_LESS_DELAY(ItemType.HEAD, "action_edit_one_very_less_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f4bfef14e8420a256e457a4a7c88112e179485e523457e4385177bad");
        }
    }),
    SCHEDULED_ACTION_EDIT_DELETE(ItemType.HEAD, "action_edit_one_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ee37aac354f866d8b41b723518de1556cccc3ae39c4e8efdeef49af188ed9f21");
        }
    }),
    SCHEDULED_ACTION_EDIT_BACK(ItemType.HEAD, "action_edit_one_back", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    // ARMOR ACTION MENU
    ARMOR_ACTION_MENU_SELF_GIVE(ItemType.HEAD, "armor_action_menu_self_give", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "eaa1d09da8ad1002543533fb5d431f7abf3719c9040290af19acffb871017102");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.GIVE_TO_SELF);
        }
    }),
    ARMOR_ACTION_MENU_GIVE_NEAREST_PLAYER(ItemType.HEAD, "armor_action_menu_give_nearest_player", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "29392be75a43666163e7505ca0756111ec62273c71b20aea61bee789fee4272a");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.GIVE_TO_NEAREST);
        }
    }),
    ARMOR_ACTION_MENU_COMMAND_BLOCK_GIVE_NEAREST_PLAYER(ItemType.HEAD, "armor_action_menu_command_block_give_nearest_player", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1ec68da9014b1733fe27f49b2ad02c289ac7146b53283c43a486891944950f15");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.COMMAND_BLOCK_GIVE_TO_NEAREST);
        }
    }),
    ARMOR_ACTION_MENU_COMMAND_BLOCK_FILLS_CHEST(ItemType.HEAD, "armor_action_menu_command_block_fills_chest", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c7675c84eb4cb7d10bdb733796f8158a150b070154c8bc1d1b9ba909778017c3");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.COMMAND_BLOCK_FILLS_CHEST);
        }
    }),
    ARMOR_ACTION_MENU_COMMAND_BLOCK_PUT_ARMOR_ON_NEAREST_PLAYER(ItemType.HEAD, "armor_action_menu_command_block_armor_on_nearest_player", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ed13b4879e2adfda42005a8eee2de0b4469752c3d499ecb97f5e800ce8f3081a");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.COMMAND_BLOCK_EQUIP_ARMOR_ON_NEAREST);
        }
    }),
    // NUMBERS
    COMPONENT_MENU_ZERO(ItemType.HEAD, "0", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6d68343bd0b129de93cc8d3bba3b97a2faa7ade38d8a6e2b864cd868cfab");
        }
    }),
    COMPONENT_MENU_ONE(ItemType.HEAD, "1", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d2a6f0e84daefc8b21aa99415b16ed5fdaa6d8dc0c3cd591f49ca832b575");
        }
    }),
    COMPONENT_MENU_TWO(ItemType.HEAD, "2", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96fab991d083993cb83e4bcf44a0b6cefac647d4189ee9cb823e9cc1571e38");
        }
    }),
    COMPONENT_MENU_THREE(ItemType.HEAD, "3", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cd319b9343f17a35636bcbc26b819625a9333de3736111f2e932827c8e749");
        }
    }),
    COMPONENT_MENU_FOUR(ItemType.HEAD, "4", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d198d56216156114265973c258f57fc79d246bb65e3c77bbe8312ee35db6");
        }
    }),
    COMPONENT_MENU_FIVE(ItemType.HEAD, "5", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "7fb91bb97749d6a6eed4449d23aea284dc4de6c3818eea5c7e149ddda6f7c9");
        }
    }),
    COMPONENT_MENU_SIX(ItemType.HEAD, "6", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9c613f80a554918c7ab2cd4a278752f151412a44a73d7a286d61d45be4eaae1");
        }
    }),
    COMPONENT_MENU_SEVEN(ItemType.HEAD, "7", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9e198fd831cb61f3927f21cf8a7463af5ea3c7e43bd3e8ec7d2948631cce879");
        }
    }),
    COMPONENT_MENU_EIGHT(ItemType.HEAD, "8", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "84ad12c2f21a1972f3d2f381ed05a6cc088489fcfdf68a713b387482fe91e2");
        }
    }),
    COMPONENT_MENU_NINE(ItemType.HEAD, "9", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9f7aa0d97983cd67dfb67b7d9d9c641bc9aa34d96632f372d26fee19f71f8b7");
        }
    }),
    /**
     * *********************** AREA DELETE CONFIRMATION **********************
     */
    //Area Delete Confirmation
    HELP_AREA_CONFIRM(ItemType.HEAD, "area_delete_confirm_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_DELETE_AREA(ItemType.HEAD, "area_delete_confirm_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_DELETE_AREA(ItemType.HEAD, "area_delete_confirm_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ************************** AREA STATS MENU *****************************
     */
    AREA_STATS_TIME_LIST_MENU(ItemType.HEAD, "area_stats_time_list_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2ee174f41e594e64ea3141c07daf7acf1fa045c230b2b0b0fb3da163db22f455");
        }
    }),
    AREA_STATS_ACTION_LIST_MENU(ItemType.HEAD, "area_stats_action_list_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "4ae29422db4047efdb9bac2cdae5a0719eb772fccc88a66d912320b343c341");
        }
    }),
    AREA_STATS_STEP_LIST_MENU(ItemType.HEAD, "area_stats_step_list_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "24346ae5e1d13857498bac5e7aaee816c8359021614961d26d2d6ba619e8ca2");
        }
    }),
    AREA_STATS_LINK(ItemType.HEAD, "area_stats_link", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5c52364520b3a9bb8ed515c01f80ab7b977025cd0b0ff6d86468a5164c6fb78");
        }
    }),
    AREA_STATS_UNLINK(ItemType.HEAD, "area_stats_unlink", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c99bc7dfd3cad70ebe12fc35db6fd3f1d652c6fea9929fa3b22fe6eef5c1");
        }
    }),
    AREA_STATS_CLEAR(ItemType.HEAD, "area_stats_clear", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c931618dee7d4ecda91b1d9fd2741537c6ed21ea26f72aa3d941e39185391");
        }
    }),
    /**
     * ******************** AREA STATS CLEAR CONFIRMATION ********************
     */
    HELP_AREA_STATS_CLEAR_CONFIRM(ItemType.HEAD, "area_stats_clear_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_AREA_STATS_CLEAR(ItemType.HEAD, "area_stats_clear_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_AREA_STATS_CLEAR(ItemType.HEAD, "area_stats_clear_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * *********************** PAGINABLE INVENTORIES ***********************
     */
    PREVIOUS_COLUMN(ItemType.HEAD, "previous_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "30b8695acbc61d0fd78a8eba0aec351c5faa18a806fe7309f16aafe937da652d");
        }
    }),
    PREVIOUS_PAGE(ItemType.HEAD, "previous_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9cdb8f43656c06c4e8683e2e6341b4479f157f48082fea4aff09b37ca3c6995b");
        }
    }),
    PAGINABLE_INDICATOR(ItemType.HEAD, "-", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "967a2f218a6e6e38f2b545f6c17733f4ef9bbb288e75402949c052189ee");
        }
    }),
    NEXT_PAGE(ItemType.HEAD, "next_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "61e1e730c77279c8e2e15d8b271a117e5e2ca93d25c8be3a00cc92a00cc0bb85");
        }
    }),
    NEXT_COLUMN(ItemType.HEAD, "next_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "84c8ba3eee68d618e783022a81d4804d2f13a5dce298de8932482b5f47b0705f");
        }
    }),
    /**
     * ******************* AREA STATS LINK CONFIRMATION ********************
     */
    HELP_AREA_STATS_LINK_CONFIRM(ItemType.HEAD, "area_stats_link_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_AREA_STATS_LINK(ItemType.HEAD, "area_stats_link_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_AREA_STATS_LINK(ItemType.HEAD, "area_stats_link_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ******************* AREA STATS UNLINK CONFIRMATION *******************
     */
    HELP_AREA_STATS_UNLINK_CONFIRM(ItemType.HEAD, "area_stats_unlink_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_AREA_STATS_UNLINK(ItemType.HEAD, "area_stats_unlink_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_AREA_STATS_UNLINK(ItemType.HEAD, "area_stats_unlink_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ********************* AREA STATS LINK SHORTCUTBAR *********************
     */
    AREA_STATS_LINK_SHORTCUTBAR_EXIT(ItemType.HEAD, "area_stats_shortcutbar_exit", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA(ItemType.HEAD, "area_stats_shortcutbar_select_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8e88ead488ff68d35b61910c927a890ebc2c7605d76a1d13b5edb715e68a2cf3");
        }
    }),
    /**
     * ********************* PLACE KEY CHESTS SHORTCUTBAR *********************
     */
    PLACE_KEY_CHEST_SHORTCUTBAR_EXIT(ItemType.HEAD, "place_key_chest_shortcutbar_exit", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    PLACE_KEY_CHEST_SHORTCUTBAR_PLACE(ItemType.HEAD, "place_key_chest_shortcutbar_place", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2");
        }
    }),
    /**
     * ****************** DELETE LOCK'S PLAYERS KEYS CONFIRM ******************
     */
    HELP_LOCK_S_PLAYERS_KEYS_DELETE_CONFIRM(ItemType.HEAD, "lock_players_keys_delete_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS(ItemType.HEAD, "lock_players_keys_delete_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_DELETE_LOCK_S_PLAYERS_KEYS(ItemType.HEAD, "lock_players_keys_delete_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ******************* DELETE LOCK'S KEY CHESTS CONFIRM *******************
     */
    HELP_LOCK_S_KEY_CHEST_DELETE_CONFIRM(ItemType.HEAD, "lock_key_chests_delete_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_DELETE_LOCK_S_KEY_CHEST(ItemType.HEAD, "lock_key_chests_delete_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_DELETE_LOCK_S_KEY_CHEST(ItemType.HEAD, "lock_key_chests_delete_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    });

    /**
     * the last slot from which the specials items will be added in the players
     * inventories
     */
    public static final int SPECIAL_ITEMS_INVENTORY_LAST_SLOT = 17;
    /**
     * the items base path in the configuration
     */
    public static final String BASE_PATH = "items.";
    /**
     * the relative path to items name in the configuration
     */
    public static final String NAME_PATH = ".name";

    /**
     * the relative path to items description in the configuration
     */
    public static final String DESCRIPTION_PATH = ".description";
    /**
     * the item type
     */
    private final ItemType type;
    /**
     * the config path
     */
    private final String configPath;
    /**
     * the item attributes and values
     */
    private final HashMap<ItemAttribute, Object> attributes;
    private final boolean isPlacableOutsideAnArea;
    private final boolean isPlacableAsBlock;

    /**
     * Constructor
     *
     * @param type                    the item type
     * @param configPath              the config path
     * @param attributes              the item attributes and values
     * @param isPlacableAsBlock       can this item be placed as a block
     * @param isPlacableOutsideAnArea can this item be placed outside a puzzle area
     */
    Item(ItemType type, String configPath, boolean isPlacableOutsideAnArea, boolean isPlacableAsBlock, HashMap<ItemAttribute, Object> attributes) {
        this.type = type;
        this.configPath = configPath;
        this.attributes = attributes;
        this.isPlacableAsBlock = isPlacableAsBlock;
        this.isPlacableOutsideAnArea = isPlacableOutsideAnArea;
    }

    public static Item getMirror(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.MIRROR;
            case RED:
                return Item.RED_MIRROR;
            case GREEN:
                return Item.GREEN_MIRROR;
            case BLUE:
                return Item.BLUE_MIRROR;
            case YELLOW:
                return Item.YELLOW_MIRROR;
            case MAGENTA:
                return Item.MAGENTA_MIRROR;
            case LIGHT_BLUE:
                return Item.LIGHT_BLUE_MIRROR;
            case BLACK:
                return Item.BLACK_MIRROR;
            default:
                return null;
        }
    }

    public static ArrayList<Item> getMirrors() {
        return new ArrayList<>(Arrays.asList(Item.MIRROR,
                Item.RED_MIRROR,
                Item.GREEN_MIRROR,
                Item.BLUE_MIRROR,
                Item.YELLOW_MIRROR,
                Item.MAGENTA_MIRROR,
                Item.LIGHT_BLUE_MIRROR,
                Item.BLACK_MIRROR
        ));
    }

    public static Item getReflectingSphereItem(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.REFLECTING_SPHERE_WHITE;
            case RED:
                return Item.REFLECTING_SPHERE_RED;
            case GREEN:
                return Item.REFLECTING_SPHERE_GREEN;
            case BLUE:
                return Item.REFLECTING_SPHERE_BLUE;
            case YELLOW:
                return Item.REFLECTING_SPHERE_YELLOW;
            case MAGENTA:
                return Item.REFLECTING_SPHERE_MAGENTA;
            case LIGHT_BLUE:
                return Item.REFLECTING_SPHERE_LIGHT_BLUE;
            case BLACK:
                return Item.REFLECTING_SPHERE;
            default:
                return null;
        }
    }

    public static Item getFilteringSphereItem(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.FILTERING_SPHERE_WHITE;
            case RED:
                return Item.FILTERING_SPHERE_RED;
            case GREEN:
                return Item.FILTERING_SPHERE_GREEN;
            case BLUE:
                return Item.FILTERING_SPHERE_BLUE;
            case YELLOW:
                return Item.FILTERING_SPHERE_YELLOW;
            case MAGENTA:
                return Item.FILTERING_SPHERE_MAGENTA;
            case LIGHT_BLUE:
                return Item.FILTERING_SPHERE_LIGHT_BLUE;
            case BLACK:
                return Item.FILTERING_SPHERE;
            default:
                return null;
        }
    }

    public static Item getMeltableClay(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.WHITE_MELTABLE_CLAY;
            case RED:
                return Item.RED_MELTABLE_CLAY;
            case GREEN:
                return Item.GREEN_MELTABLE_CLAY;
            case BLUE:
                return Item.BLUE_MELTABLE_CLAY;
            case YELLOW:
                return Item.YELLOW_MELTABLE_CLAY;
            case MAGENTA:
                return Item.MAGENTA_MELTABLE_CLAY;
            case LIGHT_BLUE:
                return Item.LIGHT_BLUE_MELTABLE_CLAY;
            default:
                return null;
        }
    }

    /**
     * gets the item type
     *
     * @return the item type
     */
    public ItemType getType() {
        return this.type;
    }

    public boolean isUsableWithoutEditionMode() {
        return isPlacableOutsideAnArea;
    }

    /**
     * checks if this item has a name and description
     *
     * @return true if this item has a name and description
     */
    public boolean hasNameAndLore() {
        return (this.configPath != null && this.configPath.length() != 0);
    }

    public boolean isMirror(boolean blackAccepted) {
        switch (this) {
            case BLACK_MIRROR:
                return blackAccepted;
            case MIRROR:
            case RED_MIRROR:
            case BLUE_MIRROR:
            case GREEN_MIRROR:
            case YELLOW_MIRROR:
            case MAGENTA_MIRROR:
            case LIGHT_BLUE_MIRROR:
                return true;
            default:
                return false;
        }
    }

    /**
     * gets the path of this item inside configuration
     *
     * @return the path of this item inside configuration
     */
    public String getConfigPath() {
        if (this.configPath == null) {
            return this.toString();

        }
        return this.configPath;
    }

    public boolean isPlacableOutsideAnArea() {
        return isPlacableOutsideAnArea;
    }

    public boolean isPlacableAsBlock() {
        return isPlacableAsBlock;
    }

    /**
     * gets the attributes of this item
     *
     * @return the attributes of this item
     */
    public HashMap<ItemAttribute, Object> getAttributes() {
        return this.attributes;
    }

}
