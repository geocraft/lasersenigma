package eu.lasersenigma.items;

public enum ArmorAction {
    GIVE_TO_SELF,
    GIVE_TO_NEAREST,
    COMMAND_BLOCK_GIVE_TO_NEAREST,
    COMMAND_BLOCK_FILLS_CHEST,
    COMMAND_BLOCK_EQUIP_ARMOR_ON_NEAREST
}
