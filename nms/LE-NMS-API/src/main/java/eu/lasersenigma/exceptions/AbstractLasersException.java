package eu.lasersenigma.exceptions;

public abstract class AbstractLasersException extends Exception {

    /**
     * abstract class for this plugin exceptions
     */
    private final String code;

    /**
     * Constructor
     *
     * @param translationCode the path to the error translation in config
     */
    public AbstractLasersException(String translationCode) {
        super();
        this.code = translationCode;
    }

    /**
     * gets the translation path for this error
     *
     * @return the translation path for this error
     */
    public String getTranslationCode() {
        return code;
    }
}
