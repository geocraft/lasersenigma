package eu.lasersenigma.exceptions;

public class SchematicInvalidException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    public static final String TRANSLATION_CODE = "invalid_schematic";

    /**
     * Constructor
     */
    public SchematicInvalidException() {
        super(TRANSLATION_CODE);
    }

}
