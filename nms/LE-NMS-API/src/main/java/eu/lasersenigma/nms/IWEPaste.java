package eu.lasersenigma.nms;

import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicInvalidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import org.bukkit.Location;

public interface IWEPaste {

    Location getPlayerLocation();

    boolean initializePaste() throws WorldEditNotAvailableException, InvalidSelectionException, SchematicInvalidException;

    void finalizePaste() throws SchematicInvalidException;

}
