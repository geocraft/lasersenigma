package eu.lasersenigma.nms.v1_12_R1;

import com.sk89q.jnbt.NBTOutputStream;
import com.sk89q.worldedit.*;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardWriter;
import com.sk89q.worldedit.extent.clipboard.io.SchematicWriter;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.internal.LocalWorldAdapter;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.util.io.Closer;
import com.sk89q.worldedit.world.World;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicSaveException;
import eu.lasersenigma.exceptions.SelectionNotCuboidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import eu.lasersenigma.nms.IWECopy;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

public class NMSWECopy extends ANMSWE implements IWECopy {

    private final Player player;

    private Location min;
    private Location max;
    private Location playerLocation;
    private byte[] bytes;

    public NMSWECopy(Player player) throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicSaveException {
        this.player = player;
        initializeClipboardCopy();
    }

    @Override
    public Location getMin() {
        return min;
    }

    @Override
    public Location getMax() {
        return max;
    }

    @Override
    public Location getPlayerLocation() {
        return playerLocation;
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }

    private void initializeClipboardCopy() throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicSaveException {
        WorldEditPlugin worldEditPlugin = this.getWorldEditPlugin();
        LocalSession session = worldEditPlugin.getSession(player);
        com.sk89q.worldedit.entity.Player worldEditPlayer = worldEditPlugin.wrapPlayer(player);
        EditSession editSession = session.createEditSession(worldEditPlayer);
        this.playerLocation = player.getLocation();
        World world = LocalWorldAdapter.adapt(new BukkitWorld(player.getWorld()));

        Selection sel = worldEditPlugin.getSelection(player);
        if (!(sel instanceof CuboidSelection)) {
            throw new SelectionNotCuboidException();
        }
        CuboidSelection cuboidSel = (CuboidSelection) sel;

        Region selectedRegion;
        try {
            selectedRegion = cuboidSel.getRegionSelector().getRegion();
        } catch (IncompleteRegionException e) {
            throw new InvalidSelectionException();
        }
        BlockArrayClipboard clipboard = new BlockArrayClipboard(selectedRegion);
        clipboard.setOrigin(new Vector(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ()));
        ForwardExtentCopy copy = new ForwardExtentCopy(editSession, selectedRegion, clipboard, selectedRegion.getMinimumPoint());
        try {
            Operations.completeLegacy(copy);
        } catch (MaxChangedBlocksException e) {
            throw new SchematicSaveException();
        }
        this.min = cuboidSel.getMinimumPoint();
        this.max = cuboidSel.getMaximumPoint();
        Closer closer = Closer.create();
        try {
            ByteArrayOutputStream baos = closer.register(new ByteArrayOutputStream());
            BufferedOutputStream bos = closer.register(new BufferedOutputStream(baos));
            GZIPOutputStream gzos = closer.register(new GZIPOutputStream(bos));
            NBTOutputStream nbtStream = closer.register(new NBTOutputStream(gzos));
            ClipboardWriter writer = closer.register(new SchematicWriter(nbtStream));
            writer.write(clipboard, world.getWorldData());
            closer.close();
            this.bytes = baos.toByteArray();
        } catch (IOException e) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "An error occured while saving schematic.", e);
            try {
                closer.close();
            } catch (IOException ex) {
                Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
            }
            throw new SchematicSaveException();
        }
    }
}
