package eu.lasersenigma.config;

import eu.lasersenigma.LasersEnigmaPlugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.logging.Level;

public class LEUpdateNotifier {

    /**
     * the path to the check_for_new_versions boolean in the configuration
     */
    private static final String CHECK_FOR_NEW_VERSIONS_CONFIG_NODE = "check_for_new_versions";
    /**
     * the path to the check_for_new_beta_versions boolean in the configuration
     */
    private static final String CHECK_FOR_NEW_BETA_VERSIONS_CONFIG_NODE = "check_for_new_beta_versions";
    /**
     * the online url of the text file containing the version number
     */
    private static final String VERSION_NUMBER_FILE_URL = "https://lasers-enigma.eu/lasers-enigma-version.txt";
    /**
     * the online url of the text file containing the commit hash
     */
    private static final String COMMIT_HASH_FILE_URL = "https://lasers-enigma.eu/lasers-enigma-commit-hash.txt";
    private static LEUpdateNotifier instance;
    private static Instant lastVerificationTime;
    /**
     * The messages for administrators that joins the server in case of
     * available update
     */
    private final ArrayList<String> updateMessages;

    private LEUpdateNotifier() {
        updateMessages = new ArrayList<>();
        lastVerificationTime = Instant.now();
        this.loadUpdateNotifier();
    }

    public static LEUpdateNotifier getInstance() {
        if (instance == null) {
            instance = new LEUpdateNotifier();
        } else {
            if (Duration.between(instance.getLastVerificationTime(), Instant.now()).toDays() > 1) {
                instance.loadUpdateNotifier();
            }
            ;
        }
        return instance;
    }

    public Instant getLastVerificationTime() {
        return lastVerificationTime;
    }

    /**
     * @return the messages for administrators that joins the server in case of
     * available update
     */
    public ArrayList<String> getUpdateMessages() {
        return updateMessages;
    }

    private void loadUpdateNotifier() {
        updateMessages.clear();
        boolean shouldCheckForNewVersions = LasersEnigmaPlugin.getInstance().getConfig().getBoolean(CHECK_FOR_NEW_VERSIONS_CONFIG_NODE, true);
        boolean shouldCheckForNewBetaVersions = LasersEnigmaPlugin.getInstance().getConfig().getBoolean(CHECK_FOR_NEW_BETA_VERSIONS_CONFIG_NODE, false);
        String version = null;
        String commitHash = null;
        if (shouldCheckForNewVersions) {
            version = getStringFromResourceTxtFile("/lasers-enigma-version.txt");
        }
        if (shouldCheckForNewBetaVersions) {
            commitHash = getStringFromResourceTxtFile("/lasers-enigma-commit-hash.txt");
        }
        if (version != null) {
            String onlineVersion = getStringFromOnlineTxtFile(VERSION_NUMBER_FILE_URL);
            compareVersions(version, onlineVersion, "A new version of LasersEnigma is available.", "Installed version: ", "Available version: ");
        }
        if (commitHash != null) {
            String onlineCommitHash = getStringFromOnlineTxtFile(COMMIT_HASH_FILE_URL);
            compareVersions(commitHash, onlineCommitHash, "A new beta version of LasersEnigma is available.", "Installed version hashcode: ", "Available version hashcode: ");
        }

    }

    private void compareVersions(String version, String onlineVersion, String title, String s2, String s3) {
        if (!version.equals(onlineVersion)) {
            String installedVersionMsg = s2 + version;
            String availableVersionMsg = s3 + onlineVersion;
            LasersEnigmaPlugin.getInstance().getBetterLogger().warning(title);
            LasersEnigmaPlugin.getInstance().getBetterLogger().warning(installedVersionMsg);
            LasersEnigmaPlugin.getInstance().getBetterLogger().warning(availableVersionMsg);
            updateMessages.add(title);
            updateMessages.add(installedVersionMsg);
            updateMessages.add(availableVersionMsg);
        }
    }

    private String getStringFromResourceTxtFile(String file_uri) {
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        String result = null;
        String error_message = "Could not load " + file_uri + " file content from within the jar";
        try {
            is = getClass().getResourceAsStream(file_uri);
            if (is == null) {
                LasersEnigmaPlugin.getInstance().getBetterLogger().info(error_message);
                return null;
            }
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
            result = br.readLine();
        } catch (IOException ex) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().severe(error_message, ex);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, error_message, ex);
                }
            }
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException ex) {
                    LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, error_message, ex);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, error_message, ex);
                }
            }
        }
        return result;
    }

    private String getStringFromOnlineTxtFile(String file_url) {

        BufferedReader br = null;
        try {
            URL url = new URL(file_url);
            br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = br.readLine(); //only first line to read...
            br.close();
            return line;
        } catch (MalformedURLException ex) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.INFO, "Could not get " + file_url, ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, null, ex);
                }
            }
        }
        return "-";
    }

}
