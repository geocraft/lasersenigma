package eu.lasersenigma.stats;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ConfigData;
import eu.lasersenigma.player.LEPlayers;
import org.bstats.bukkit.Metrics;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LEBStatsConfig {

    public static void addCustomCharts(Metrics metrics) {
        metrics.addCustomChart(new Metrics.SimplePie("language", () -> LasersEnigmaPlugin.getInstance().getConfig().getString(ConfigData.LANGUAGE)));
        metrics.addCustomChart(new Metrics.SingleLineChart("nb_areas", () -> Areas.getInstance().getAreaSet().size()));
        metrics.addCustomChart(new Metrics.SingleLineChart("nb_components", () -> Areas.getInstance().getAreaSet().stream()
                .map(a -> a.getComponents().size())
                .reduce(Integer::sum).orElse(0))
        );
        metrics.addCustomChart(new Metrics.AdvancedPie("component_types", () -> {
            return Areas.getInstance().getAreaSet().stream()
                    .flatMap(area -> area.getComponents().stream())
                    .map(c -> c.getType().toString())
                    .collect(Collectors.groupingBy(Function.identity(),Collectors.summingInt(a -> 1)));
        }));
        metrics.addCustomChart(new Metrics.SingleLineChart("nb_areas_solved", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> player.getPerAreaRecords(null).size())
                .reduce(0, Integer::sum)
        ));
        metrics.addCustomChart(new Metrics.SingleLineChart("nb_actions_records", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> (int) player.getTotalNbActionDoneInSolvedAreasRecords(null))
                .reduce(0, Integer::sum)
        ));
        metrics.addCustomChart(new Metrics.SingleLineChart("nb_steps_records", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> (int) player.getTotalNbStepsInSolvedAreasRecords(null))
                .reduce(0, Integer::sum)
        ));
        metrics.addCustomChart(new Metrics.SingleLineChart("nb_minutes_records", ()
                -> LEPlayers.getInstance().getPlayers().values().stream()
                .map(player -> (int) player.getTotalTimeSpentInSolvedAreasRecords(null).toMinutes())
                .reduce(0, Integer::sum)
        ));
    }
}
