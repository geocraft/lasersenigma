package eu.lasersenigma.exceptions;

public class PlayerStatsNotFoundException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "player_stats_not_found";

    /**
     * Constructor
     */
    public PlayerStatsNotFoundException() {
        super(TRANSLATION_CODE);
    }

}
