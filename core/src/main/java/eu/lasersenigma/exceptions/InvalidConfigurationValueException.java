package eu.lasersenigma.exceptions;

public class InvalidConfigurationValueException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "invalid_config_value";

    /**
     * Constructor
     */
    public InvalidConfigurationValueException() {
        super(TRANSLATION_CODE);
    }

}
