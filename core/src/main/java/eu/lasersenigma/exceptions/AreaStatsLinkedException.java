package eu.lasersenigma.exceptions;

public class AreaStatsLinkedException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "stats_linked";

    /**
     * Constructor
     */
    public AreaStatsLinkedException() {
        super(TRANSLATION_CODE);
    }

}
