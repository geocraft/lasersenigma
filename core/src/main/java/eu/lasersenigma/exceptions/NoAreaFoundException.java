package eu.lasersenigma.exceptions;

public class NoAreaFoundException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "no_area_found";

    /**
     * Constructor
     */
    public NoAreaFoundException() {
        super(TRANSLATION_CODE);
    }

}
