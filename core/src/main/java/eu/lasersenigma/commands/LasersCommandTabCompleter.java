package eu.lasersenigma.commands;

import eu.lasersenigma.areas.CardinalDirection;
import eu.lasersenigma.config.Permission;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class LasersCommandTabCompleter implements TabCompleter {

    public static List<String> ARG_1_POSSIBILITIES =  Arrays.asList("menu", "area","copy","paste","save","load");
    public static List<String> ARG_AREA_POSSIBILITIES =  Arrays.asList("show","expand","contract");

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String alias, String[] args) {
        if (!command.getName().equals("lasers") || !Permission.EDIT.hasPermission(commandSender)) {
            return new ArrayList<>();
        }
        if (args.length == 0) {
            return ARG_1_POSSIBILITIES;
        } else if (args.length == 1) {
            List<String> possibilities = ARG_1_POSSIBILITIES.stream().filter(possibleArg -> possibleArg.startsWith(args[0])).collect(Collectors.toList());
            if (possibilities.size() == 0) {
                return ARG_1_POSSIBILITIES.stream().map(str -> ChatColor.RED + str).collect(Collectors.toList());
            }
            return possibilities;
        } else {
            switch (args[0]) {
                case "area":
                    if (args.length == 2) {
                        List<String> possibilities = ARG_AREA_POSSIBILITIES.stream().filter(possibleArg -> possibleArg.startsWith(args[1])).collect(Collectors.toList());
                        if (possibilities.size() == 0) {
                            return ARG_AREA_POSSIBILITIES.stream().map(str -> ChatColor.RED + str).collect(Collectors.toList());
                        }
                        return possibilities;
                    } else {
                        switch (args[1]) {
                            case "contract":
                            case "expand":
                                if (args.length == 3) {
                                    if (args[2].length() == 0) {
                                        return Collections.singletonList("<amount> [direction]");
                                    } else {
                                        try {
                                            Integer.parseInt(args[2]);
                                        } catch (NumberFormatException e) {
                                            return Collections.singletonList(ChatColor.RED + "<amount (must be a number)>");
                                        }
                                        return new ArrayList<>();
                                    }
                                } else if (args.length == 4) {
                                    List<String> allOptions = CardinalDirection.getAllNames();
                                    List<String> possibilities = allOptions.stream()
                                            .map(str -> str.toString().toLowerCase())
                                            .filter(str -> str.startsWith(args[3]))
                                            .collect(Collectors.toList());
                                    if (possibilities.size() == 0) {
                                        return allOptions.stream().map(str -> ChatColor.RED + str).collect(Collectors.toList());
                                    }
                                    return possibilities;
                                } else {
                                    return new ArrayList<>();
                                }
                            default:
                                return new ArrayList<>();
                        }
                    }
                case "save":
                case "load":
                    return args[1].length() == 0 ? Collections.singletonList("<schematic-name>") : new ArrayList<>();
                case "copy":
                case "paste":
                case "menu":
                default:
                    return new ArrayList<>();
            }
        }
    }
}
