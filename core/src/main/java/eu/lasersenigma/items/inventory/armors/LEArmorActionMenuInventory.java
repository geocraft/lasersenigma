package eu.lasersenigma.items.inventory.armors;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.items.ArmorAction;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemAttribute;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Skyhighjinks (alias Skyhighjinks) a.m.d.rogers@hotmail.co.uk
 */
public class LEArmorActionMenuInventory extends ALEOpenableInventory {

    public LEArmorActionMenuInventory(LEPlayer player) {
        super(player, Message.ARMOR_ACTION_MENU_TITLE);
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.ARMOR_ACTION_MENU_SELF_GIVE, Item.ARMOR_ACTION_MENU_GIVE_NEAREST_PLAYER))); // Not linked to CMD Block
        inv.add(new ArrayList<>(Arrays.asList(Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_FILLS_CHEST, Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_GIVE_NEAREST_PLAYER, Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_PUT_ARMOR_ON_NEAREST_PLAYER))); // Linked to CMD Block
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEArmorActionMenuInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        player.getInventoryManager().onSelectedArmor((ArmorAction) item.getAttributes().get(ItemAttribute.ARMOR_ACTION));
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.ARMOR_ACTION_MENU_SELF_GIVE,
                Item.ARMOR_ACTION_MENU_GIVE_NEAREST_PLAYER,
                Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_FILLS_CHEST,
                Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_GIVE_NEAREST_PLAYER,
                Item.ARMOR_ACTION_MENU_COMMAND_BLOCK_PUT_ARMOR_ON_NEAREST_PLAYER)).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.ARMOR_ACTION_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return null;
    }

}
