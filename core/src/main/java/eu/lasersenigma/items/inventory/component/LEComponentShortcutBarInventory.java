package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.*;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.attributes.RotationType;
import eu.lasersenigma.components.parents.*;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.exceptions.NotSameAreaException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.LEMainShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * Component edition shortcut bar
 */
public class LEComponentShortcutBarInventory extends ALEShortcutBarInventory {

    private final IComponent component;
    private final ComponentType componentType;

    public LEComponentShortcutBarInventory(LEPlayer player, IComponent component) {
        super(player);
        componentType = component.getType();
        this.component = component;
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        switch (componentType) {
            case LASER_SENDER:
                shortcutBar.addAll(getRotations());
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case LASER_RECEIVER:
                shortcutBar.addAll(getRotations());
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                break;
            case MIRROR_SUPPORT:
                IMirrorContainer mirrorSupport = (IMirrorContainer) component;
                if (mirrorSupport.hasMirror()) {
                    shortcutBar.addAll(getRotations());
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR);
                } else {
                    shortcutBar = addEmpty(shortcutBar, 5);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_PLACE_MIRROR);
                }
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case CONCENTRATOR:
                shortcutBar.addAll(getRotations());
                break;
            case REFLECTING_SPHERE:
            case FILTERING_SPHERE:
                IMirrorContainer reflectingSphere = (IMirrorContainer) component;
                if (reflectingSphere.hasMirror()) {
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR);
                } else {
                    shortcutBar.add(Item.EMPTY);
                    shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_PLACE_MIRROR);
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case MELTABLE_CLAY:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                break;
            case MIRROR_CHEST:
                int nbMirrors = ((MirrorChest) component).getNbMirrors();
                shortcutBar.add(nbMirrors <= 1 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB);
                shortcutBar.add(nbMirrors >= 99 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER);
                break;
            case REDSTONE_WINNER_BLOCK:
            case APPEARING_WINNER_BLOCK:
            case DISAPPEARING_WINNER_BLOCK:
                shortcutBar.addAll(getRanges(component));
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case MUSIC_BLOCK:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_NEXT_SONG);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_PREV_SONG);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_SONG_LOOP);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case LOCK:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ADD_KEYCHEST);
                shortcutBar.add(Item.EMPTY);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU);
                break;
            case KEY_CHEST:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER);
                shortcutBar.add(Item.EMPTY);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK);
                break;
            case ELEVATOR:
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CREATE_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS);
                shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER);
                break;
            case REDSTONE_SENSOR:
            case PRISM:
            case CALL_BUTTON:
                break;
            default:
                throw new UnsupportedOperationException();
        }

        shortcutBar = addEmptyUntil(shortcutBar, 7);

        shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_MENU);
        shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    private ArrayList<Item> getRotations() {
        return new ArrayList<>(Arrays.asList(
                Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_UP,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN
        ));
    }

    private ArrayList<Item> getRanges(IComponent component) {
        IDetectionComponent detectionComponent = (IDetectionComponent) component;
        ArrayList<Item> ranges = new ArrayList<>();
        int min = detectionComponent.getMin();
        int max = detectionComponent.getMax();
        ranges.add((min <= 0 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN));
        ranges.add((min >= 99 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN));
        ranges.add((max <= 0 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX));
        ranges.add((max >= 99 ? Item.EMPTY : Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX));
        return ranges;
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER,
                Item.COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR,
                Item.COMPONENT_SHORTCUTBAR_PLACE_MIRROR,
                Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB,
                Item.COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB,
                Item.COMPONENT_SHORTCUTBAR_NEXT_SONG,
                Item.COMPONENT_SHORTCUTBAR_PREV_SONG,
                Item.COMPONENT_SHORTCUTBAR_SONG_LOOP,
                Item.COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT,
                Item.COMPONENT_SHORTCUTBAR_MOD_LOOPER,
                Item.COMPONENT_SHORTCUTBAR_MENU,
                Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN,
                Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN,
                Item.COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX,
                Item.COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_UP,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN,
                Item.COMPONENT_SHORTCUTBAR_CLOSE,
                Item.COMPONENT_SHORTCUTBAR_ADD_KEYCHEST,
                Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU,
                Item.COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER,
                Item.COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK,
                Item.COMPONENT_SHORTCUTBAR_CREATE_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR,
                Item.COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS
        )).contains(item);
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEComponentShortcutBarInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        Area area = component.getArea();

        if (!area.containsLocation(player.getBukkitPlayer().getLocation())) {
            item = Item.COMPONENT_SHORTCUTBAR_CLOSE;
        }
        MirrorChest mc;
        switch (item) {
            case COMPONENT_SHORTCUTBAR_COLOR_LOOPER:
                ComponentController.changeColor(component.getArea(), (IColorableComponent) component, player);
                break;
            case COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR:
                ComponentController.getMirror((IMirrorContainer) component, player);
                break;
            case COMPONENT_SHORTCUTBAR_PLACE_MIRROR:
                ComponentController.placeMirror((IMirrorContainer) component, player, LasersColor.WHITE);
                break;
            case COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, (AttractionRepulsionSphere) component, true);
                break;
            case COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, (AttractionRepulsionSphere) component, false);
                break;
            case COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB:
                mc = (MirrorChest) component;
                ComponentController.increaseMirrorChest(mc, player);
                break;
            case COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB:
                ComponentController.decreaseMirrorChest((MirrorChest) component, player);
                break;
            case COMPONENT_SHORTCUTBAR_NEXT_SONG:
                ComponentController.nextSong(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_PREV_SONG:
                ComponentController.prevSong(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_SONG_LOOP:
                ComponentController.toogleLoop(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT:
                ComponentController.toogleStopOnExit(player, (MusicBlock) component);
                break;
            case COMPONENT_SHORTCUTBAR_MOD_LOOPER:
                if (component instanceof IDetectionComponent) {
                    ComponentController.changeMode(player, (IDetectionComponent) component);
                } else if (component instanceof AttractionRepulsionSphere) {
                    ComponentController.changeMode(player, (AttractionRepulsionSphere) component);
                } else if (component instanceof MirrorSupport) {
                    ComponentController.changeMirrorSupportMode(player, (MirrorSupport) component);
                }
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX:
                ComponentController.increaseMax(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX:
                ComponentController.decreaseMax(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN:
                ComponentController.increaseMin(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN:
                ComponentController.decreaseMin(player, (IDetectionComponent) component);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_LEFT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.LEFT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_RIGHT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.RIGHT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_UP:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.UP);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_DOWN:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.DOWN);
                break;
            case COMPONENT_SHORTCUTBAR_MENU:
                player.getInventoryManager().openLEInventory(new LEComponentMenuInventory(player, component));
                break;
            case COMPONENT_SHORTCUTBAR_CLOSE:
                player.getInventoryManager().openLEInventory(new LEMainShortcutBarInventory(player));
                break;
            case COMPONENT_SHORTCUTBAR_ADD_KEYCHEST:
                player.getInventoryManager().openLEInventory(new LEKeyChestCreationShortcutBarInventory(player, (Lock) component));
                break;
            case COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU:
                player.getInventoryManager().openLEInventory(new LEKeyChestTeleportationMenuInventory(player, (Lock) component));
                break;
            case COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER:
                player.getInventoryManager().openLEInventory(new LEKeyChestSelectorMenuInventory(player, (KeyChest) component));
                break;
            case COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK:
                player.teleportToNearestSafeLocation(((KeyChest) component).getLock().getLocation());
                break;
            case COMPONENT_SHORTCUTBAR_CREATE_FLOOR:
                ComponentController.elevatorCreateFloor(player, (Elevator) component);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR:
                Elevator elevator1 = (Elevator) component;
                int index1 = elevator1.getGroundFloorIndex();
                ComponentController.goToFloor(player, elevator1, index1);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR:
                Elevator elevator2 = (Elevator) component;
                int index2 = elevator2.getPrevFloorIndex();
                ComponentController.goToFloor(player, elevator2, index2);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR:
                Elevator elevator3 = (Elevator) component;
                int index3 = elevator3.getNextFloorIndex();
                ComponentController.goToFloor(player, elevator3, index3);
                break;
            case COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR:
                Elevator elevator4 = (Elevator) component;
                int index4 = elevator4.getTopFloorIndex();
                ComponentController.goToFloor(player, elevator4, index4);
                break;
            case COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS:
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
                    return;
                }
                Elevator elevator5 = (Elevator) component;
                Area area2 = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area2 == null || !area.isActivated()) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                if (area2 != elevator5.getArea()) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NotSameAreaException());
                    return;
                }
                List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(LEPlayer.TRANSPARENT_MATERIALS, 20);
                if (blocks.size() < 2) {
                    return;
                }
                ComponentController.addComponentFromBlockFace(ComponentType.CALL_BUTTON, blocks.get(1), blocks.get(1).getFace(blocks.get(0)), player, elevator5);
                break;
        }
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.COMPONENT_SHORTCUTBAR;
    }

    @Override
    public Area getArea() {
        return component.getArea();
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
