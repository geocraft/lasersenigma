package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemAttribute;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LEColorSelectorInventory extends ALEOpenableInventory {

    private final boolean isBlackIncluded;
    private final IComponent component;
    private final Area area;

    public LEColorSelectorInventory(LEPlayer player, Message titleMessage, boolean isBlackIncluded, IComponent component, Area area) {
        super(player, titleMessage);
        this.isBlackIncluded = isBlackIncluded;
        this.component = component;
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.EMPTY, Item.BLUE)));
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.MAGENTA, Item.WHITE, Item.LIGHT_BLUE)));
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.RED, Item.YELLOW, Item.GREEN)));
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY)));
        if (isBlackIncluded) {
            inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.EMPTY, Item.BLACK)));
        }
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEColorSelectorInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        LasersColor color = (LasersColor) item.getAttributes().get(ItemAttribute.COLOR);
        player.getInventoryManager().onColorSelected(color);
    }

    @Override
    public boolean contains(Item item) {
        ArrayList<Item> content = new ArrayList<>(Arrays.asList(Item.WHITE,
                Item.RED,
                Item.GREEN,
                Item.BLUE,
                Item.YELLOW,
                Item.MAGENTA,
                Item.LIGHT_BLUE
        ));
        if (isBlackIncluded) {
            content.add(Item.BLACK);
        }
        return content.contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.COLOR_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
