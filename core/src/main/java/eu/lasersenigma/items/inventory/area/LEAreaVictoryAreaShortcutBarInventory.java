package eu.lasersenigma.items.inventory.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.AreaController;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.LEMainShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Benjamin
 */
public class LEAreaVictoryAreaShortcutBarInventory extends ALEShortcutBarInventory {

    private final Area area;

    public LEAreaVictoryAreaShortcutBarInventory(LEPlayer player, Area area) {
        super(player);
        this.area = area;
    }

    @Override
    protected ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.VICTORY_AREA_SHORTCUTBAR_SELECT);
        shortcutBar = addEmpty(shortcutBar, 7);
        shortcutBar.add(Item.VICTORY_AREA_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case VICTORY_AREA_SHORTCUTBAR_CLOSE:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new LEMainShortcutBarInventory(player));
                }, 1);
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new LEAreaConfigMenuInventory(player, area));
                }, 3);
                break;
            case VICTORY_AREA_SHORTCUTBAR_SELECT:
                Location selectedLocation = player.getBukkitPlayer().getTargetBlock(LEPlayer.TRANSPARENT_MATERIALS, 20).getLocation();
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
                    return;
                }
                Area area2 = Areas.getInstance().getAreaFromLocation(selectedLocation);
                if (area2 == null || area != area2) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                AreaController.createVictoryArea(player, area, player.getBukkitPlayer().getTargetBlock(LEPlayer.TRANSPARENT_MATERIALS, 20).getLocation());
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.VICTORY_AREA_SHORTCUTBAR_SELECT, Item.VICTORY_AREA_SHORTCUTBAR_CLOSE)).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.AREA_VICTORY_AREAS;
    }

}
