package eu.lasersenigma.items.inventory.area.stats;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.exceptions.PlayerStatsNotFoundException;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.items.inventory.parents.ALEPaginableStatsOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.stats.PlayerStats;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

public final class LEAreaStatsActionListInventory extends ALEPaginableStatsOpenableInventory {

    private final Area area;

    public LEAreaStatsActionListInventory(LEPlayer player, Area area) {
        super(player, Message.STATS_ACTION_LIST_MENU_TITLE, getItemList(area));
        this.area = area;
    }

    private static ArrayList<ItemStack> getItemList(Area area) {
        ArrayList<ItemStack> result = new ArrayList<>();
        ItemsFactory itemFactory = ItemsFactory.getInstance();
        Iterator<Map.Entry<UUID, Integer>> it = area.getStats().getNbActionPlayersRecord().entrySet().iterator();
        int rank = 0;
        Integer lastStats = null;
        while (it.hasNext()) {
            Map.Entry<UUID, Integer> entry = it.next();
            try {
                if (!entry.getValue().equals(lastStats)) {
                    rank++;
                }
                lastStats = entry.getValue();
                PlayerStats playerStats = area.getStats().getStats(entry.getKey());
                result.add(itemFactory.getPlayerStatsItem(entry.getKey(), getRankAndStats(rank, playerStats, LEInventoryType.AREA_STATS_ACTION_MENU)));
            } catch (PlayerStatsNotFoundException ex) {
                LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "Player's stats not found. Player UUID=%s", entry.getKey());
                ErrorsConfig.logError(ex);
                result.add(itemFactory.getPlayerStatsItem(entry.getKey(), new StringBuilder(getRank(rank)).append(NEW_LINE_STR).append("error retrieving stats").toString()));
            }
        }
        return result;
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.AREA_STATS_ACTION_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }
}
