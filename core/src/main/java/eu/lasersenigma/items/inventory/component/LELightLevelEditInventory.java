package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.ILightComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LELightLevelEditInventory extends ALEOpenableInventory {

    private final ILightComponent component;
    private final Area area;

    public LELightLevelEditInventory(LEPlayer player, ILightComponent component) {
        super(player, Message.LIGHT_LEVEL_MENU_TITLE);
        this.component = component;
        this.area = component.getArea();
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        List<Item> firstLine = new ArrayList<>();

        firstLine.add(Item.LIGHT_LEVEL_DECREMENT);
        firstLine.addAll(getNumberAsItems(component.getLightLevel(), true));
        firstLine.add(Item.LIGHT_LEVEL_INCREMENT);

        inv.add(firstLine);
        return inv;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case LIGHT_LEVEL_DECREMENT:
                ComponentController.changeLightLevel(player, component, -1);
                break;
            case LIGHT_LEVEL_INCREMENT:
                ComponentController.changeLightLevel(player, component, 1);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return Arrays.asList(
                Item.LIGHT_LEVEL_DECREMENT,
                Item.LIGHT_LEVEL_INCREMENT
        ).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.LIGHT_LEVEL_EDIT_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
