package eu.lasersenigma.items.inventory.area.stats;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.AreaController;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LEAreaStatsLinkConfirmationInventory extends ALEOpenableInventory {

    private final Area area;

    private final Area area2;

    public LEAreaStatsLinkConfirmationInventory(LEPlayer player, Area area, Area area2) {
        super(player, Message.AREA_STATS_LINK_CONFIRM);
        this.area = area;
        this.area2 = area2;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.HELP_AREA_STATS_LINK_CONFIRM)));
        inv.add(new ArrayList<>(Arrays.asList(Item.CONFIRM_AREA_STATS_LINK, Item.EMPTY, Item.CANCEL_AREA_STATS_LINK)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEAreaStatsLinkConfirmationInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.ADMIN)) {
            return;
        }
        switch (item) {
            case CONFIRM_AREA_STATS_LINK:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                AreaController.statsLink(player, area, area2);
                break;
            case CANCEL_AREA_STATS_LINK:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.HELP_AREA_STATS_LINK_CONFIRM,
                Item.CONFIRM_AREA_STATS_LINK,
                Item.CANCEL_AREA_STATS_LINK
        )).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.AREA_STATS_LINK_CONFIRM;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
