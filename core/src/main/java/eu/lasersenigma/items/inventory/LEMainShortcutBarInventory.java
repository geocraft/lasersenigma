package eu.lasersenigma.items.inventory;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.AreaController;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.area.LEAreaConfigMenuInventory;
import eu.lasersenigma.items.inventory.area.LEDeleteAreaConfirmationInventory;
import eu.lasersenigma.items.inventory.area.stats.LEAreaStatsMenuInventory;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Main edition shortcut bar
 */
public class LEMainShortcutBarInventory extends ALEShortcutBarInventory {

    public LEMainShortcutBarInventory(LEPlayer player) {
        super(player);
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>(Arrays.asList(
                Item.MAIN_SHORTCUTBAR_PLACE_COMPONENT,
                Item.EMPTY,
                Item.MAIN_SHORTCUTBAR_AREA_CONFIG,
                Item.MAIN_SHORTCUTBAR_STATS_MENU,
                Item.EMPTY,
                Item.MAIN_SHORTCUTBAR_CREATE_AREA,
                Item.MAIN_SHORTCUTBAR_DELETE_AREA,
                Item.MAIN_SHORTCUTBAR_ARMOR_MENU,
                Item.MAIN_SHORTCUTBAR_CLOSE
        ));
        return shortcutBar;
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.MAIN_SHORTCUTBAR_PLACE_COMPONENT,
                Item.MAIN_SHORTCUTBAR_AREA_CONFIG,
                Item.MAIN_SHORTCUTBAR_STATS_MENU,
                Item.MAIN_SHORTCUTBAR_CREATE_AREA,
                Item.MAIN_SHORTCUTBAR_DELETE_AREA,
                Item.MAIN_SHORTCUTBAR_ARMOR_MENU,
                Item.MAIN_SHORTCUTBAR_CLOSE
        )).contains(item);
    }

    /**
     * @param item an item
     */
    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEMainShortcutBarInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        Area area;
        switch (item) {
            case MAIN_SHORTCUTBAR_AREA_CONFIG:
                area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
                    break;
                }
                player.getInventoryManager().openLEInventory(new LEAreaConfigMenuInventory(player, area));
                break;
            case MAIN_SHORTCUTBAR_CLOSE:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().exitEditionMode();
                }, 2);
                break;
            case MAIN_SHORTCUTBAR_CREATE_AREA:
                AreaController.createArea(player, player.getBukkitPlayer().getTargetBlock(LEPlayer.TRANSPARENT_MATERIALS, 20).getLocation());
                break;
            case MAIN_SHORTCUTBAR_STATS_MENU:
                area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
                    break;
                }
                player.getInventoryManager().openLEInventory(new LEAreaStatsMenuInventory(player, area));
                break;
            case MAIN_SHORTCUTBAR_DELETE_AREA:
                area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
                    break;
                }
                player.getInventoryManager().openLEInventory(new LEDeleteAreaConfirmationInventory(player, area));
                break;
            case MAIN_SHORTCUTBAR_ARMOR_MENU:
                player.getInventoryManager().onArmorMenu();
                break;
            case MAIN_SHORTCUTBAR_PLACE_COMPONENT:
                player.getInventoryManager().onPlaceComponent();
                break;
        }
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.MAIN_SHORTCUTBAR;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return null;
    }
}
