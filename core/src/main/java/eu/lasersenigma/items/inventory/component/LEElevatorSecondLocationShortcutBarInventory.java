package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.parents.IAreaComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.LEMainShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Select the second location of the elevator cage during Elevator creation shortcut bar
 */
public class LEElevatorSecondLocationShortcutBarInventory extends ALEShortcutBarInventory {

    private final Location firstLocation;
    private final Area area;

    public LEElevatorSecondLocationShortcutBarInventory(LEPlayer player, Area area, Location firstLocation) {
        super(player);
        this.area = area;
        this.firstLocation = firstLocation;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.ELEVATOR_SECOND_LOCATION);
        while (shortcutBar.size() < 8) {
            shortcutBar.add(Item.EMPTY);
        }
        shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER,
                Item.ELEVATOR_SECOND_LOCATION,
                Item.COMPONENT_SHORTCUTBAR_CLOSE
        )).contains(item);
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEComponentShortcutBarInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        Location secondLocation = player.getBukkitPlayer().getTargetBlock(LEPlayer.TRANSPARENT_MATERIALS, 20).getLocation();
        if (secondLocation == null) {
            return;
        }
        if (!area.containsLocation(secondLocation)) {
            item = Item.COMPONENT_SHORTCUTBAR_CLOSE;
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
        }
        switch (item) {
            case ELEVATOR_SECOND_LOCATION:
                IAreaComponent component = ComponentController.addIAreaComponent(player, firstLocation, secondLocation, ComponentType.ELEVATOR);
                player.getInventoryManager().openLEInventory(new LEComponentShortcutBarInventory(player, component));
                break;
            case COMPONENT_SHORTCUTBAR_CLOSE:
                player.getInventoryManager().openLEInventory(new LEMainShortcutBarInventory(player));
                break;
        }
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.ELEVATOR_SECOND_LOCATION_SHORTCUTBAR;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
