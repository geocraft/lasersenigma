package eu.lasersenigma.areas;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.CallButton;
import eu.lasersenigma.components.KeyChest;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.components.attributes.RotationType;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IAreaComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.ITaskComponent;
import eu.lasersenigma.exceptions.AreaCrossWorldsException;
import eu.lasersenigma.exceptions.AreaNoDepthException;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

/**
 * contains basics of an area
 * <p>
 * this abstract class was made to clear Area class code (partial equivalent)
 */
public abstract class AArea {

    public static final int LASER_DAMAGE_FREQUENCY = 5;

    public static final int LASERS_SHOW_FREQUENCY = 4;
    public static final int LASERS_LIGHT_UPDATE_FREQUENCY = 8;

    public static final int VICTORY_DETECTION_DELAY = 40;
    public static final int LIMIT_FOR_SHOWING_WHOLE_AREA = 70;
    /**
     * The world containing the area
     */
    protected final World world;
    /**
     * The components contained in this area
     */
    protected final HashSet<IComponent> components;
    protected final HashMap<Location, IComponent> componentByLocationCache = new HashMap<>();
    protected final HashSet<Location> locationsWithNoComponent = new HashSet<>();
    /**
     * The id of the area in database
     */
    protected int areaId;
    /**
     * The location of the corner of the area with the minimal coordinates
     */
    protected Location min;
    /**
     * The location of the corner of the area with the maximal coordinates
     */
    protected Location max;
    /**
     * If the area is activated or not. An area is actived when players are
     * inside it
     */
    protected boolean activated = false;
    /**
     * This will contain the locations where light needs to be added during the next update of the laser particles as
     * well as the level of brightness to be applied.
     */
    protected HashMap<Location, Integer> lightLevelPerLocations;
    /**
     * This will contain the locations where light was added during the previous laser particle update as well as the
     * brightness level that was applied.
     */
    protected HashMap<Location, Integer> previousLightLevelPerLocations;
    /**
     * The laser particles
     */
    protected HashSet<LaserParticle> laserParticles;
    /**
     * The new laser particles to add
     */
    protected HashSet<LaserParticle> laserParticlesToAdd;
    /**
     * Defines if the vertical rotation of mirrors is allowed in this area.
     */
    private boolean vMirrorRotationAllowed;
    /**
     * Defines if the horizontal rotation of mirrors is allowed in this area
     */
    private boolean hMirrorRotationAllowed;
    /**
     * Defines if the horizontal rotation of concentrator is allowed in this
     * area or not
     */
    private boolean vConcentratorRotationAllowed;
    /**
     * Defines if the verical rotation of concentrator is allowed in this area
     * or not
     */
    private boolean hConcentratorRotationAllowed;
    /**
     * Defines if the horizontal rotation of laser senders is allowed in this
     * area or not
     */
    private boolean vLaserSenderRotationAllowed;
    /**
     * Defines if the verical rotation of laser senders is allowed in this area
     * or not
     */
    private boolean hLaserSenderRotationAllowed;
    /**
     * Defines if the horizontal rotation of laser receivers is allowed in this
     * area or not
     */
    private boolean vLaserReceiverRotationAllowed;
    /**
     * Defines if the verical rotation of laser receivers is allowed in this
     * area or not
     */
    private boolean hLaserReceiverRotationAllowed;
    /**
     * Define if the resizing of attraction/repulsion spheres is allowed in this
     * area or not
     */
    private boolean attractionRepulsionSpheresResizingAllowed;
    /**
     * The area victory criteria
     */
    private DetectionMode victoryCriteria = DetectionMode.DETECTION_LASER_RECEIVERS;
    /**
     * The minimum amount of things that must be activated for the area to be
     * won
     */
    private int minimumRange = 1;
    /**
     * The minimum amount of things that must be activated for the area to be
     * won
     */
    private int maximumRange = 10;
    /**
     * Constructor
     *
     * @param a first location (not minimal location yet, simply a corner
     *          location)
     * @param b second location (not maximal location yet, simply the opposite
     *          corner location)
     * @throws AreaCrossWorldsException if the 2 locations are in different
     *                                  worlds
     * @throws AreaNoDepthException     if the area has no depth/width/height
     */
    public AArea(Location a, Location b) throws AreaCrossWorldsException, AreaNoDepthException {
        world = a.getWorld();
        if (world != b.getWorld()) {
            throw new AreaCrossWorldsException();
        }
        //Defining min and max location:
        double[] xCoordinates = new double[2];
        xCoordinates[0] = a.getBlockX();
        xCoordinates[1] = b.getBlockX();
        Arrays.sort(xCoordinates);

        double[] yCoordinates = new double[2];
        yCoordinates[0] = a.getBlockY();
        yCoordinates[1] = b.getBlockY();
        Arrays.sort(yCoordinates);

        double[] zCoordinates = new double[2];
        zCoordinates[0] = a.getBlockZ();
        zCoordinates[1] = b.getBlockZ();
        Arrays.sort(zCoordinates);

        if (xCoordinates[0] == xCoordinates[1] || yCoordinates[0] == yCoordinates[1] || zCoordinates[0] == zCoordinates[1]) {
            throw new AreaNoDepthException();
        }
        this.min = new Location(world, xCoordinates[0], yCoordinates[0], zCoordinates[0]);
        this.max = new Location(world, xCoordinates[1], yCoordinates[1], zCoordinates[1]);

        components = new HashSet<>();
        this.vMirrorRotationAllowed = true;
        this.hMirrorRotationAllowed = true;
        this.vConcentratorRotationAllowed = true;
        this.hConcentratorRotationAllowed = true;
        this.hLaserReceiverRotationAllowed = false;
        this.vLaserReceiverRotationAllowed = false;
        this.hLaserSenderRotationAllowed = false;
        this.hLaserSenderRotationAllowed = false;
        this.attractionRepulsionSpheresResizingAllowed = false;
        this.laserParticles = new HashSet<>();
        this.laserParticlesToAdd = new HashSet<>();
        this.lightLevelPerLocations = new HashMap<>();
        this.previousLightLevelPerLocations = new HashMap<>();
    }

    /**
     * checks if the area contains a location
     *
     * @param location the location to test
     * @param min      the minimum location of the area
     * @param max      the maximum location of the area
     * @return true if the area contains the given location
     */
    public static boolean containsLocation(Location location, Location min, Location max) {
        if (location.getBlockX() > max.getBlockX() || location.getBlockX() < min.getBlockX()) {
            return false;
        }
        if (location.getBlockY() > max.getBlockY() || location.getBlockY() < min.getBlockY()) {
            return false;
        }
        return !(location.getBlockZ() > max.getBlockZ() || location.getBlockZ() < min.getBlockZ());
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public final boolean isHMirrorsRotationAllowed() {
        return hMirrorRotationAllowed;
    }

    public final void setHMirrorsRotationAllowed(boolean hRotationAllowed) {
        this.hMirrorRotationAllowed = hRotationAllowed;
    }

    public final boolean isVMirrorsRotationAllowed() {
        return vMirrorRotationAllowed;
    }

    public final void setVMirrorsRotationAllowed(boolean vRotationAllowed) {
        this.vMirrorRotationAllowed = vRotationAllowed;
    }

    public final boolean isVConcentratorsRotationAllowed() {
        return vConcentratorRotationAllowed;
    }

    public final void setVConcentratorsRotationAllowed(boolean vConcentratorRotationAllowed) {
        this.vConcentratorRotationAllowed = vConcentratorRotationAllowed;
    }

    public final boolean isHConcentratorsRotationAllowed() {
        return hConcentratorRotationAllowed;
    }

    public final void setHConcentratorsRotationAllowed(boolean hConcentratorRotationAllowed) {
        this.hConcentratorRotationAllowed = hConcentratorRotationAllowed;
    }

    public final boolean isHLaserSendersRotationAllowed() {
        return hLaserSenderRotationAllowed;
    }

    public final void setHLaserSendersRotationAllowed(boolean hLaserSenderRotationAllowed) {
        this.hLaserSenderRotationAllowed = hLaserSenderRotationAllowed;
    }

    public final boolean isHLaserReceiversRotationAllowed() {
        return hLaserReceiverRotationAllowed;
    }

    public final void setHLaserReceiversRotationAllowed(boolean hLaserReceiverRotationAllowed) {
        this.hLaserReceiverRotationAllowed = hLaserReceiverRotationAllowed;
    }

    public final boolean isVLaserSendersRotationAllowed() {
        return vLaserSenderRotationAllowed;
    }

    public final void setVLaserSendersRotationAllowed(boolean vLaserSenderRotationAllowed) {
        this.vLaserSenderRotationAllowed = vLaserSenderRotationAllowed;
    }

    public final boolean isVLaserReceiversRotationAllowed() {
        return vLaserReceiverRotationAllowed;
    }

    public final void setVLaserReceiversRotationAllowed(boolean vLaserReceiverRotationAllowed) {
        this.vLaserReceiverRotationAllowed = vLaserReceiverRotationAllowed;
    }

    public final boolean isAttractionRepulsionSpheresResizingAllowed() {
        return attractionRepulsionSpheresResizingAllowed;
    }

    public final void setAttractionRepulsionSpheresResizingAllowed(boolean attractionRepulsionSpheresResizingAllowed) {
        this.attractionRepulsionSpheresResizingAllowed = attractionRepulsionSpheresResizingAllowed;
    }

    public final HashMap<Location, Integer> getLightLevelPerLocations() {
        return lightLevelPerLocations;
    }

    public DetectionMode getVictoryCriteria() {
        return victoryCriteria;
    }

    public void setVictoryCriteria(DetectionMode detectionMode) {
        this.victoryCriteria = detectionMode;
    }

    public int getMinimumRange() {
        return minimumRange;
    }

    public void setMinimumRange(int minimumRange) {
        this.minimumRange = minimumRange;
    }

    public int getMaximumRange() {
        return maximumRange;
    }

    public void setMaximumRange(int maximumRange) {
        this.maximumRange = maximumRange;
    }

    public final boolean isRotationAllowedWithoutPermissions(ComponentType componentType, RotationType rotationType) {
        switch (componentType) {
            case LASER_RECEIVER:
                switch (rotationType) {
                    case LEFT:
                    case RIGHT:
                        return isHLaserReceiversRotationAllowed();
                    case UP:
                    case DOWN:
                        return isVLaserReceiversRotationAllowed();
                    default:
                        throw new UnsupportedOperationException("rotation type should never be null");
                }
            case LASER_SENDER:
                switch (rotationType) {
                    case LEFT:
                    case RIGHT:
                        return isHLaserSendersRotationAllowed();
                    case UP:
                    case DOWN:
                        return isVLaserSendersRotationAllowed();
                    default:
                        throw new UnsupportedOperationException("rotation type should never be null");
                }
            case CONCENTRATOR:
                switch (rotationType) {
                    case LEFT:
                    case RIGHT:
                        return isHConcentratorsRotationAllowed();
                    case UP:
                    case DOWN:
                        return isVConcentratorsRotationAllowed();
                    default:
                        throw new UnsupportedOperationException("rotation type should never be null");
                }
            case MIRROR_SUPPORT:
                switch (rotationType) {
                    case LEFT:
                    case RIGHT:
                        return isHMirrorsRotationAllowed();
                    case UP:
                    case DOWN:
                        return isVMirrorsRotationAllowed();
                    default:
                        throw new UnsupportedOperationException("rotation type should never be null");
                }
            default:
                throw new UnsupportedOperationException("unsupported component type");
        }
    }

    /**
     * checks if the area contains a location
     *
     * @param location the location to test
     * @return true if the area contains the given location
     */
    public final boolean containsLocation(Location location) {
        return containsLocation(location, min, max);
    }

    /**
     * gets the players (not in spectator gamemode) inside the area without
     * using or updating cached data
     *
     * @return the players inside the area
     */
    public final HashSet<Player> getPlayersNoCache() {
        HashSet<Player> actualPlayersInside = new HashSet<>();
        int pX, pY, pZ;
        int minX = min.getBlockX();
        int minY = min.getBlockY();
        int minZ = min.getBlockZ();
        int maxX = max.getBlockX();
        int maxY = max.getBlockY();
        int maxZ = max.getBlockZ();
        for (Player player : min.getWorld().getPlayers()) {
            Location loc = player.getLocation();
            pX = loc.getBlockX();
            pY = loc.getBlockY();
            pZ = loc.getBlockZ();
            if (minX <= pX && pX <= maxX && minY <= pY && pY <= maxY && minZ <= pZ && pZ <= maxZ) {
                actualPlayersInside.add(player);
            }
        }
        return actualPlayersInside;
    }

    /**
     * gets the players (not in spectator gamemode) inside the area without
     * using or updating cached data
     *
     * @param players all players
     * @return the players inside the area which are not using spectator mode
     */
    protected final HashSet<Player> filterSpectators(HashSet<Player> players) {
        return players.stream()
                .filter(p -> p.getGameMode() != GameMode.SPECTATOR)
                .collect(Collectors.toCollection(HashSet::new));
    }

    /**
     * checks if the area is activated (contains players)
     *
     * @return true if the area contains player
     */
    public final boolean isActivated() {
        return activated;
    }

    /**
     * gets the minimal location (lowest corner in every axis)
     *
     * @return the minimal location (lowest corner in every axis)
     */
    public final Location getMinLocation() {
        return min;
    }

    /**
     * gets the maximal location (highest corner in every axis)
     *
     * @return the maximal location (highest corner in every axis)
     */
    public final Location getMaxLocation() {
        return max;
    }

    /**
     * gets the components of the location
     *
     * @return the components of the location
     */
    public final HashSet<IComponent> getComponents() {
        return components;
    }

    /**
     * removes the area
     */
    public final void remove() {
        components.stream().filter(c -> {
            //Let's delete linked components first
            if (c instanceof KeyChest || c instanceof CallButton) {
                c.remove();
                return false;
            }
            return true;
        }).forEach(IComponent::remove);
        components.clear();
    }

    /**
     * removes a component from the location
     *
     * @param component the component to remove
     */
    public final void removeComponent(IComponent component) {
        component.remove();
        components.remove(component);
        componentByLocationCache.entrySet().removeIf(e -> e.getValue() == component);
    }

    /**
     * adds a laser particle which will be added later in the main
     * laserParticles HashSet
     *
     * @param laserParticle a laser particle
     */
    public final void addLaserParticle(LaserParticle laserParticle) {
        if (laserParticlesToAdd.stream().anyMatch(p -> p.isDuplicate(laserParticle))) {
            return;
        }
        laserParticlesToAdd.add(laserParticle);
    }

    /**
     * Retrieves the area's laser particles
     *
     * @return the area's laser particles
     */
    public final HashSet<LaserParticle> getLaserParticles() {
        return this.laserParticles;
    }

    /**
     * gets a component from its location
     *
     * @param location the location of the component
     * @return null if no component were found, else the component.
     */
    @SuppressWarnings("null")
    public final IComponent getComponentFromLocation(Location location) {
        Location blockLocation = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
        if (locationsWithNoComponent.contains(blockLocation)) {
            return null;
        }
        IComponent cachedComponent = componentByLocationCache.get(blockLocation);
        if (cachedComponent == null) {
            cachedComponent = components.stream()
                    .filter(c -> {
                        if (c instanceof IAreaComponent) {
                            IAreaComponent areaComponent = (IAreaComponent) c;
                            return AArea.containsLocation(location, areaComponent.getMinLocation(), areaComponent.getMaxLocation());
                        } else {
                            return (c.getLocation().getWorld().getUID() == location.getWorld().getUID()
                                    && c.getLocation().getBlockX() == location.getBlockX()
                                    && c.getLocation().getBlockY() == location.getBlockY()
                                    && c.getLocation().getBlockZ() == location.getBlockZ());
                        }
                    })
                    .findAny().orElse(null);
            if (cachedComponent == null) {
                locationsWithNoComponent.add(blockLocation);
            } else {
                componentByLocationCache.put(blockLocation, cachedComponent);
            }
        }
        return cachedComponent;
    }

    public final IComponent getComponent(int componentId) {
        return components.stream()
                .filter(c -> c.getComponentId() == componentId)
                .findAny().orElse(null);
    }

    /**
     * checks if an area is overlapping this area
     *
     * @param area the other area which could overlapp the current area
     * @return true if the areas overlapps
     */
    public final boolean isOverlapping(Area area) {
        return this.isOverlapping(area.getMinLocation().getWorld(), area.getMinLocation().getBlockX(), area.getMinLocation().getBlockY(), area.getMinLocation().getBlockZ(), area.getMaxLocation().getBlockX(), area.getMaxLocation().getBlockY(), area.getMaxLocation().getBlockZ());
    }


    /**
     * checks if an area is overlapping this area
     *
     * @return true if the areas overlaps
     */
    public final boolean isOverlapping(World world, double minX, double minY, double minZ, double maxX, double maxY, double maxZ) {
        if (!this.getMinLocation().getWorld().getUID().equals(world.getUID())) {
            return false;
        }
        if (!intersectsDimension(min.getBlockX(), max.getBlockX(), minX, maxX)) {
            return false;
        }
        if (!intersectsDimension(min.getBlockY(), max.getBlockY(), minY, maxY)) {
            return false;
        }
        return intersectsDimension(min.getBlockZ(), max.getBlockZ(), minZ, maxZ);

    }

    /**
     * checks if an area is overlapping this area
     *
     * @param chunk the chunk which could overlapp the current area
     * @return true if the areas overlapps
     */
    public final boolean isOverlapping(Chunk chunk) {
        if (!chunk.getWorld().getName().equals(Objects.requireNonNull(min.getWorld()).getName())) {
            return false;
        }
        Location cMinLoc = new Location(chunk.getWorld(), chunk.getX() * 16, 0, chunk.getZ() * 16);
        Location cMaxLoc = new Location(chunk.getWorld(), chunk.getX() * 16 + 15, 255, chunk.getZ() * 16 + 15);
        if (!intersectsDimension(min.getBlockX(), max.getBlockX(), cMinLoc.getBlockX(), cMaxLoc.getBlockX())) {
            return false;
        }
        if (!intersectsDimension(min.getBlockY(), max.getBlockY(), cMinLoc.getBlockY(), cMaxLoc.getBlockY())) {
            return false;
        }
        return intersectsDimension(min.getBlockZ(), max.getBlockZ(), cMinLoc.getBlockZ(), cMaxLoc.getBlockZ());
    }

    /**
     * Checks if dimensions intersects
     *
     * @param aMin minimal value of the first area on a particular axis
     * @param aMax maximal value of the first area on a particular axis
     * @param bMin minimal value of the second area on a particular axis
     * @param bMax maximal value of the second area on a particular axis
     * @return true if the dimensions intersects
     */
    protected final boolean intersectsDimension(double aMin, double aMax, double bMin, double bMax) {
        return aMin <= bMax && aMax >= bMin;
    }

    public void addComponent(IComponent component, boolean update) {
        components.add(component);
        if (update) {
            component.updateDisplay();
        }
        if (component instanceof ITaskComponent) {
            ((ITaskComponent) component).startTask();
        }
        locationsWithNoComponent.clear();
    }

    public void addComponent(IComponent component) {
        addComponent(component, true);
    }

    /**
     * gets the location of this area's center
     *
     * @return the location of this area's center
     */
    public Location getCenter() {
        return new Location(this.min.getWorld(), min.getBlockX() + (max.getBlockX() - min.getBlockX()) / 2.0D, min.getBlockY() + (max.getBlockY() - min.getBlockY()) / 2.0D, min.getBlockZ() + (max.getBlockZ() - min.getBlockZ()) / 2.0D);
    }

    private boolean isLocationInChunk(Location loc, double minX, double maxX, double minZ, double maxZ) {
        if (loc.getBlockX() < minX || loc.getBlockX() > maxX) {
            return false;
        }

        return !(loc.getBlockZ() < minZ || loc.getBlockZ() > maxZ);
    }

    protected void removeAllASEntitiesInside() {
        double minX = min.getX() - 1;
        double minY = min.getY() - 1;
        double minZ = min.getZ() - 1;
        double maxX = max.getX() + 1;
        double maxY = max.getY() + 1;
        double maxZ = max.getZ() + 1;
        Objects.requireNonNull(min.getWorld()).getEntities().forEach(e -> {
            if (e.getType() == EntityType.ARMOR_STAND && e.getCustomName() != null && e.getCustomName().equals(AArmorStandComponent.ARMORSTAND_CUSTOM_NAME)) {
                Location l = e.getLocation();
                double asX = l.getX();
                double asY = l.getY();
                double asZ = l.getZ();
                if (minX <= asX && asX <= maxX && minY <= asY && asY <= maxY && minZ <= asZ && asZ <= maxZ) {
                    e.remove();
                }
            }
        });
    }

    public void show(Player player) {
        show(player, 20);
    }

    public void show(Player player, int nbShow) {
        final HashSet<Player> players = new HashSet<>(Collections.singleton(player));
        final HashSet<Location> particlesLocations = new HashSet<>();
        final double areaDiagonalSize = max.distance(min);
        int minX = min.getBlockX();
        int maxX = max.getBlockX() + 1;
        int minY = min.getBlockY();
        int maxY = max.getBlockY() + 1;
        int minZ = min.getBlockZ();
        int maxZ = max.getBlockZ() + 1;

        //Getting edges
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                for (int z = minZ; z <= maxZ; z++) {
                    boolean checkX = x == minX || x == maxX;
                    boolean checkY = y == minY || y == maxY;
                    boolean checkZ = z == minZ || z == maxZ;
                    boolean corner = false;

                    if (checkX && checkY && checkZ) {
                        corner = true;
                    }
                    if (!((checkX && checkY) || (checkX && checkZ) || (checkY && checkZ))) {
                        //not a edge
                        continue;
                    }
                    if (corner || areaDiagonalSize < LIMIT_FOR_SHOWING_WHOLE_AREA) { //we show only corners if the area is too big
                        particlesLocations.add(new Location(min.getWorld(), x, y, z));
                    }
                }
            }
        }

        //Getting diagonal
        for (int i = 0; i <= areaDiagonalSize; i++) {
            double x = minX + ((maxX - minX) / areaDiagonalSize) * i;
            double y = minY + ((maxY - minY) / areaDiagonalSize) * i;
            double z = minZ + ((maxZ - minZ) / areaDiagonalSize) * i;
            particlesLocations.add(new Location(min.getWorld(), x, y, z));
        }

        //Pour N, x = minX + ((maxX - minX) / areaDiagonalSize) * N
        //Pour N, x = minX + ((maxX - minX) / areaDiagonalSize) * N
        //Pour N, x = minX + ((maxX - minX) / areaDiagonalSize) * N


        particlesLocations.forEach(location -> {
            for (int delay = 1, i = 0; i < nbShow; i++) {
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> NMSManager.getNMS().playEffect(
                        players,
                        location,
                        "REDSTONE",
                        Color.ORANGE), delay);
                delay += 5;
            }
        });

    }

    public void clearLocationsWithNoComponentCache() {
        locationsWithNoComponent.clear();
    }
}
