package eu.lasersenigma.areas;

public enum AreaSizeModificationAction {
    CONTRACT, EXPAND
}
