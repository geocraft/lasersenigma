package eu.lasersenigma;

import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.commands.LasersCommandExecutor;
import eu.lasersenigma.commands.LasersCommandTabCompleter;
import eu.lasersenigma.config.*;
import eu.lasersenigma.database.Database;
import eu.lasersenigma.database.SQLDatabase;
import eu.lasersenigma.events.listeners.bukkit.*;
import eu.lasersenigma.events.listeners.bukkit.checkpoint.CheckpointClearOnPlayerQuitEventListener;
import eu.lasersenigma.events.listeners.bukkit.checkpoint.CheckpointPlayerChangedWorldEventListener;
import eu.lasersenigma.events.listeners.bukkit.checkpoint.CheckpointPlayerJoinEventListener;
import eu.lasersenigma.events.listeners.bukkit.checkpoint.CheckpointPlayerRespawnEventListener;
import eu.lasersenigma.events.listeners.bukkit.keylocks.KeyPlayerChangedWorldEventListener;
import eu.lasersenigma.events.listeners.bukkit.keylocks.KeyPlayerQuitEventListener;
import eu.lasersenigma.events.listeners.bukkit.keylocks.KeyPlayerRespawnEventListener;
import eu.lasersenigma.events.listeners.custom.PlayerEnteredAreaStatsInitializationListener;
import eu.lasersenigma.events.listeners.custom.PlayerLeavedAreaSongListener;
import eu.lasersenigma.events.listeners.custom.SongStopListener;
import eu.lasersenigma.events.listeners.dungeonsxl.EditWorldSaveDungeonsXLListener;
import eu.lasersenigma.events.listeners.dungeonsxl.InstanceWorldPostUnloadEventDungeonsXLListener;
import eu.lasersenigma.events.listeners.dungeonsxl.ResourceWorldInstantiateDungeonsXLListener;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.songs.PlayersListSongManager;
import eu.lasersenigma.stats.LEBStatsConfig;
import eu.lasersenigma.tasks.AreaTask;
import eu.lasersenigma.tasks.LaserTask;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;
import java.util.logging.Level;

/**
 * Main class of this plugin
 */
public class LasersEnigmaPlugin extends JavaPlugin {

    /**
     * the static instance of this plugin
     */
    private static LasersEnigmaPlugin lasersEnigmaPlugin;
    /**
     * the database
     */
    private Database db;
    /**
     * Logger
     */
    private BetterLogger logger;
    /**
     * the instance of the configuration manager to the translation custom
     * configuration
     */
    private ConfigManager translationConfig;

    /**
     * true if the plugin is disabling itself
     */
    private boolean disabling;

    /**
     * gets the only instance of this class
     *
     * @return the instance of this class
     */
    public static LasersEnigmaPlugin getInstance() {
        return lasersEnigmaPlugin;
    }

    /**
     * gets the translation custom configuration
     *
     * @return the translation custom configuration
     */
    public ConfigManager getTranslationConfig() {
        return this.translationConfig;
    }

    /**
     * gets the plugin database
     *
     * @return the plugin database
     */
    public Database getPluginDatabase() {
        return this.db;
    }

    /**
     * Retrieves a logger
     *
     * @return the logger
     */
    public BetterLogger getBetterLogger() {
        return logger;
    }

    /**
     * @return true if the plugin is currently being disabled.
     */
    public boolean isDisabling() {
        return disabling;
    }

    /**
     * The method called when the plugin is enabled
     */
    @Override
    @SuppressWarnings({"ResultOfObjectAllocationIgnored", "null"})
    public void onEnable() {
        LasersEnigmaPlugin.lasersEnigmaPlugin = this;
        this.disabling = false;

        //Config and translations
        loadConfig();

        //Logger
        this.logger = new BetterLogger(lasersEnigmaPlugin);
        getBetterLogger().setLogLevel(Level.parse(Objects.requireNonNull(getConfig().getString("debug_level", "INFO"))));

        //Check for new updates and notify in console.
        LEUpdateNotifier.getInstance();

        // Plugin usage statistics
        Metrics metrics = new Metrics(this);

        //Command handling
        CommandExecutor lasersCmdExecutor = new LasersCommandExecutor();
        Objects.requireNonNull(getCommand("lasers")).setExecutor(lasersCmdExecutor);
        TabCompleter lasersCmdTabCompleter = new LasersCommandTabCompleter();
        Objects.requireNonNull(getCommand("lasers")).setTabCompleter(lasersCmdTabCompleter);

        getBetterLogger().info("Connecting to database.");
        //Database
        this.db = new SQLDatabase(this);
        this.db.load();

        getBetterLogger().info("Loading plugin data from database.");
        Areas.getInstance().loadAreas();
        getBetterLogger().info("Data loading ended. Loaded worlds contains {0} areas.", String.valueOf(Areas.getInstance().getAreaSet().size()));

        //Event handlers
        loadEventHandlers();

        //TimerTask
        new AreaTask();
        new LaserTask();

        LEBStatsConfig.addCustomCharts(metrics);

        getBetterLogger().info("Initialization end.");
    }

    public boolean isLightAPIAvailable() {
        return Bukkit.getServer().getPluginManager().getPlugin("LightAPI") != null && this.getConfig().getBoolean("laser_light");
    }

    /**
     * the method called when the plugin is disabled
     */
    @Override
    public void onDisable() {
        this.disabling = true;
        Areas.getInstance().clearAreas();
    }

    /**
     * Reloads configurations
     */
    public void reloadAllConfigs() {
        loadConfig();
        ItemsFactory.reload();
        PlayersListSongManager.getInstance().reload();
    }

    private void loadConfig() {
        //LasersEnigmaPlugin Configuration
        ConfigData.initializeConfig();
        //Translation
        translationConfig = ConfigData.initializeTranslationConfig();
    }

    private void loadEventHandlers() {
        new PlayerQuitEventListener();
        new PlayerJoinEventListener();
        new BlockBreakEventListener();
        new BlockPlaceEventListener();
        new PlayerInteractEventListener();
        new InventoryClickEventListener();
        new PlayerPickupItemEventListener();
        new PlayerSwapHandItemsEventListener();
        new PlayerDropItemEventListener();
        new PlayerInteractAtEntityEventListener();
        new EntityDamageByEntityEventListener();
        new PlayerAnimationEventListener();
        new PlayerLeavedAreaSongListener();
        new SongStopListener();
        new BlockRedstoneEventListener();
        new PlayerEnteredAreaStatsInitializationListener();
        loadCheckpointEventHandlers();
        new KeyPlayerChangedWorldEventListener();
        new KeyPlayerQuitEventListener();
        new KeyPlayerRespawnEventListener();
        new PlayerJumpEventListener();
        new PlayerToggleSneekEventListener();
        new WorldLoadEventListener();
        new WorldUnloadEventListener();

        if (isLightAPIAvailable()) {
            getBetterLogger().log(Level.INFO, "LightAPI support enabled");
        }
        if (Bukkit.getPluginManager().isPluginEnabled("DungeonsXL")) {
            getBetterLogger().log(Level.INFO, "DungeonsXL support enabled");
            new EditWorldSaveDungeonsXLListener();
            new InstanceWorldPostUnloadEventDungeonsXLListener();
            new ResourceWorldInstantiateDungeonsXLListener();
        }
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void loadCheckpointEventHandlers() {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        GetBackToCheckpointOnDeath getBackToCheckpointOnDeath = GetBackToCheckpointOnDeath.fromString(LasersEnigmaPlugin.getInstance().getConfig().getString("get_back_to_checkpoint_on_death"));
        GetBackToCheckpointOnJoin getBackToCheckpointOnJoin = GetBackToCheckpointOnJoin.fromString(LasersEnigmaPlugin.getInstance().getConfig().getString("get_back_to_checkpoint_on_join"));
        GetBackToCheckpointOnChangeWorld getBackToCheckpointOnChangeWorld = GetBackToCheckpointOnChangeWorld.fromString(LasersEnigmaPlugin.getInstance().getConfig().getString("get_back_to_checkpoint_on_change_world"));
        if (getBackToCheckpointOnDeath != GetBackToCheckpointOnDeath.FALSE) {
            EventPriority playerRespawnEventPriority = CheckpointPriority.fromString(
                    Objects.requireNonNull(LasersEnigmaPlugin.getInstance().getConfig().getString("player_respawn_event_priority", "HIGH"))
            ).getPriority();

            CheckpointPlayerRespawnEventListener checkpointPlayerRespawnEventListener = new CheckpointPlayerRespawnEventListener(getBackToCheckpointOnDeath);

            this.getServer().getPluginManager().registerEvent(
                    PlayerRespawnEvent.class,
                    checkpointPlayerRespawnEventListener,
                    playerRespawnEventPriority,
                    (ll, event) -> ((CheckpointPlayerRespawnEventListener) ll).onPlayerRespawnEvent((PlayerRespawnEvent) event),
                    this
            );
        }
        if (getBackToCheckpointOnJoin != GetBackToCheckpointOnJoin.FALSE) {
            EventPriority playerJoinEventPriority = CheckpointPriority.fromString(
                    Objects.requireNonNull(LasersEnigmaPlugin.getInstance().getConfig().getString("player_join_event_priority", "HIGH"))
            ).getPriority();

            CheckpointPlayerJoinEventListener checkpointPlayerJoinEventListener = new CheckpointPlayerJoinEventListener(getBackToCheckpointOnJoin);

            this.getServer().getPluginManager().registerEvent(
                    PlayerJoinEvent.class,
                    checkpointPlayerJoinEventListener,
                    playerJoinEventPriority,
                    (ll, event) -> ((CheckpointPlayerJoinEventListener) ll).onPlayerJoinEvent((PlayerJoinEvent) event),
                    this
            );
        }
        EventPriority changedWorldEventPriority = CheckpointPriority.fromString(
                Objects.requireNonNull(LasersEnigmaPlugin.getInstance().getConfig().getString("player_changed_world_event_priority", "HIGH"))
        ).getPriority();
        boolean deleteCheckpointOnExitWorld = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("delete_checkpoint_on_exit_world");
        CheckpointPlayerChangedWorldEventListener checkpointPlayerChangedWorldEventListener = new CheckpointPlayerChangedWorldEventListener(getBackToCheckpointOnChangeWorld, deleteCheckpointOnExitWorld);

        this.getServer().getPluginManager().registerEvent(
                PlayerChangedWorldEvent.class,
                checkpointPlayerChangedWorldEventListener,
                changedWorldEventPriority,
                (ll, event) -> ((CheckpointPlayerChangedWorldEventListener) ll).onPlayerChangedWorld((PlayerChangedWorldEvent) event),
                this
        );
        boolean deleteCheckpointOnQuit = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("delete_checkpoint_on_leave");
        if (deleteCheckpointOnQuit) {
            new CheckpointClearOnPlayerQuitEventListener();
        }
    }


}
