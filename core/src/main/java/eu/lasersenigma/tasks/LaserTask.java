package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * the taks used to refresh lasers
 */
public class LaserTask extends BukkitRunnable {

    /**
     * Constructor
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public LaserTask() {
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 20, LaserParticle.LASERS_FREQUENCY);
    }

    /**
     * method called each times the task is executed
     */
    @Override
    public void run() {
        Areas.getInstance().updateLasers();
    }

}
