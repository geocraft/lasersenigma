package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.MirrorSupport;
import eu.lasersenigma.components.attributes.ArmorStandItemOffset;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * The task runned when a players retrieves a mirror from a reflecting sphere
 */
public class GetMirrorFromSupportTask extends BukkitRunnable {

    /**
     * the reflecting sphere
     */
    private final MirrorSupport mirrorSupport;

    private final Vector unclipVector;

    private final Vector exitVector;

    /**
     * the number of times this task will currently be executed
     */
    private int nbTimesRemaining;

    private Location mirrorASBaseLocation;

    private Location mirrorSupportASBaseLocation;

    private final boolean save;

    /**
     * Constructor
     *
     * @param mirrorSupport the MirrorSupport
     * @param save should the lack of mirror be saved in database
     */
    public GetMirrorFromSupportTask(MirrorSupport mirrorSupport, boolean save) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().fine("GetMirrorFromSupportTask.constructor");
        this.mirrorSupport = mirrorSupport;
        this.nbTimesRemaining = 30;
        this.save = save;
        Location mirrorSupportLoc = mirrorSupport.getLocation();
        mirrorSupportLoc.getWorld().playSound(mirrorSupportLoc, Sound.BLOCK_PORTAL_AMBIENT, 1, 2f);
        unclipVector = mirrorSupport.getAnimationClipVector().multiply(1d / 5);
        exitVector = mirrorSupport.getAnimationVector().multiply(1d / 25);
        mirrorASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorSupportLoc);
        mirrorSupportASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorSupportLoc, mirrorSupport.getFace()).add(mirrorSupport.getAnimationVector().multiply(-1d));
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 1, 0);
    }

    /**
     * the method called on each execution of the task
     */
    @Override
    public void run() {
        if (mirrorSupport.isRemoved()) {
            this.cancel();
            return;
        }
        if (nbTimesRemaining <= 0) {
            mirrorSupport.setOnGoingAnimation(false);
            mirrorSupport.setHasMirror(false, save);
            this.cancel();
            return;
        }
        if (nbTimesRemaining > 5) {
            mirrorASBaseLocation = mirrorASBaseLocation.add(exitVector);
            Location newLocationMirror = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotation());
            mirrorSupport.getArmorStandMirror().teleport(newLocationMirror);
            mirrorSupportASBaseLocation = mirrorSupportASBaseLocation.add(exitVector);
            Location newLocationMirrorSupport = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorSupportASBaseLocation,
                    ArmorStandItemOffset.HEAD,
                    mirrorSupport.getFace().getDefaultRotation(mirrorSupport.getType()));
            mirrorSupport.getArmorStandMirrorContainer().teleport(newLocationMirrorSupport);
        } else {
            if (nbTimesRemaining == 5) {
                mirrorSupport.getLocation().getWorld().playSound(mirrorASBaseLocation, Sound.BLOCK_WOODEN_DOOR_CLOSE, 1, 1.7f);
            }
            mirrorASBaseLocation = mirrorASBaseLocation.add(unclipVector);
            Location newLocation = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotation());
            mirrorSupport.getArmorStandMirror().teleport(newLocation);
        }
        nbTimesRemaining--;
    }

}
