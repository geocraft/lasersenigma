package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.Elevator;
import eu.lasersenigma.components.LaserReceiver;
import eu.lasersenigma.components.parents.IComponent;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;

/**
 * the task ran to play the defeat sound
 */
public class VictorySoundTask extends BukkitRunnable {

    /**
     * the sound played
     */
    private static final Sound SOUND = Sound.ENTITY_EXPERIENCE_ORB_PICKUP;
    /**
     * the pitch of the sound played
     */
    private static final float PITCH_BASE = 0.8f;
    /**
     * the modification applied to the pitch after each run
     */
    private static final float PITCH_MODIFICATION = 0.2f;
    /**
     * the area the sound must be played in
     */
    private final Area area;
    private final IComponent component;
    /**
     * the current pitch
     */
    private float pitch;
    /**
     * the current number of times the task must be ran
     */
    private int nbTimesRemaining;

    /**
     * Constructor
     *
     * @param area the area the sound must be played in
     */
    public VictorySoundTask(Area area) {
        this.area = area;
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 3);
        this.nbTimesRemaining = 3;
        this.pitch = PITCH_BASE;
        this.component = null;
    }

    public VictorySoundTask(Area area, IComponent component) {
        this.area = area;
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 3);
        this.nbTimesRemaining = 3;
        this.pitch = PITCH_BASE;
        this.component = component;
    }

    /**
     * the method called on each repeated execution
     */
    @Override
    public void run() {
        if (nbTimesRemaining == 0) {
            this.cancel();
            return;
        }
        int nbLaserReceiver = 0;
        if (component == null) {
            Iterator<IComponent> it = area.getComponents().iterator();
            while (it.hasNext()) {
                IComponent ccomponent = it.next();
                if (ccomponent instanceof LaserReceiver) {
                    Location componentLoc = ccomponent.getLocation();
                    componentLoc.getWorld().playSound(componentLoc, SOUND, 1, pitch);
                    nbLaserReceiver++;
                }
            }
        } else {
            Location componentLoc;
            if (component instanceof Elevator) {
                componentLoc = ((Elevator) component).getCageCenter();
            } else {
                componentLoc = component.getLocation();
            }
            componentLoc.getWorld().playSound(componentLoc, SOUND, 1, pitch);
        }
        if (nbLaserReceiver == 0) {
            this.area.getPlayers().forEach(p -> {
                Location loc = p.getLocation();
                loc.getWorld().playSound(loc, SOUND, 1, pitch);
            });
        }
        pitch = pitch + PITCH_MODIFICATION;
        nbTimesRemaining--;
    }
}
