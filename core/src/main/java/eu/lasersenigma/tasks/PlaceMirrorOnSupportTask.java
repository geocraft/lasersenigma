package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.MirrorSupport;
import eu.lasersenigma.components.attributes.ArmorStandItemOffset;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * The task runned when a players put a mirror on a reflecting sphere
 */
public class PlaceMirrorOnSupportTask extends BukkitRunnable {

    /**
     * the reflecting sphere
     */
    private final MirrorSupport mirrorSupport;

    private final Vector clipVector;

    private final Vector enterVector;

    /**
     * the number of times this task will currently be executed
     */
    private int nbTimesRemaining;

    private Location mirrorASBaseLocation;

    private Location mirrorSupportASBaseLocation;

    private final boolean save;

    /**
     * Constructor
     *
     * @param mirrorSupport the MirrorSupport
     * @param save should the presence of mirror be saved in database
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public PlaceMirrorOnSupportTask(MirrorSupport mirrorSupport, boolean save) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().fine("PlaceMirrorOnSupportTask.constructor");
        this.mirrorSupport = mirrorSupport;
        this.nbTimesRemaining = 30;
        this.save = save;
        Location mirrorSupportLoc = mirrorSupport.getLocation();
        mirrorSupportLoc.getWorld().playSound(mirrorSupportLoc, Sound.BLOCK_PORTAL_AMBIENT, 1, 2f);
        clipVector = mirrorSupport.getAnimationClipVector().multiply(-1d / 5);
        enterVector = mirrorSupport.getAnimationVector().multiply(-1d / 25);
        mirrorASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorSupportLoc).add(mirrorSupport.getAnimationClipVector()).add(mirrorSupport.getAnimationVector());
        mirrorSupportASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorSupportLoc, mirrorSupport.getFace());
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 1, 0);

    }

    /**
     * the method called on each execution of the task
     */
    @Override
    public void run() {
        if (mirrorSupport.isRemoved()) {
            this.cancel();
            return;
        }
        if (nbTimesRemaining <= 0) {
            mirrorSupport.setOnGoingAnimation(false);
            mirrorSupport.setHasMirror(true, save);
            this.cancel();
            return;
        }

        if (nbTimesRemaining > 25) {
            mirrorASBaseLocation = mirrorASBaseLocation.add(clipVector);
            Location newLocation = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotation());
            mirrorSupport.getArmorStandMirror().teleport(newLocation);
        } else {
            if (nbTimesRemaining == 25) {
                mirrorSupport.getLocation().getWorld().playSound(mirrorASBaseLocation, Sound.BLOCK_WOODEN_DOOR_CLOSE, 1, 1.7f);
            }
            mirrorASBaseLocation = mirrorASBaseLocation.add(enterVector);
            Location newLocationMirror = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorSupport.getRotation());
            mirrorSupport.getArmorStandMirror().teleport(newLocationMirror);
            mirrorSupportASBaseLocation = mirrorSupportASBaseLocation.add(enterVector);
            Location newLocationMirrorSupport = mirrorSupport.getArmorStandLocationWithOffsets(
                    mirrorSupportASBaseLocation,
                    ArmorStandItemOffset.HEAD,
                    mirrorSupport.getFace().getDefaultRotation(mirrorSupport.getType()));
            mirrorSupport.getArmorStandMirrorContainer().teleport(newLocationMirrorSupport);
        }
        nbTimesRemaining--;
    }

}
