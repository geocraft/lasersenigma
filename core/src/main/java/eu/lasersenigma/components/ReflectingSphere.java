package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.ReflectionData;
import eu.lasersenigma.tasks.GetMirrorFromSphereTask;
import eu.lasersenigma.tasks.PlaceMirrorOnSphereTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Reflecting sphere component
 */
public class ReflectingSphere extends AArmorStandComponent implements IColorableComponent, IMirrorContainer {

    /**
     * The difference between the location where the mirror appears and the
     * location where it starts entering inside the sphere
     */
    public static final double MIRROR_ANIMATION_CLIP_LOCATION_DIFF = 0.6d;
    /**
     * The difference between the location where the mirror starts entering the
     * sphere and the location where it stops and disappears
     */
    public static final double MIRROR_ANIMATION_LOCATION_DIFF = 0.7d;
    /**
     * Cache for lasers reflexion calculation
     */
    private final HashMap<ReflectionData, ReflectionData> reflexionCache;
    /**
     * does this reflecting sphere has a mirror inside
     */
    private boolean hasMirror;
    /**
     * the armor stand representing the mirror
     */
    private ArmorStand armorStandMirror;
    /**
     * The armorstand representing the reflecting sphere
     */
    private ArmorStand armorStandReflectingSphere;
    /**
     * Is this reflecting sphere currently showing an animation
     */
    private boolean onGoingAnimation;
    /**
     * The color of this reflecting sphere
     */
    private LasersColor color;

    /**
     * The saved color of this component's mirror. Used only on reset.
     */
    private LasersColor savedColor;


    private PlaceMirrorOnSphereTask placeMirrorOnSphereTask;
    private GetMirrorFromSphereTask getMirrorFromSphereTask;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this component
     * @param componentId the id of the component inside the database
     * @param location    the location of this component
     * @param face        the face the component is on
     */
    public ReflectingSphere(Area area, int componentId, Location location, ComponentFace face, LasersColor savedColor) {
        super(area, componentId, location, ComponentType.REFLECTING_SPHERE, face, face.getDefaultRotation(ComponentType.REFLECTING_SPHERE));
        hasMirror = savedColor != null;
        color = savedColor != null ? savedColor : LasersColor.BLACK;
        this.savedColor = savedColor;
        onGoingAnimation = false;
        reflexionCache = new HashMap<>();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this component
     * @param location the location of this component
     * @param face     the face the component is on
     */
    public ReflectingSphere(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.REFLECTING_SPHERE, face);
        hasMirror = true;
        onGoingAnimation = false;
        color = LasersColor.WHITE;
        savedColor = null;
        reflexionCache = new HashMap<>();
        updateDisplay();
        dbCreate();
    }

    @Override
    public ArmorStand getArmorStandMirror() {
        return armorStandMirror;
    }

    @Override
    public ArmorStand getArmorStandMirrorContainer() {
        return armorStandReflectingSphere;
    }

    public HashMap<ReflectionData, ReflectionData> getReflexionCache() {
        return reflexionCache;
    }

    /**
     * deletes this component
     */
    @Override
    public void hide() {
        super.hide();
        if (onGoingAnimation && getMirrorFromSphereTask != null) {
            getMirrorFromSphereTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSphereTask != null) {
            placeMirrorOnSphereTask.cancel();
        }
        onGoingAnimation = false;
        //Retrieving armorstands if required
        armorStandReflectingSphere = findArmorStandBack(armorStandReflectingSphere);
        armorStandMirror = findArmorStandBack(armorStandMirror);
        if (armorStandReflectingSphere != null) {
            armorStandReflectingSphere.remove();
            armorStandReflectingSphere = null;
        }
        if (armorStandMirror != null) {
            armorStandMirror.remove();
            armorStandMirror = null;
        }
    }

    /**
     * checks if an animation is currently on going
     *
     * @return true if an animation is currently shown
     */
    public boolean getOnGoingAnimation() {
        return onGoingAnimation;
    }

    /**
     * sets if this component is currently animated or not
     *
     * @param onGoingAnimation if this component is currently animated
     */
    @Override
    public void setOnGoingAnimation(boolean onGoingAnimation) {
        this.onGoingAnimation = onGoingAnimation;
    }

    /**
     * does this component currently has a mirror on it
     *
     * @return true if this component has a mirror on it
     */
    @Override
    public boolean hasMirror() {
        return hasMirror;
    }

    /**
     * Sets if this component currently has a mirror on it
     *
     * @param save      should the lack/presence of mirror be saved in database
     * @param hasMirror if this component currently has a mirror on it or not
     */
    @Override
    public void setHasMirror(boolean hasMirror, boolean save) {
        if (onGoingAnimation) {
            return;
        }
        this.hasMirror = hasMirror;
        if (!hasMirror) {
            color = LasersColor.BLACK;
        }
        updateDisplay();
        if (save) {
            savedColor = hasMirror ? color : null;
            dbUpdate();
        }
    }

    @Override
    public boolean hasSavedMirror() {
        return savedColor != null;
    }

    /**
     * retrieves the mirror from this component
     *
     * @param save should the lack of mirror be saved in database
     * @return true if the mirror has been retrieved successfully
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean getMirror(boolean save) {
        if (!onGoingAnimation && hasMirror && !this.isRemoved()) {
            onGoingAnimation = true;
            // Animation
            getMirrorFromSphereTask = new GetMirrorFromSphereTask(this, Item.REFLECTING_SPHERE, save);
            return true;
        }
        return false;
    }

    /**
     * place a mirror on this component
     *
     * @param color the color of the mirror retrieved
     * @param save  should the presence of mirror be saved in database
     * @return true is the mirror has been placed successfully
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean placeMirror(LasersColor color, boolean save) {
        if (!onGoingAnimation && !hasMirror && !this.isRemoved() && armorStandMirror == null) {
            onGoingAnimation = true;
            this.color = color;
            createMirrorArmorStand();
            placeMirrorOnSphereTask = new PlaceMirrorOnSphereTask(this, Item.getReflectingSphereItem(color), save);
            return true;
        }
        return false;
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
        armorStandReflectingSphere = findArmorStandBack(armorStandReflectingSphere);
        armorStandMirror = findArmorStandBack(armorStandMirror);
        if (armorStandReflectingSphere == null) {
            createReflectingSphereArmorStand();
        }
        if (hasMirror && armorStandMirror == null) {
            createMirrorArmorStand();
        } else if (!hasMirror && armorStandMirror != null) {
            armorStandMirror.remove();
            Areas.getInstance().removeEntity(armorStandMirror.getUniqueId());
            armorStandMirror = null;
        }
        //Rotation, head and location of mirror support armor stand
        Rotation sphereRotation = face.getDefaultRotation(ComponentType.REFLECTING_SPHERE);

        armorStandReflectingSphere.setHeadPose(sphereRotation);
        armorStandReflectingSphere.setHelmet(ItemsFactory.getInstance().getItemStack(Item.getReflectingSphereItem(color)));

        Location sphereLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                sphereRotation);
        armorStandReflectingSphere.teleport(sphereLoc);

        //Rotation, head and location of mirror armor stand
        if (hasMirror) {
            armorStandMirror.setHeadPose(rotation);
            armorStandMirror.setHelmet(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
            Location mirrorLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.GLASS_PANE, face.getDefaultRotation(getType()));
            armorStandMirror.teleport(mirrorLoc);
        }
    }

    /**
     * Creates the armorstand corresponding to the component
     */
    private void createReflectingSphereArmorStand() {
        Location armorStandReflectingSphereLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                face.getDefaultRotation(getType())
        );
        armorStandReflectingSphere = createArmorStand(armorStandReflectingSphereLoc, rotation, Item.getReflectingSphereItem(color));
        Areas.getInstance().addEntity(armorStandReflectingSphere.getUniqueId(), this);
    }

    /**
     * Creates the armorstand corresponding to the component
     */
    private void createMirrorArmorStand() {
        Location armorStandMirrorLoc = getDefaultArmorStandBaseLocation(getLocation(), face);
        if (!hasMirror()) {
            armorStandMirrorLoc.add(getAnimationClipVector()).add(getAnimationVector());
        }
        armorStandMirrorLoc = getArmorStandLocationWithOffsets(
                armorStandMirrorLoc,
                ArmorStandItemOffset.GLASS_PANE,
                face.getDefaultRotation(getType())
        );
        armorStandMirror = createArmorStand(armorStandMirrorLoc, rotation, Item.getMirror(color));
        Areas.getInstance().addEntity(armorStandMirror.getUniqueId(), this);
    }

    @Override
    public final Vector getAnimationVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_LOCATION_DIFF);
    }

    @Override
    public final Vector getAnimationClipVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_CLIP_LOCATION_DIFF);
    }

    /**
     * Rotates the mirror on this component
     */
    @Override
    @Deprecated
    public void rotate(RotationType rotationType) {
    }

    /**
     * Resets the component
     */
    @Override
    public void reset() {
        if (onGoingAnimation && getMirrorFromSphereTask != null) {
            getMirrorFromSphereTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSphereTask != null) {
            placeMirrorOnSphereTask.cancel();
        }
        onGoingAnimation = false;
        color = hasSavedMirror() ? savedColor : LasersColor.BLACK;
        hasMirror = hasSavedMirror();
        updateDisplay();
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of ReflectingSpheres is defined by its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(color.getNextColor(false), save);
    }

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        setColor(color.getNextColor(false));
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, false);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is this mirror color saved in database
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }
        if (!hasMirror || onGoingAnimation) {
            return;
        }
        this.color = color;
        updateDisplay();
        if (save) {
            this.savedColor = color;
            dbUpdate();
        }
    }

    /**
     * gets the color of this component
     *
     * @return the color of this component
     */
    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getSavedColor() {
        return savedColor;
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandReflectingSphere = null;
        armorStandMirror = null;
    }

    @Override
    public boolean canBeDeleted() {
        return !onGoingAnimation;
    }
}
