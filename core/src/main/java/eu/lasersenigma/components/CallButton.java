package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.items.Item;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

/**
 * Call button component for elevators
 */
public final class CallButton extends AArmorStandComponent {

    /**
     * The corresponding elevator
     */
    private final Elevator elevator;
    /**
     * The armorStand representing the call button
     */
    private ArmorStand armorStandCallButton = null;

    /**
     * Constructor used for creation from the database
     *
     * @param area        the area containing this call button
     * @param componentId the id of the component inside the database
     * @param location    the location of the call button
     * @param face        the blockface of the component
     * @param rotation    the rotation of the component
     * @param elevator    the corresponding elevator
     */
    public CallButton(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, Elevator elevator) {
        super(area, componentId, location, ComponentType.CALL_BUTTON, face, rotation);
        this.elevator = elevator;
    }

    /**
     * Constructor
     *
     * @param area     the area containing this call button
     * @param location the location of the component
     * @param face     the blockface of the component
     * @param elevator the corresponding elevator
     */
    public CallButton(Area area, Location location, ComponentFace face, Elevator elevator) {
        super(area, location, ComponentType.CALL_BUTTON, face);
        this.elevator = elevator;
        dbCreate();
    }

    public Elevator getElevator() {
        return this.elevator;
    }

    public boolean onCallButton(boolean isEditingPuzzle) {
        return elevator.onCallButtonPushed(this, isEditingPuzzle);
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandCallButton != null) {
            Areas.getInstance().removeEntity(armorStandCallButton.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandCallButton.getUniqueId())) {
                    armorStandCallButton = (ArmorStand) entity;
                }
            }
            armorStandCallButton.remove();
            armorStandCallButton = null;
        }
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        armorStandCallButton = findArmorStandBack(armorStandCallButton);
        if (armorStandCallButton == null) {
            createCallButtonArmorStand();
        } else {
            armorStandCallButton.setHeadPose(rotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, rotation);
            armorStandCallButton.teleport(nextLoc);
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    /**
     * Creates the call button armorstand
     */
    private void createCallButtonArmorStand() {
        Location armorStandCallButtonLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandCallButton = createArmorStand(armorStandCallButtonLoc, rotation, Item.CALL_BUTTON);
        Areas.getInstance().addEntity(armorStandCallButton.getUniqueId(), this);
    }

    /**
     * Rotates the component (disabled)
     */
    @Deprecated
    @Override
    public void rotate(RotationType rotationType) {
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandCallButton = null;
    }
}
