package eu.lasersenigma.components;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.*;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

/**
 * Laser sender component
 */
public class LaserSender extends AArmorStandComponent implements IColorableComponent, IDetectionComponent, IRotatableComponent, ILightComponent, IPlayerModifiableComponent {

    /**
     * the color of this component as saved in database
     */
    private LasersColor color;

    /**
     * current color of this component (not saved in the database)
     */
    private LasersColor currentColor;

    /**
     * The armorStand representing the laser sender
     */
    private ArmorStand armorStandLaserSender = null;

    /**
     * If the laser sender is conditionnal (redstone signal / range of activated
     * laser receivers / number of player inside the area) or not.
     */
    private DetectionMode mode;

    /**
     * the minmum number of [activated laser receivers | players inside the
     * area] needed to activate this component
     */
    private int min;

    /**
     * the maximum number of [activated laser receivers | players inside the
     * area] needed to activate this component
     */
    private int max;

    /**
     * The actual number of (laser receivers activated | players inside the
     * area)
     */
    private int nbActivated;

    private boolean powered;

    private int lightLevel;

    private Rotation currentRotation;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area of the laser sender
     * @param componentId the id of the component inside the database
     * @param location    the location of the laser sender
     * @param face        the face of the laser sender
     * @param rotation    the rotation of the laser sender
     * @param color       the color of the laser sender
     * @param min         The minimum number of activated laser receivers | players
     *                    needed to make this block appear
     * @param max         The maximum number of activated laser receivers | players
     *                    needed to make this block appear
     * @param mode        the detection mode
     */
    public LaserSender(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, LasersColor color, DetectionMode mode, int min, int max, int lightLevel) {
        super(area, componentId, location, ComponentType.LASER_SENDER, face, rotation);
        this.color = color;
        currentColor = color;
        this.mode = mode;
        this.min = min;
        this.max = max;
        this.nbActivated = -1;
        this.powered = false;
        this.lightLevel = lightLevel;
        currentRotation = getRotation();
    }

    /**
     * Constructor
     *
     * @param area     the area of the laser sender
     * @param location the location of the laser sender
     * @param face     the face of the laser sender
     */
    public LaserSender(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.LASER_SENDER, face);
        color = LasersColor.WHITE;
        currentColor = color;
        mode = DetectionMode.PERMANENTLY_ENABLED;
        min = 1;
        max = 10;
        nbActivated = -1;
        this.powered = false;
        this.lightLevel = LasersEnigmaPlugin.getInstance().getConfig().getInt("laser_default_light_level");
        currentRotation = getRotation();
        dbCreate();
    }

    public Rotation getCurrentRotation() {
        return currentRotation;
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        updateDisplay();
        dbUpdate();
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        updateDisplay();
        dbUpdate();
    }

    public void setPowered(boolean powered) {
        if (powered != this.powered) {
            this.powered = powered;
            updateDisplay();
        }
    }

    @Override
    public boolean isActivated() {
        switch (mode) {
            case DETECTION_PLAYERS:
            case DETECTION_LASER_RECEIVERS:
            case DETECTION_REDSTONE_SENSORS:
            case DETECTION_LOCKS:
                return (this.min <= this.nbActivated && this.nbActivated <= this.max);
            case DETECTION_REDSTONE:
                return powered;
            default:
                return true;
        }
    }

    /**
     * resets this component
     */
    @Override
    public void reset() {
        if (!rotation.equals(currentRotation)) {
            armorStandLaserSender.setHeadPose(currentRotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
            armorStandLaserSender.teleport(nextLoc);
        }
        this.nbActivated = 0;
        currentRotation = rotation;
        currentColor = color;
    }

    /**
     * sets the current number of laser receivers activated
     *
     * @param nbActivated the current number of laser receivers activated
     */
    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated && mode != DetectionMode.PERMANENTLY_ENABLED) {
            this.nbActivated = nbActivated;
            updateDisplay();
        }
    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.mode = mode;
        dbUpdate();
        updateDisplay();
    }

    @Override
    public DetectionMode getMode() {
        return mode;
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandLaserSender != null) {
            Areas.getInstance().removeEntity(armorStandLaserSender.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandLaserSender.getUniqueId())) {
                    armorStandLaserSender = (ArmorStand) entity;
                }
            }
            armorStandLaserSender.remove();
            armorStandLaserSender = null;
        }
        currentRotation = rotation;
    }

    /**
     * updates the display
     */
    @Override
    public void updateDisplay() {
        armorStandLaserSender = findArmorStandBack(armorStandLaserSender);
        if (armorStandLaserSender == null) {
            createLaserSenderArmorStand();
        } else {
            armorStandLaserSender.setHeadPose(currentRotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
            armorStandLaserSender.teleport(nextLoc);
            armorStandLaserSender.setHelmet(ItemsFactory.getInstance().getItemStack(getItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    /**
     * Creates the armor stand corresponding to the mirror support
     */
    private void createLaserSenderArmorStand() {
        Location armorStandLaserSenderLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                currentRotation
        );
        armorStandLaserSender = createArmorStand(armorStandLaserSenderLoc, currentRotation, getItem());
        Areas.getInstance().addEntity(armorStandLaserSender.getUniqueId(), this);
    }

    private Item getItem() {
        switch (mode) {
            case DETECTION_PLAYERS:
            case DETECTION_LASER_RECEIVERS:
            case DETECTION_REDSTONE_SENSORS:
            case DETECTION_LOCKS:
                if (isActivated()) {
                    return Item.LASER_SENDER_CONDITIONNAL_RANGE_ACTIVATED;
                }
                return Item.LASER_SENDER_CONDITIONNAL_RANGE_DEACTIVATED;
            case DETECTION_REDSTONE:
                if (isActivated()) {
                    return Item.LASER_SENDER_CONDITIONNAL_REDSTONE_ACTIVATED;
                }
                return Item.LASER_SENDER_CONDITIONNAL_REDSTONE_DEACTIVATED;
            default:
                return Item.LASER_SENDER;
        }
    }

    /**
     * Rotates the laser sender
     *
     * @param rotationType the type of the rotation
     * @param save         if the rotation must be saved or not
     */
    @Override
    public void rotate(RotationType rotationType, boolean save) {
        rotate(rotationType, save, true);
    }

    /**
     * Rotates the mirror on this laser sender
     *
     * @param rotationType the type of the rotation
     * @param save         if the rotation must be saved or not
     * @param playSound    if the rotation sound should be played
     */
    @Override
    public void rotate(RotationType rotationType, boolean save, boolean playSound) {
        currentRotation = currentRotation.getNextRotation(rotationType);
        armorStandLaserSender.setHeadPose(currentRotation);
        Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
        armorStandLaserSender.teleport(nextLoc);
        if (playSound)
            NMSManager.getNMS().playSound(armorStandLaserSender.getLocation(), "ENTITY_IRONGOLEM_ATTACK", 1f, 0.7f);
        if (save) {
            rotation = currentRotation;
            dbUpdate();
        }
    }


    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        changeColor(true);
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of FilteringSpheres is defined by
     *             its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(currentColor.getNextColor(false), save);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is ignored since the color of FilteringSpheres is defined by
     *              its mirror so it's never saved in database.
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }
        this.currentColor = color;
        if (save) {
            this.color = color;
        }
        if (save) {
            dbUpdate();
        }
    }

    /**
     * gets the color of the laser sender
     *
     * @return the color of this laser sender
     */
    @Override
    public LasersColor getColor() {
        return this.currentColor;
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, true);
    }

    @Override
    public LasersColor getSavedColor() {
        return color;
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandLaserSender = null;
    }

    @Override
    public int getLightLevel() {
        return lightLevel;
    }

    @Override
    public void setLightLevel(int lightLevel) {
        this.lightLevel = lightLevel;
    }
}
