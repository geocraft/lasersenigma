package eu.lasersenigma.components.attributes;

import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.util.Vector;

public class Direction extends Vector implements Cloneable {

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Direction(double radialYaw, double radialPitch) {
        super(Math.sin(radialPitch) * Math.cos(radialYaw), Math.sin(radialPitch) * Math.sin(radialYaw), Math.cos(radialPitch));
        normalize();
        multiply(LaserParticle.LASERS_SPEED);
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Direction(double x, double y, double z) {
        super(x, y, z);
        normalize();
        multiply(LaserParticle.LASERS_SPEED);
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Direction(Vector v) {
        super(v.getX(), v.getY(), v.getZ());
        normalize();
        multiply(LaserParticle.LASERS_SPEED);
    }

    public boolean isAcceptedInputAngleFor(Vector inputAngle, double maxRadialAngle) {
        Float angle = this.clone().multiply(-1).angle(inputAngle);
        return (Float.isNaN(angle) || angle < maxRadialAngle);
    }

    @Override
    public Direction clone() {
        Object o = super.clone();
        return (Direction) o;
    }
}
