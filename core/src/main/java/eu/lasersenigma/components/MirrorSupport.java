package eu.lasersenigma.components;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.*;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.nms.v1_13_R2.ParticleMapping;
import eu.lasersenigma.particles.ReflectionData;
import eu.lasersenigma.tasks.GetMirrorFromSupportTask;
import eu.lasersenigma.tasks.PlaceMirrorOnSupportTask;
import org.apache.maven.settings.Mirror;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Mirror support component
 */
public class MirrorSupport extends AArmorStandComponent implements IColorableComponent, IMirrorContainer, IRotatableComponent, IPlayerModifiableComponent {

    public static final double MIRROR_ANIMATION_LOCATION_DIFF = 0.275d;
    public static final double MIRROR_ANIMATION_CLIP_LOCATION_DIFF = 0.6d;
    /**
     * Cache for lasers reflexion calculation
     */
    private final HashMap<ReflectionData, ReflectionData> reflectionCache;
    /**
     * does this mirror support has a mirror on it
     */
    private boolean hasMirror;
    /**
     * the armor stand representing the mirror
     */
    private ArmorStand armorStandMirror;
    /**
     * the armor stand representing the mirror support
     */
    private ArmorStand armorStandMirrorSupport;
    /**
     * is this mirror support currently showing an animation
     */
    private boolean onGoingAnimation;
    /**
     * the color of this mirror support
     */
    private LasersColor color;

    /**
     * The saved color of this component's mirror. Used only on reset
     */
    private LasersColor savedColor;

    /**
     * Saved mirror rotation
     */
    private Rotation savedMirrorRotation;

    private MirrorSupportMode mode;

    private PlaceMirrorOnSupportTask placeMirrorOnSupportTask;

    private GetMirrorFromSupportTask getMirrorFromSupportTask;

    /**
     * Constructor used for creation from database
     *
     * @param area          the area containing this mirror support
     * @param componentId   the id of the component inside the database
     * @param location      the location of this mirror support
     * @param face          the face the component is on
     * @param savedColor    the color of this component on reset
     * @param savedRotation the rotation of this component on reset
     */
    public MirrorSupport(Area area, int componentId, Location location, ComponentFace face, LasersColor savedColor, Rotation savedRotation, MirrorSupportMode mode) {
        super(area, componentId, location, ComponentType.MIRROR_SUPPORT, face, face.getDefaultRotation(ComponentType.MIRROR_SUPPORT));
        this.onGoingAnimation = false;
        this.savedColor = savedColor;
        this.color = savedColor == null ? LasersColor.WHITE : savedColor;
        this.rotation = savedColor == null ? rotation : savedRotation;
        this.savedMirrorRotation = savedRotation;
        this.hasMirror = savedColor != null;
        this.mode = mode;
        this.reflectionCache = new HashMap<>();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this mirror support
     * @param location the location of this mirror support
     * @param face     the face the component is on
     */
    public MirrorSupport(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.MIRROR_SUPPORT, face);
        this.hasMirror = true;
        this.onGoingAnimation = false;
        this.savedColor = LasersColor.WHITE;
        this.savedMirrorRotation = rotation;
        this.color = LasersColor.WHITE;
        this.mode = MirrorSupportMode.FREE;
        this.reflectionCache = new HashMap<>();
        updateDisplay();
        dbCreate();
    }

    @Override
    public ArmorStand getArmorStandMirror() {
        return armorStandMirror;
    }

    @Override
    public ArmorStand getArmorStandMirrorContainer() {
        return armorStandMirrorSupport;
    }

    public HashMap<ReflectionData, ReflectionData> getReflectionCache() {
        return reflectionCache;
    }

    /**
     * deletes this mirror support
     */
    @Override
    public void hide() {
        super.hide();
        if (onGoingAnimation && getMirrorFromSupportTask != null) {
            getMirrorFromSupportTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSupportTask != null) {
            placeMirrorOnSupportTask.cancel();
        }
        onGoingAnimation = false;
        hasMirror = false;
        armorStandMirrorSupport = findArmorStandBack(armorStandMirrorSupport);
        armorStandMirror = findArmorStandBack(armorStandMirror);
        if (armorStandMirror != null) {
            armorStandMirror.remove();
            armorStandMirror = null;
        }
        if (armorStandMirrorSupport != null) {
            armorStandMirrorSupport.remove();
            armorStandMirrorSupport = null;
        }
    }

    /**
     * checks if an animation is currently on going
     *
     * @return true if an animation is currently shown
     */
    public boolean getOnGoingAnimation() {
        return onGoingAnimation;
    }

    /**
     * sets if this mirror support is currently animated or not
     *
     * @param onGoingAnimation if this mirror support is currently animated or not
     */
    @Override
    public void setOnGoingAnimation(boolean onGoingAnimation) {
        this.onGoingAnimation = onGoingAnimation;
    }

    /**
     * does this mirror support currently has a mirror on it
     *
     * @return true if this mirror support has a mirror on it
     */
    @Override
    public boolean hasMirror() {
        return hasMirror;
    }

    public boolean hasSavedMirror() {
        return savedMirrorRotation != null;
    }

    /**
     * Sets if this mirror support currently has a mirror on it
     *
     * @param hasMirror if this mirror support currently has a mirror on it or not
     * @param save should the presence/lack of mirror be saved in database
     */
    @Override
    public void setHasMirror(boolean hasMirror, boolean save) {
        if (onGoingAnimation) {
            return;
        }
        this.hasMirror = hasMirror;
        updateDisplay();
        if (save) {
            savedColor = hasMirror ? color : null;
            savedMirrorRotation = hasMirror ? rotation : face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);
            dbUpdate();
        }
    }
    /**
     * retrieves the mirror from this mirror support
     *
     * @return true if the mirror has been retrieved successfully
     * @param save should the presence/lack of mirror be saved in database
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean getMirror(boolean save) {
        if (!onGoingAnimation && hasMirror && !this.isRemoved()) {
            onGoingAnimation = true;
            // Animation
            getMirrorFromSupportTask = new GetMirrorFromSupportTask(this, save);
            return true;
        }
        return false;
    }

    /**
     * place a mirror on this mirror support
     *
     * @param color the color of the mirror retrieved
     * @return true is the mirror has been placed successfully
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean placeMirror(LasersColor color, boolean save) {
        if (!onGoingAnimation && !hasMirror && !this.isRemoved()) {
            onGoingAnimation = true;
            this.color = color;
            reflectionCache.clear();
            rotation = face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);
            createMirrorArmorStand();
            placeMirrorOnSupportTask = new PlaceMirrorOnSupportTask(this, save);
            return true;
        }
        return false;
    }

    /**
     * updates this mirror support
     */
    @Override
    public void updateDisplay() {

        //Component hidden block
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }

        //Retrieving armorstands if required
        armorStandMirrorSupport = findArmorStandBack(armorStandMirrorSupport);
        armorStandMirror = findArmorStandBack(armorStandMirror);

        //Creation/Deletion of armorstands
        if (armorStandMirrorSupport == null) {
            createMirrorSupportArmorStand();
        }
        if (hasMirror && armorStandMirror == null) {
            createMirrorArmorStand();
        } else if (!hasMirror && armorStandMirror != null) {
            armorStandMirror.remove();
            Areas.getInstance().removeEntity(armorStandMirror.getUniqueId());
            armorStandMirror = null;
        }

        //Rotation, head and location of mirror support armor stand
        Rotation supportRotation = face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);

        armorStandMirrorSupport.setHeadPose(supportRotation);
        armorStandMirrorSupport.setHelmet(ItemsFactory.getInstance().getItemStack(mode.getCorrespondingSupportSkin()));

        Location supportLoc = getDefaultArmorStandBaseLocation(getLocation(), face);
        if (hasMirror) {
            supportLoc.add(getAnimationVector().multiply(-1d));
        }
        supportLoc = getArmorStandLocationWithOffsets(
                supportLoc,
                ArmorStandItemOffset.HEAD,
                supportRotation);
        armorStandMirrorSupport.teleport(supportLoc);

        //Rotation, head and location of mirror armor stand
        if (hasMirror) {
            armorStandMirror.setHeadPose(rotation);
            armorStandMirror.setHelmet(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
            Location mirrorLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation()), ArmorStandItemOffset.GLASS_PANE, rotation);
            armorStandMirror.teleport(mirrorLoc);
        }
    }
    /**
     * Creates the armor stand corresponding to the mirror support
     */
    private void createMirrorSupportArmorStand() {
        Rotation supportRotation = face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);
        Location supportLoc = getDefaultArmorStandBaseLocation(getLocation(), face);
        if (hasMirror) {
            supportLoc.add(getAnimationVector().multiply(-1d));
        }
        supportLoc = getArmorStandLocationWithOffsets(
                supportLoc,
                ArmorStandItemOffset.HEAD,
                supportRotation);
        armorStandMirrorSupport = createArmorStand(supportLoc,  face.getDefaultRotation(ComponentType.MIRROR_SUPPORT), Item.MIRROR_SUPPORT);
        Areas.getInstance().addEntity(armorStandMirrorSupport.getUniqueId(), this);
    }

    /**
     * Creates the armor stand corresponding to the mirror
     */
    private void createMirrorArmorStand() {
        Location mirrorLoc = getDefaultArmorStandBaseLocation(getLocation());
        if (!hasMirror)
            mirrorLoc = mirrorLoc.add(getAnimationClipVector()).add(getAnimationVector());
        Location armorStandMirrorLoc = getArmorStandLocationWithOffsets(
                mirrorLoc,
                ArmorStandItemOffset.GLASS_PANE,
                rotation
        );
        armorStandMirror = createArmorStand(armorStandMirrorLoc, rotation, Item.getMirror(color));
        Areas.getInstance().addEntity(armorStandMirror.getUniqueId(), this);
    }

    @Override
    public final Vector getAnimationVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_LOCATION_DIFF);
    }

    @Override
    public final Vector getAnimationClipVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_CLIP_LOCATION_DIFF);
    }

    /**
     * Rotates the mirror on this mirror support
     * @param rotationType the type of the rotation
     */
    @Override
    public void rotate(RotationType rotationType, boolean save) {
        rotate(rotationType, save, true);
    }

    /**
     * Rotates the mirror on this mirror support
     * @param rotationType the type of the rotation
     * @param playSound    if the rotation sound should be played
     */
    @Override
    public void rotate(RotationType rotationType, boolean save, boolean playSound) {
        if (onGoingAnimation || !hasMirror) {
            return;
        }
        reflectionCache.clear();
        this.rotation = rotation.getNextRotation(rotationType);
        updateDisplay();
        if (playSound)
            NMSManager.getNMS().playSound(armorStandMirrorSupport.getLocation(), "ENTITY_IRONGOLEM_ATTACK", 1f, 0.7f);
        if (save) {
            savedMirrorRotation = rotation;
            savedColor = color;
            dbUpdate();
        }
    }

    /**
     * Resets the mirror support
     */
    @Override
    public void reset() {
        if (onGoingAnimation && getMirrorFromSupportTask != null) {
            getMirrorFromSupportTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSupportTask != null) {
            placeMirrorOnSupportTask.cancel();
        }
        onGoingAnimation = false;
        this.color = savedColor == null ? LasersColor.WHITE : savedColor;
        this.rotation = savedColor == null ? face.getDefaultRotation(ComponentType.MIRROR_SUPPORT) : savedMirrorRotation;
        reflectionCache.clear();
        updateDisplay();
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of MirrorSupports is defined by its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(color.getNextColor(false), save);
    }

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        changeColor(false);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, false);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is ignored since the color of MirrorSupports is defined by its mirror so it's never saved in database.
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        if (!hasMirror || onGoingAnimation) {
            return;
        }
        this.color = color;
        updateDisplay();
        if (save) {
            savedColor = color;
            savedMirrorRotation = rotation;
            dbUpdate();
        }
    }

    /**
     * gets the color of this mirror support
     *
     * @return the color of this mirror support
     */
    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getSavedColor() {
        return savedColor;
    }

    @Override
    public Rotation getSavedRotation() {
        return savedMirrorRotation;
    }

    public MirrorSupportMode getMode() {
        return mode;
    }

    public void changeMode(MirrorSupportMode mode) {
        this.mode = mode;
        updateDisplay();
        dbUpdate();
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandMirror = null;
        armorStandMirrorSupport = null;
    }

    @Override
    public boolean canBeDeleted() {
        return !onGoingAnimation;
    }
}
