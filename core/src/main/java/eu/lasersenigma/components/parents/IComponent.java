package eu.lasersenigma.components.parents;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.SavedAction;
import org.bukkit.Location;

import java.util.ArrayList;

public interface IComponent {

    /**
     * gets the location of the component
     *
     * @return the location of the component
     */
    public Location getLocation();

    /**
     * gets the type of the component
     *
     * @return the type of the component
     */
    public ComponentType getType();

    /**
     * gets the area containing the component
     *
     * @return the area containing the component
     */
    public Area getArea();

    /**
     * Hide the component (= removes visible elements)
     */
    public void hide();

    /**
     * Show (or update the visible elements of) the component
     */
    public void updateDisplay();

    /**
     * Resets the component (when the last player leaves the area)
     */
    public void reset();

    /**
     * Removes the component
     */
    public void remove();

    /**
     * Removes the component from the database
     */
    public void dbRemove();

    /**
     * Creates the component into the database
     */
    public void dbCreate();

    /**
     * Updates the component in the database
     */
    public void dbUpdate();

    /**
     * Retrieves the component id
     *
     * @return int the component id
     */
    public int getComponentId();

    /**
     * To know if the component have been removed
     *
     * @return true if the component still exists, false otherwise
     */
    public boolean isRemoved();

    /**
     * Can this component currently be deleted
     *
     * @return true if the component can  be deleted now
     */
    public boolean canBeDeleted();

    /**
     * Retrieves the scheduled actions which will be executed in a loop on this component
     *
     * @return the scheduled actions which will be executed in a loop on this component
     */
    public ArrayList<SavedAction> getScheduledActions();

    /**
     * Notify the system that this component scheduled actions has been modified.
     */
    public void updateActions();

    public void startScheduledActions();

    public void stopScheduledActions();
}
