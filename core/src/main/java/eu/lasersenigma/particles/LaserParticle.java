package eu.lasersenigma.particles;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.*;
import eu.lasersenigma.components.attributes.Direction;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.laserparticles.ParticleTryToHitBlockLEEvent;
import eu.lasersenigma.events.laserparticles.ParticleTryToHitComponentLEEvent;
import eu.lasersenigma.events.laserparticles.ParticleTryToHitEntityLEEvent;
import eu.lasersenigma.events.laserparticles.ParticleTryToMoveInTheAirLEEvent;
import eu.lasersenigma.nms.NMSManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.*;

/**
 * A laser particle
 */
public class LaserParticle {

    public static final double MAX_NB_MOVEMENT_WHILE_AFFECTED_BY_SPHERE = 500;

    public static final double LASERS_SPEED = 0.5;

    public static final int LASERS_FREQUENCY = 0;
    public static final double SPHERE_RADIUS = 0.42;
    public static final double ACCURACY = 0.05;
    public static final boolean isLightAPIenabled = LasersEnigmaPlugin.getInstance().isLightAPIAvailable();
    private static final double MIRROR_SIZE = 0.42;
    private static final double MIRROR_DEPTH = 0.23;
    /**
     * the materials that can be crossed by a laser particle
     */

    private static final boolean AFFECT_PLAYERS = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("laser_affect_players");
    private static final boolean AFFECT_MOBS = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("laser_affect_mobs");
    private static final int LASERS_BURNS_TICK_DURATION = LasersEnigmaPlugin.getInstance().getConfig().getInt("laser_burns_tick_duration");
    private static final double LASERS_ADDITIONAL_DAMAGES = LasersEnigmaPlugin.getInstance().getConfig().getDouble("laser_additional_damage");
    private static final double LASERS_KNOCKBACK_MULTIPLIER = LasersEnigmaPlugin.getInstance().getConfig().getDouble("laser_knockback_multiplier");
    private static final boolean ENTITIES_STOPS_LASERS = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("entities_stops_lasers");
    private static final String CROSSABLE_MATERIALS_STRING = LasersEnigmaPlugin.getInstance().getConfig().getString("crossable_materials", "AIR,GLASS,GLASS_PANE,BARRIER,REDSTONE_WIRE,REDSTONE,WATER,LEGACY_STATIONARY_WATER,TRIPWIRE,WATER,CYAN_CARPET,BLACK_CARPET,BLUE_CARPET,BROWN_CARPET,GRAY_CARPET,GREEN_CARPET,LIGHT_BLUE_CARPET,LIGHT_GRAY_CARPET,LIME_CARPET,MAGENTA_CARPET,ORANGE_CARPET,PINK_CARPET,PURPLE_CARPET,RED_CARPET,WHITE_CARPET,YELLOW_CARPET,ACACIA_SLAB,SANDSTONE_SLAB,BIRCH_SLAB,SPRUCE_SLAB,STONE_BRICK_SLAB,STONE_SLAB,BRICK_SLAB,COBBLESTONE_SLAB,DARK_OAK_SLAB,DARK_PRISMARINE_SLAB,JUNGLE_SLAB,NETHER_BRICK_SLAB,OAK_SLAB,PETRIFIED_OAK_SLAB,PRISMARINE_BRICK_SLAB,PURPUR_SLAB,PRISMARINE_SLAB,RED_SANDSTONE_SLAB,QUARTZ_SLAB");

    @SuppressWarnings("SetReplaceableByEnumSet")
    public static final HashSet<Material> CROSSABLE_MATERIALS = NMSManager.getNMS().getCrossableMaterials(CROSSABLE_MATERIALS_STRING.split(","));
    /**
     * the area containing this particle
     */
    private final Area area;
    /**
     * the current direction of this particle
     */
    private Direction direction;
    /**
     * the current location of this particle
     */
    private Location location;
    /**
     * the last component passed by this particle
     */
    private IComponent lastComponent;
    /**
     * The last entity hit by this particle
     */
    private UUID lastEntityUUID;
    /**
     * the color of this particle
     */
    private LasersColor color;
    private boolean justCreated = true;
    private int nbMovementWhileAffectedBySphere = 0;
    private int nbMovementSinceCreation = 0;
    private double speed;
    private int lightLevel;

    /**
     * Constructor
     *
     * @param origin the laser sender that sent this particle
     * @param area   the area containing this particle
     */
    public LaserParticle(LaserSender origin, Area area) {
        this.lastComponent = origin;
        this.lastEntityUUID = null;
        this.color = origin.getColor();
        this.direction = new Direction(origin.getCurrentRotation().toEyeDirection());
        this.area = area;
        this.location = origin.getASHeadCenterLocation();
        this.speed = direction.lengthSquared();
        this.lightLevel = origin.getLightLevel();
    }

    /**
     * Constructor
     *
     * @param concentrator the concentrator that sent this particle
     * @param color        the color of this particle
     * @param area         the area containing this particle
     */
    public LaserParticle(Concentrator concentrator, LasersColor color, Area area) {
        this.lastComponent = concentrator;
        this.lastEntityUUID = null;
        this.color = color;
        this.direction = new Direction(concentrator.getCurrentRotation().toEyeDirection());
        this.area = area;
        this.location = concentrator.getASHeadCenterLocation();
        this.speed = direction.lengthSquared();
        this.lightLevel = concentrator.getResultingLightLevel();
    }

    public LaserParticle(UUID lastEntityUUID, Location location, Direction direction, LasersColor color, Area area, int lightLevel) {
        this.lastComponent = null;
        this.lastEntityUUID = lastEntityUUID;
        this.justCreated = false;
        this.location = location.clone();
        this.direction = direction.clone();
        this.color = color;
        this.area = area;
        this.speed = direction.lengthSquared();
        this.lightLevel = lightLevel;
    }

    /**
     * Constructor
     *
     * @param lastComponent the last component passed by this particle
     * @param location      the current location of this particle
     * @param direction     the current direction of this particle
     * @param color         the color of this particle
     * @param area          the area containing this particle
     */
    private LaserParticle(IComponent lastComponent, Location location, Direction direction, LasersColor color, Area area, int lightLevel) {
        this.lastComponent = lastComponent;
        this.lastEntityUUID = null;
        this.location = location.clone();
        this.direction = direction.clone();
        this.color = color;
        this.area = area;
        this.speed = direction.lengthSquared();
        this.lightLevel = lightLevel;
    }

    private LaserParticle(Location location, Direction direction, LasersColor color, Area area, int lightLevel) {
        this.lastEntityUUID = null;
        this.lastComponent = null;
        this.location = location.clone();
        this.direction = direction.clone();
        this.color = color;
        this.area = area;
        this.speed = direction.lengthSquared();
        this.lightLevel = lightLevel;
    }

    public static ReflectionData reflect(Block block, ReflectionData laserParticleData) {

        //Calculating the intersection location between the laser and the block
        Location origin = laserParticleData.getLocation().clone().subtract(laserParticleData.getDirection()); //we get back one movement behind
        Vector detectionDirection = laserParticleData.getDirection().clone().multiply(ACCURACY); //we get a smaller vector to move
        double maxNbIteration = 1 / ACCURACY + 1;
        double curPosIndex = 1;
        Location curLocation = origin.add(detectionDirection);
        Block b = null;
        while (curPosIndex <= maxNbIteration) {
            b = curLocation.getBlock();
            if (NMSManager.getNMS().isConcretePowder(b.getType())) {
                break;
            }
            curPosIndex++;
            curLocation = curLocation.add(detectionDirection);
        }
        if (b == null) {
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.NO_INTERSECTION);
        }
        final Location intersectionPoint = curLocation;

        AABB blockBox = new AABB(b.getLocation());
        Vector blockBoxCenterVector = blockBox.getCenter();
        Vector faceTouchedCenterVector = blockBox.getFaces().stream().min(Comparator.comparingDouble((face) -> face.distanceSquared(intersectionPoint.toVector()))).get();
        Vector mirrorPlaneNormal = new Vector(
                faceTouchedCenterVector.getX() - blockBoxCenterVector.getX(),
                faceTouchedCenterVector.getY() - blockBoxCenterVector.getY(),
                faceTouchedCenterVector.getZ() - blockBoxCenterVector.getZ()
        );
        mirrorPlaneNormal.normalize();

        double dotProd = laserParticleData.getDirection().dot(mirrorPlaneNormal);
        if (dotProd == 0) { //No Intersection can be calculated.
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.ORTHOGONAL);
        } else { //reflection angle calculation

            double param = (-mirrorPlaneNormal.getX() * (laserParticleData.getLocation().getX() - faceTouchedCenterVector.getX())
                    - mirrorPlaneNormal.getY() * (laserParticleData.getLocation().getY() - faceTouchedCenterVector.getY())
                    - mirrorPlaneNormal.getZ() * (laserParticleData.getLocation().getZ() - faceTouchedCenterVector.getZ()))
                    / (mirrorPlaneNormal.getX() * laserParticleData.getDirection().getX()
                    + mirrorPlaneNormal.getY() * laserParticleData.getDirection().getY()
                    + mirrorPlaneNormal.getZ() * laserParticleData.getDirection().getZ());
            Location intersection = new Location(
                    laserParticleData.getLocation().getWorld(),
                    laserParticleData.getLocation().getX() + laserParticleData.getDirection().getX() * param,
                    laserParticleData.getLocation().getY() + laserParticleData.getDirection().getY() * param,
                    laserParticleData.getLocation().getZ() + laserParticleData.getDirection().getZ() * param);
            Direction resultDirection = new Direction(
                    laserParticleData.getDirection().getX() - (2 * dotProd * mirrorPlaneNormal.getX()),
                    laserParticleData.getDirection().getY() - (2 * dotProd * mirrorPlaneNormal.getY()),
                    laserParticleData.getDirection().getZ() - (2 * dotProd * mirrorPlaneNormal.getZ()));
            return new ReflectionData(resultDirection, intersection.add(resultDirection.clone().multiply(ACCURACY)), ReflectionResultType.REFLECTED);
        }
    }

    public static ReflectionData reflect(ReflectingSphere rs, ReflectionData laserParticleData) {

        // http://www.codeproject.com/Articles/19799/Simple-Ray-Tracing-in-C-Part-II-Triangles-Intersec
        Location sphereCenter = rs.getASHeadCenterLocation();

        double cx = sphereCenter.getX();
        double cy = sphereCenter.getY();
        double cz = sphereCenter.getZ();

        double px = laserParticleData.getLocation().getX();
        double py = laserParticleData.getLocation().getY();
        double pz = laserParticleData.getLocation().getZ();

        double vx = laserParticleData.getDirection().getX();
        double vy = laserParticleData.getDirection().getY();
        double vz = laserParticleData.getDirection().getZ();

        double a = vx * vx + vy * vy + vz * vz;
        double b = 2.0 * (px * vx + py * vy + pz * vz - vx * cx - vy * cy - vz * cz);
        double c = px * px - 2 * px * cx + cx * cx + py * py - 2 * py * cy + cy * cy
                + pz * pz - 2 * pz * cz + cz * cz - SPHERE_RADIUS * SPHERE_RADIUS;

        // discriminant
        double d = b * b - 4 * a * c;

        if (d < 0) {
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation());
        }

        double t1 = (-b - Math.sqrt(d)) / (2.0 * a);

        Location solution1 = new Location(
                laserParticleData.getLocation().getWorld(),
                px * (1 - t1) + t1 * (px + vx),
                py * (1 - t1) + t1 * (py + vy),
                pz * (1 - t1) + t1 * (pz + vz));
        Location finalIntersection;
        if (d == 0) {
            finalIntersection = solution1;
        } else {

            double t2 = (-b + Math.sqrt(d)) / (2.0 * a);
            Location solution2 = new Location(
                    laserParticleData.getLocation().getWorld(),
                    px * (1 - t2) + t2 * (px + vx),
                    py * (1 - t2) + t2 * (py + vy),
                    pz * (1 - t2) + t2 * (pz + vz));

            // prefer a solution that's the first intersection between the laser and the sphere.
            Location prevParticleLoc = new Location(
                    laserParticleData.getLocation().getWorld(),
                    px - vx,
                    py - vy,
                    pz - vz
            );

            if (solution1.distance(prevParticleLoc) < solution2.distance(prevParticleLoc)) {
                finalIntersection = solution1;
            } else {
                finalIntersection = solution2;
            }

        }

        double normalVectorX = cx - finalIntersection.getX();
        double normalVectorY = cy - finalIntersection.getY();
        double normalVectorZ = cz - finalIntersection.getZ();
        Vector normalVector = new Vector(normalVectorX, normalVectorY, normalVectorZ).normalize();
        double dotProd = laserParticleData.getDirection().clone().normalize().dot(normalVector);
        if (dotProd == 0) {
            return new ReflectionData(new Direction(laserParticleData.getDirection().clone().multiply(-1)), laserParticleData.getLocation());
        }
        Direction resultDirection = new Direction(
                vx - (2 * dotProd * normalVectorX),
                vy - (2 * dotProd * normalVectorY),
                vz - (2 * dotProd * normalVectorZ));
        return new ReflectionData(resultDirection, finalIntersection);

    }

    public static ReflectionData reflect(MirrorSupport ms, ReflectionData laserParticleData) {
        /* Reflexion Direction calculation */
        //https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
        //https://stackoverflow.com/questions/5666222/3d-line-plane-intersection
        //http://www.3dkingdoms.com/weekly/weekly.php?a=2
        Vector mirrorPlaneNormal = ms.getRotation().toEyeDirection();
        Vector mirrorHeadDirection = ms.getRotation().toHeadDirection();
        Vector mirrorSideDirection = ms.getRotation().toSideDirection();
        Location mirrorCenterLocation = new Location(ms.getLocation().getWorld(), ms.getLocation().getBlockX() + 0.5, ms.getLocation().getBlockY() + 0.5, ms.getLocation().getBlockZ() + 0.5);
        double dotProd = laserParticleData.getDirection().dot(mirrorPlaneNormal);
        if (dotProd == 0) { //No Intersection can be calculated.
            //Calculate the distance between the laser particle and the mirror center but only on the axis defined by the mirrorPlaneNormal Vector.
            double distanceBetweenLaserAndMirror = Math.abs((laserParticleData.getLocation().getX() - mirrorCenterLocation.getX()) * mirrorPlaneNormal.getX())
                    + Math.abs((laserParticleData.getLocation().getY() - mirrorCenterLocation.getY()) * mirrorPlaneNormal.getY())
                    + Math.abs((laserParticleData.getLocation().getZ() - mirrorCenterLocation.getZ()) * mirrorPlaneNormal.getZ());
            //System.out.println(distanceBetweenLaserAndMirror + " < " + MIRROR_DEPTH);
            //Check if the laser is really touching the mirror
            if (distanceBetweenLaserAndMirror < MIRROR_DEPTH) {
                return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.ORTHOGONAL);
            } else {
                return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.NO_INTERSECTION);
            }
        } else { //intersection calculation

            double param = (-mirrorPlaneNormal.getX() * (laserParticleData.getLocation().getX() - mirrorCenterLocation.getX())
                    - mirrorPlaneNormal.getY() * (laserParticleData.getLocation().getY() - mirrorCenterLocation.getY())
                    - mirrorPlaneNormal.getZ() * (laserParticleData.getLocation().getZ() - mirrorCenterLocation.getZ()))
                    / (mirrorPlaneNormal.getX() * laserParticleData.getDirection().getX()
                    + mirrorPlaneNormal.getY() * laserParticleData.getDirection().getY()
                    + mirrorPlaneNormal.getZ() * laserParticleData.getDirection().getZ());
            Location intersection = new Location(
                    laserParticleData.getLocation().getWorld(),
                    laserParticleData.getLocation().getX() + laserParticleData.getDirection().getX() * param,
                    laserParticleData.getLocation().getY() + laserParticleData.getDirection().getY() * param,
                    laserParticleData.getLocation().getZ() + laserParticleData.getDirection().getZ() * param);

            //Calculate the distance between the intersection and the mirror center but only on the axis defined by the mirrorHeadDirection Vector.
            double distanceBetweenLaserAndMirrorOnHeadAxis = Math.abs((intersection.getX() - mirrorCenterLocation.getX()) * mirrorHeadDirection.getX())
                    + Math.abs((intersection.getY() - mirrorCenterLocation.getY()) * mirrorHeadDirection.getY())
                    + Math.abs((intersection.getZ() - mirrorCenterLocation.getZ()) * mirrorHeadDirection.getZ());

            //Calculate the distance between the intersection and the mirror center but only on the axis defined by the mirrorHeadDirection Vector.
            double distanceBetweenLaserAndMirrorOnSideAxis = Math.abs((intersection.getX() - mirrorCenterLocation.getX()) * mirrorSideDirection.getX())
                    + Math.abs((intersection.getY() - mirrorCenterLocation.getY()) * mirrorSideDirection.getY())
                    + Math.abs((intersection.getZ() - mirrorCenterLocation.getZ()) * mirrorSideDirection.getZ());

            if (distanceBetweenLaserAndMirrorOnHeadAxis < MIRROR_SIZE
                    && distanceBetweenLaserAndMirrorOnSideAxis < MIRROR_SIZE) {
                Direction resultDirection = new Direction(
                        laserParticleData.getDirection().getX() - (2 * dotProd * mirrorPlaneNormal.getX()),
                        laserParticleData.getDirection().getY() - (2 * dotProd * mirrorPlaneNormal.getY()),
                        laserParticleData.getDirection().getZ() - (2 * dotProd * mirrorPlaneNormal.getZ()));
                return new ReflectionData(resultDirection, intersection, ReflectionResultType.REFLECTED);
            } else {
                return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.NO_INTERSECTION);
            }
        }
    }

    /**
     * gets the current dirrection of this particle
     *
     * @return the rotation of the particle
     */
    public Direction getDirection() {
        return this.direction.clone();
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * gets the current color of this particle
     *
     * @return the current color of this particle
     */
    public LasersColor getColor() {
        return color;
    }

    public void setColor(LasersColor color) {
        this.color = color;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public IComponent getLastComponent() {
        return lastComponent;
    }

    public void setLastComponent(IComponent lastComponent) {
        this.lastComponent = lastComponent;
    }

    public UUID getLastEntityUUID() {
        return lastEntityUUID;
    }

    public Area getArea() {
        return area;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setJustCreated(boolean justCreated) {
        this.justCreated = justCreated;
    }

    public void setNbMovementWhileAffectedBySphere(int nbMovementWhileAffectedBySphere) {
        this.nbMovementWhileAffectedBySphere = nbMovementWhileAffectedBySphere;
    }

    public void setNbMovementSinceCreation(int nbMovementSinceCreation) {
        this.nbMovementSinceCreation = nbMovementSinceCreation;
    }

    public int getLightLevel() {
        return lightLevel;
    }

    public void setLightLevel(int lightLevel) {
        this.lightLevel = lightLevel;
    }


    /**
     * updates this particle
     *
     * @param checkDamage if the entity must check if it touched a player or not
     * @param show        if the laser particles must be shown
     * @return false if the particle must disappear
     */
    public boolean update(boolean checkDamage, boolean show) {
        if (justCreated) {
            show(checkDamage, show);
            justCreated = false;
        } else {
            calculateTrajectory();
        }
        move();
        boolean mustContinueMoving = check();
        if (mustContinueMoving && show(checkDamage, show)) {
            return false;
        }
        if (nbMovementWhileAffectedBySphere > MAX_NB_MOVEMENT_WHILE_AFFECTED_BY_SPHERE) {
            return false;
        }
        if (nbMovementSinceCreation > 3) {
            this.lastEntityUUID = null;
        }
        return mustContinueMoving;
    }

    /**
     * updateDisplay this particle
     */
    private boolean show(boolean checkDamage, boolean show) {
        if (show) {
            NMSManager.getNMS().playEffect(area.getPlayers(), location, "REDSTONE", color.getBukkitColor());
            //We check world borders because particles can sometimes appear outside those borders and this would cause a NPE in LightAPI.
            if (isLightAPIenabled && lightLevel != 0 && location.getBlockY() < location.getWorld().getMaxHeight() && location.getBlockY() > 0) {
                this.getArea().getLightLevelPerLocations().put(new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ()), lightLevel);
            }
        }
        return impact(checkDamage);
    }

    /**
     * Checks what will be the next action of this particle (hide, reflected,
     * ...)
     *
     * @return true if this particle
     */
    private boolean check() {
        //If we are out of the area the laser particle disappear
        if (!(area.containsLocation(location))) {
            return false;
        }
        //If the laser particle is on a component
        IComponent component = area.getComponentFromLocation(location);
        if (component != null) {
            ParticleTryToHitComponentLEEvent hitCompEvent = new ParticleTryToHitComponentLEEvent(area, this, component);
            Bukkit.getPluginManager().callEvent(hitCompEvent);
            if (hitCompEvent.isCancelled()) {
                return false;
            }
            if (hitCompEvent.getIgnoreDefaults()) {
                return true;
            }
            if (component instanceof LaserReceiver) {
                ((LaserReceiver) component).receiveLaser(this);
                return false;
            } else if (component instanceof Concentrator) {
                if (component.equals(lastComponent)) {
                    return true;
                }
                ((Concentrator) component).receiveLaser(this);
                return false;
            } else if (component instanceof MirrorSupport) {
                MirrorSupport ms = (MirrorSupport) component;
                if (ms.equals(lastComponent)) {
                    return true;
                }
                if (ms.hasMirror() && !ms.getOnGoingAnimation()) {
                    ReflectionData inputRD = new ReflectionData(direction, location);
                    ReflectionData outputRD = ms.getReflectionCache().get(inputRD);
                    if (outputRD == null) {
                        outputRD = reflect(ms, inputRD);
                        ms.getReflectionCache().put(inputRD, outputRD);
                    }
                    HashMap<LasersColor.FilterResult, LasersColor> filterResult;
                    LasersColor reflectedColor = null;
                    LasersColor passThroughColor = null;
                    switch (outputRD.getReflexionResult()) {
                        case NO_INTERSECTION:
                            area.addLaserParticle(new LaserParticle(ms, outputRD.getLocation(), outputRD.getDirection(), color, area, lightLevel));
                            break;
                        case ORTHOGONAL:
                            filterResult = color.filterBy(ms.getColor());
                            passThroughColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                            if (ms.getColor() == LasersColor.WHITE) {
                                passThroughColor = filterResult.get(LasersColor.FilterResult.PASS_THROUGH);
                            }
                            break;
                        case REFLECTED:
                            filterResult = color.filterBy(ms.getColor());
                            passThroughColor = filterResult.get(LasersColor.FilterResult.PASS_THROUGH);
                            reflectedColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                            if (ms.getColor() == LasersColor.WHITE) {
                                reflectedColor = passThroughColor;
                                passThroughColor = null;

                            }
                            break;
                        default:
                            break;
                    }
                    if (passThroughColor != null) {
                        area.addLaserParticle(new LaserParticle(ms, outputRD.getLocation(), direction, passThroughColor, area, lightLevel));
                    }
                    if (reflectedColor != null) {
                        area.addLaserParticle(new LaserParticle(ms, outputRD.getLocation(), outputRD.getDirection(), reflectedColor, area, lightLevel));
                    }
                    return false;
                }
            } else if (component instanceof LaserSender) {
                return component.equals(lastComponent);
            } else if (component instanceof MeltableClayBlock) {
                MeltableClayBlock meltableClayBlock = ((MeltableClayBlock) component);
                if (meltableClayBlock.isMelted()) {
                    return true;
                }
                meltableClayBlock.receiveLaser(this);
            } else if (component instanceof DisappearingWinnerBlock) {
                return ((DisappearingWinnerBlock) component).isCrossable();
            } else if (component instanceof AppearingWinnerBlock) {
                return ((AppearingWinnerBlock) component).isCrossable();
            } else if (component instanceof Prism) {
                if (component.equals(lastComponent)) {
                    return true;
                }
                Prism prism = ((Prism) component);
                ArrayList<LasersColor> newColors = color.decompose();
                ArrayList<Direction> newDirections = prism.getNewDirection(direction, newColors.size());
                for (int i = 0; i < newColors.size(); ++i) {
                    area.addLaserParticle(new LaserParticle(component, location.clone().add(newDirections.get(i)), newDirections.get(i), newColors.get(i), area, lightLevel));
                }
            } else if (component instanceof ReflectingSphere) {
                ReflectingSphere reflectiveSphere = (ReflectingSphere) component;
                if (component.equals(lastComponent)) {
                    return true;
                }
                if (!reflectiveSphere.hasMirror() || reflectiveSphere.getOnGoingAnimation()) {
                    return false;
                }
                LasersColor reflectedColor;
                if (reflectiveSphere.getColor() == LasersColor.WHITE) {
                    reflectedColor = color;
                } else {
                    reflectedColor = color.filterBy(reflectiveSphere.getColor()).get(LasersColor.FilterResult.REFLECTED);
                }
                if (reflectedColor == null) {
                    return false;
                }
                ReflectionData inputRD = new ReflectionData(direction, location);
                ReflectionData outputRD = reflectiveSphere.getReflexionCache().get(inputRD);
                if (outputRD == null) {
                    outputRD = reflect(reflectiveSphere, inputRD);
                    reflectiveSphere.getReflexionCache().put(inputRD, outputRD);
                }
                area.addLaserParticle(new LaserParticle(reflectiveSphere, outputRD.getLocation(), outputRD.getDirection(), reflectedColor, area, lightLevel));
                return false;
            } else if (component instanceof FilteringSphere) {
                FilteringSphere filteringSphere = (FilteringSphere) component;
                if (component.equals(lastComponent)) {
                    return true;
                }
                if (!filteringSphere.hasMirror() || filteringSphere.getOnGoingAnimation()) {
                    return false;
                }
                LasersColor resColor;
                HashMap<LasersColor.FilterResult, LasersColor> filterResult = color.filterBy(filteringSphere.getColor());
                if (filteringSphere.getColor() == LasersColor.WHITE) {
                    resColor = color;
                } else {
                    resColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                }
                if (resColor != null) {
                    area.addLaserParticle(new LaserParticle(filteringSphere, location, direction, resColor, area, lightLevel));
                }
            }
        } else {
            //If the laser particle is NOT on a component
            //checking of the type of the block we are on.
            Block b = location.getBlock();
            if (CROSSABLE_MATERIALS.contains(b.getType())) {
                if (lastComponent != null && !(lastComponent instanceof AttractionRepulsionSphere)) {
                    lastComponent = null;
                }
                ParticleTryToMoveInTheAirLEEvent moveAirEvent = new ParticleTryToMoveInTheAirLEEvent(area, this);
                Bukkit.getServer().getPluginManager().callEvent(moveAirEvent);
                if (moveAirEvent.getIgnoreDefaults()) {
                    return !moveAirEvent.isCancelled();
                } else return !moveAirEvent.isCancelled();
            }
            if (NMSManager.getNMS().isConcretePowder(b.getType())) {

                LasersColor concreteColor = NMSManager.getNMS().getColorFromBlock(b);

                LasersColor reflectedColor;
                if (concreteColor == LasersColor.WHITE) {
                    reflectedColor = color.filterBy(concreteColor).get(LasersColor.FilterResult.PASS_THROUGH);
                } else {
                    reflectedColor = color.filterBy(concreteColor).get(LasersColor.FilterResult.REFLECTED);
                }
                if (reflectedColor == null) {
                    return false;
                }

                ReflectionData inputRD = new ReflectionData(direction, location);
                ReflectionData outputRD = area.getBlocksReflectionCache().get(inputRD);
                if (outputRD == null) {
                    outputRD = reflect(b, inputRD);
                    area.getBlocksReflectionCache().put(inputRD, outputRD);
                }
                if (outputRD.getReflexionResult() == ReflectionResultType.REFLECTED) {
                    if (!NMSManager.getNMS().isConcretePowder(outputRD.getLocation().getBlock().getType())) {
                        LaserParticle lp = new LaserParticle(outputRD.getLocation().clone().add(outputRD.getDirection()), outputRD.getDirection(), reflectedColor, area, lightLevel);
                        area.addLaserParticle(lp);
                    }
                }//else Should not happen
                return false;

            } else if (NMSManager.getNMS().isGlassOrStainedGlass(b.getType())) {
                LasersColor passThroughColor;
                LasersColor glassColor = NMSManager.getNMS().getColorFromBlock(b);
                HashMap<LasersColor.FilterResult, LasersColor> filterResult = color.filterBy(glassColor);
                if (glassColor == LasersColor.WHITE) {
                    passThroughColor = color;
                } else {
                    passThroughColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                }
                if (passThroughColor == null) {
                    return false;
                }
                color = passThroughColor;
                return true;
            }
            ParticleTryToHitBlockLEEvent blockHitEvent = new ParticleTryToHitBlockLEEvent(area, b, this);
            Bukkit.getServer().getPluginManager().callEvent(blockHitEvent);
            if (blockHitEvent.getIgnoreDefaults()) {
                return !blockHitEvent.isCancelled();
            } else if (blockHitEvent.isCancelled()) {
                return false;
            }
        }

        return false;
    }

    /**
     * Moves this particle
     */
    private void move() {
        location = location.add(direction);
        nbMovementSinceCreation++;
    }

    private boolean impact(boolean checkDamage) {
        boolean ret = false;
        if (AFFECT_PLAYERS || AFFECT_MOBS) {
            ArrayList<Entity> entities = new ArrayList(Arrays.asList(location.getChunk().getEntities()));
            area.getPlayersExceptSpectators().forEach(entities::add);

            for (Entity entity : (Iterable<Entity>) () -> entities.stream()
                    .filter((entity) -> !(entity.isInvulnerable() || !entity.isValid())).iterator()) {
                boolean entityAffected = false;
                if (AFFECT_PLAYERS && entity instanceof Player) {
                    GameMode gamemode = ((Player) entity).getGameMode();
                    if (gamemode != GameMode.CREATIVE && gamemode != GameMode.SPECTATOR) {
                        entityAffected = true;
                    }
                } else if (AFFECT_MOBS && entity instanceof LivingEntity) {
                    if (entity.getCustomName() == null || (!entity.getCustomName().equals(AArmorStandComponent.ARMORSTAND_CUSTOM_NAME))) {
                        entityAffected = true;
                    }
                }
                if (entityAffected) {
                    LivingEntity lentity = (LivingEntity) entity;
                    if (particleInsideHitbox(lentity)) {
                        ParticleTryToHitEntityLEEvent pHitEvent = new ParticleTryToHitEntityLEEvent(area, this, lentity, LASERS_BURNS_TICK_DURATION, LASERS_ADDITIONAL_DAMAGES, LASERS_KNOCKBACK_MULTIPLIER, ENTITIES_STOPS_LASERS);
                        Bukkit.getServer().getPluginManager().callEvent(pHitEvent);
                        ParticleImpactProcessor particleImpactProcessor = new ParticleImpactProcessor(this, lentity, pHitEvent);
                        ret = particleImpactProcessor.process(checkDamage);
                    }
                }
            }
        }
        return ret;
    }

    private boolean particleInsideHitbox(LivingEntity lentity) {
        return NMSManager.getNMS().isInsideEntityHitbox(lentity, location);
    }

    private void calculateTrajectory() {
        ArrayList<AttractionRepulsionSphere> affectingSpheres = area.getAffectingAttractionRepulsionSphere(location);
        Vector tmpModificationVector = null;
        for (AttractionRepulsionSphere affectingSphere : affectingSpheres) {
            if (tmpModificationVector == null) {
                tmpModificationVector = affectingSphere.getTrajectoryModicationVector(location, direction);
            } else {
                tmpModificationVector.add(affectingSphere.getTrajectoryModicationVector(location, direction));
            }
        }
        if (tmpModificationVector != null) {
            tmpModificationVector.multiply(1.0 / affectingSpheres.size());
            double currentLengthSquared = direction.lengthSquared();
            Vector oldTrajectory = direction;
            if (speed != currentLengthSquared) {
                oldTrajectory.normalize().multiply(Math.sqrt(speed));
            }
            Vector newTrajectory = new Vector(
                    (oldTrajectory.getX() + tmpModificationVector.getX()),
                    (oldTrajectory.getY() + tmpModificationVector.getY()),
                    (oldTrajectory.getZ() + tmpModificationVector.getZ())
            );
            speed = newTrajectory.lengthSquared();
            direction = new Direction(newTrajectory);
            if (nbMovementWhileAffectedBySphere > 2) {
                lastComponent = affectingSpheres.get(affectingSpheres.size() - 1);
            }
            nbMovementWhileAffectedBySphere++;
        } else {
            nbMovementWhileAffectedBySphere = 0;
        }
    }

    public boolean isDuplicate(LaserParticle particle) {
        //we do not compare justCreated / lastComponent / nbMovementWhileAffectedBySphere on purpose
        return !(area != particle.getArea() || color != particle.getColor() || !location.equals(particle.getLocation()) || !direction.equals(particle.getDirection()) || speed != particle.getSpeed());
    }
}
