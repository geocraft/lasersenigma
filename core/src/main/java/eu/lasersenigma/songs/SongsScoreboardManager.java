package eu.lasersenigma.songs;

import eu.lasersenigma.LasersEnigmaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.UUID;

public class SongsScoreboardManager {

    private static final String SOUND_BLOCKS_SCOREBOARD_ACTIVATED_CONFIG_PATH = "music_blocks_scoreboards";

    private static final int SCOREBOARD_DISAPPEARING_DELAY = 100;

    private static final String SCOREBOARD_TITLE = new StringBuilder(ChatColor.MAGIC.toString())
            .append(ChatColor.BLUE)
            .append("[")
            .append(ChatColor.AQUA)
            .append("] ")
            .append(ChatColor.WHITE)
            .append(ChatColor.BOLD)
            .append(" Songs ")
            .append(ChatColor.MAGIC)
            .append(ChatColor.AQUA)
            .append("[")
            .append(ChatColor.BLUE)
            .append("] ")
            .toString();
    private final HashMap<UUID, Scoreboard> playerOldScoreboard;
    private int scoreboardViewRange;
    private HashMap<UUID, Integer> playersScoreboardDisappearingTaskId;
    private boolean isActivated;

    private int songsListSize;

    private int maxSongNameLength;

    private String upSeparator;

    private String downSeparator;

    protected SongsScoreboardManager(int size, int maxSongNameLength) {
        reload(size, maxSongNameLength);
        playerOldScoreboard = new HashMap<>();
    }

    private int getScoreboardViewRange() {
        if (songsListSize < 3) {
            return 0;
        }
        if (songsListSize < 5) {
            return 1;
        }
        if (songsListSize < 7) {
            return 2;
        }
        return 3;
    }

    protected final void reload(int size, int maxSongNameLength) {
        songsListSize = size;
        isActivated = LasersEnigmaPlugin.getInstance().getConfig().getBoolean(SOUND_BLOCKS_SCOREBOARD_ACTIVATED_CONFIG_PATH, true);
        scoreboardViewRange = getScoreboardViewRange();
        playersScoreboardDisappearingTaskId = new HashMap<>();
        this.maxSongNameLength = maxSongNameLength <= 40 ? maxSongNameLength : 40;
        generateSeparators();
    }

    protected void showScoreboard(int index, Player player) {
        if (!isActivated) {
            return;
        }
        Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
        final Objective objective = sb.registerNewObjective("le_snd_sb" + index, "dummy");

        SongsListManager songsList = SongsListManager.getInstance();

        final int middleScoreValue = 5;
        int upIndex = index;
        int upScore = middleScoreValue;
        int downIndex = index;
        int downScore = middleScoreValue;
        objective.getScore(truncateScoreName("> " + ChatColor.GOLD + ChatColor.BOLD + songsList.getSongFileName(index))).setScore(middleScoreValue);
        for (int i = 0; i < scoreboardViewRange; ++i) {
            upIndex--;
            upScore++;
            downIndex++;
            downScore--;
            if (downIndex >= songsListSize) {
                downIndex = 0;
            }
            if (upIndex == -1) {
                upIndex = songsListSize - 1;
            }
            objective.getScore(truncateScoreName(songsList.getSongFileName(upIndex))).setScore(upScore);
            objective.getScore(truncateScoreName(songsList.getSongFileName(downIndex))).setScore(downScore);
        }
        upScore++;
        downScore--;
        objective.getScore(upSeparator).setScore(upScore);
        objective.getScore(downSeparator).setScore(downScore);
        objective.setDisplayName(SCOREBOARD_TITLE);
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        Scoreboard oldScoreboard = playerOldScoreboard.get(player.getUniqueId());
        if (oldScoreboard == null) {
            playerOldScoreboard.put(player.getUniqueId(), player.getScoreboard());
        }

        player.setScoreboard(sb);
        Integer taskId = playersScoreboardDisappearingTaskId.get(player.getUniqueId());
        if (taskId != null) {
            Bukkit.getScheduler().cancelTask(taskId);
        }
        taskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            Scoreboard prevScoreboard = playerOldScoreboard.get(player.getUniqueId());
            if (prevScoreboard == null) {
                player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
            } else {
                player.setScoreboard(prevScoreboard);
            }

            playersScoreboardDisappearingTaskId.remove(player.getUniqueId());
        }, SCOREBOARD_DISAPPEARING_DELAY).getTaskId();
        playersScoreboardDisappearingTaskId.put(player.getUniqueId(), taskId);
    }

    private String truncateScoreName(String scoreName) {
        int length = scoreName.length();
        if (length > maxSongNameLength) {
            return scoreName.substring(0, maxSongNameLength);
        } else {
            int nbCharToAdd = maxSongNameLength - length;
            StringBuilder sb = new StringBuilder(scoreName);
            for (int i = 0; i < nbCharToAdd; ++i) {
                sb.append(" ");
            }
            return sb.toString();
        }
    }

    private void generateSeparators() {
        String upSeparatorChar = "=";
        StringBuilder upSB = new StringBuilder();
        String downSeparatorChar = "-";
        StringBuilder downSB = new StringBuilder();

        for (int i = 0; i < maxSongNameLength; ++i) {
            downSB.append(downSeparatorChar);
            upSB.append(upSeparatorChar);
        }

        this.downSeparator = downSB.toString();
        this.upSeparator = upSB.toString();
    }
}
