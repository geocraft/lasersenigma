package eu.lasersenigma.events.laserparticles;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ALaserParticleLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.particles.LaserParticle;

public class ParticleTryToHitComponentLEEvent extends ALaserParticleLEEvent implements IComponentLEEvent {

    private final IComponent component;

    public ParticleTryToHitComponentLEEvent(Area area, LaserParticle particle, IComponent component) {
        super(particle, area);
        this.component = component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
