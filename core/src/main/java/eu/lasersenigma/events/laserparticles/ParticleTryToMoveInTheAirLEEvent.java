package eu.lasersenigma.events.laserparticles;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.ALaserParticleLEEvent;
import eu.lasersenigma.particles.LaserParticle;

public class ParticleTryToMoveInTheAirLEEvent extends ALaserParticleLEEvent {

    public ParticleTryToMoveInTheAirLEEvent(Area area, LaserParticle particle) {
        super(particle, area);
    }

}
