package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;

public class NBRedstoneSensorChangeLEEvent extends AAfterActionLEEvent implements IAreaLEEvent {

    private final Area area;

    private final int oldNbActivatedLaserReceivers;

    private final int newNbActivatedLaserReceivers;

    public NBRedstoneSensorChangeLEEvent(Area area,
                                         int oldNbActivatedLaserReceivers, int newNbActivatedLaserReceivers) {
        super();
        this.area = area;
        this.oldNbActivatedLaserReceivers = oldNbActivatedLaserReceivers;
        this.newNbActivatedLaserReceivers = newNbActivatedLaserReceivers;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public int getOldNbActivatedLaserReceivers() {
        return oldNbActivatedLaserReceivers;
    }

    public int getNewNbActivatedLaserReceivers() {
        return newNbActivatedLaserReceivers;
    }

}
