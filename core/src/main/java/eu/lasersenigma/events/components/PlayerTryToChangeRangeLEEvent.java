package eu.lasersenigma.events.components;

import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IDetectionComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeRangeLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final IDetectionComponent component;

    private final int minDiff;

    private final int maxDiff;

    public PlayerTryToChangeRangeLEEvent(Player player, IDetectionComponent component, int minDiff, int maxDiff) {
        super();
        this.player = player;
        this.component = component;
        this.minDiff = minDiff;
        this.maxDiff = maxDiff;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IDetectionComponent getRangeDetectionComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public int getMinDiff() {
        return minDiff;
    }

    public int getMaxDiff() {
        return maxDiff;
    }

}
