package eu.lasersenigma.events.components;

import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.ILightComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeLightLevelLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final ILightComponent component;

    private final int newValue;

    public PlayerTryToChangeLightLevelLEEvent(Player player, ILightComponent component, int newValue) {
        super();
        this.player = player;
        this.component = component;
        this.newValue = newValue;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public ILightComponent getRangeDetectionComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public int getNewValue() {
        return newValue;
    }

}
