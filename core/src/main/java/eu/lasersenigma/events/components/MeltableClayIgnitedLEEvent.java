package eu.lasersenigma.events.components;

import eu.lasersenigma.components.MeltableClayBlock;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;

public class MeltableClayIgnitedLEEvent extends AAfterActionLEEvent implements IComponentLEEvent {

    private final MeltableClayBlock component;

    public MeltableClayIgnitedLEEvent(MeltableClayBlock component) {
        super();
        this.component = component;
    }

    @Override
    public MeltableClayBlock getComponent() {
        return component;
    }

}
