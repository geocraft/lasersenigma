package eu.lasersenigma.events.components;

import eu.lasersenigma.components.Elevator;
import eu.lasersenigma.components.attributes.ElevatorDirection;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToMoveElevatorLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final Elevator component;

    private final ElevatorDirection direction;

    public PlayerTryToMoveElevatorLEEvent(Player player, Elevator component, ElevatorDirection direction) {
        super();
        this.player = player;
        this.component = component;
        this.direction = direction;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public Elevator getElevator() {
        return component;
    }

    public ElevatorDirection getDirection() {
        return direction;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
