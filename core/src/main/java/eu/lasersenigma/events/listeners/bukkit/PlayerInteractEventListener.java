package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.logging.Level;

/**
 * EventListener for PlayerInteractEvent
 */
public class PlayerInteractEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerInteractEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each PlayerInteractEvent
     *
     * @param e a PlayerInteractEvent
     */
    @EventHandler(ignoreCancelled = false)
    public void onPlayerInteractEvent(PlayerInteractEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerInteractEvent");
        Player p = e.getPlayer();
        Action action = e.getAction();
        boolean shouldBeCancelled = false;
        if (Action.LEFT_CLICK_AIR == action || Action.LEFT_CLICK_BLOCK == action) {
            shouldBeCancelled = LEPlayers.getInstance().findLEPlayer(p).onLeftClick();
        } else if (Action.RIGHT_CLICK_AIR == action || Action.RIGHT_CLICK_BLOCK == action) {
            shouldBeCancelled = LEPlayers.getInstance().findLEPlayer(p).onRightClick();
        }
        if (!e.isCancelled()) {
            e.setCancelled(shouldBeCancelled);
        }
        if (e.getAction() == Action.PHYSICAL && e.getClickedBlock() != null) {
            Location loc = e.getClickedBlock().getLocation();
            Area area = Areas.getInstance().getAreaFromLocation(loc);
            if (area != null) {
                area.updateRedstone();
            }
        }
    }

}
