package eu.lasersenigma.events.listeners.bukkit.checkpoint;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.config.GetBackToCheckpointOnJoin;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class CheckpointPlayerJoinEventListener implements Listener {

    private final GetBackToCheckpointOnJoin backToCheckpointOnJoin;

    public CheckpointPlayerJoinEventListener(GetBackToCheckpointOnJoin backToCheckpointOnJoin) {
        this.backToCheckpointOnJoin = backToCheckpointOnJoin;
    }

    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
            Location checkpoint = lePlayer.getOnJoinCheckpoint(e.getPlayer().getLocation().getWorld(), backToCheckpointOnJoin);
            if (checkpoint == null) {
                return;
            }
            lePlayer.getBukkitPlayer().teleport(checkpoint, PlayerTeleportEvent.TeleportCause.PLUGIN);
        }, 1);
    }

}
