package eu.lasersenigma.events.parents;

import eu.lasersenigma.particles.LaserParticle;

public interface IParticleLEEvent {

    public LaserParticle getLaserParticle();

}
