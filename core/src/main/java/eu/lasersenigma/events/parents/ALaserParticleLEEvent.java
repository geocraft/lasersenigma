package eu.lasersenigma.events.parents;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.particles.LaserParticle;

public abstract class ALaserParticleLEEvent extends ABeforeActionLEEvent implements IParticleLEEvent, IAreaLEEvent {

    private final LaserParticle particle;

    private final Area area;

    private boolean ignoreDefaults = false;

    public ALaserParticleLEEvent(LaserParticle particle, Area area) {
        super();
        this.area = area;
        this.particle = particle;
    }

    public final boolean getIgnoreDefaults() {
        return ignoreDefaults;
    }

    public final void setIgnoreDefaults(boolean ignoreDefaults) {
        this.ignoreDefaults = ignoreDefaults;
    }

    @Override
    public final LaserParticle getLaserParticle() {
        return particle;
    }

    @Override
    public final Area getArea() {
        return area;
    }

}
