package eu.lasersenigma.events.parents;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public abstract class ACommandLEEvent extends ABeforeActionPermissionLEEvent implements ICommandLEEvent {

    private final CommandSender sender;

    private final Command cmd;

    private final String label;

    private final String[] args;

    public ACommandLEEvent(CommandSender sender, Command cmd, String label, String[] args) {
        super();
        this.sender = sender;
        this.cmd = cmd;
        this.label = label;
        this.args = args;
    }

    @Override
    public CommandSender getCommandSender() {
        return sender;
    }

    @Override
    public Command getCommand() {
        return cmd;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String[] getArgs() {
        return args;
    }

}
