# Welcome to Lasers Enigma Minecraft plugin !

[![Lasers Enigma banner](http://lasers-enigma.eu/wp-content/uploads/2018/08/logo-line-02.jpg)](https://lasers-enigma.eu)

## Introduction

This is a minecraft bukkit plugin (works on spigot) made to create, solve and share puzzles based on light.

For a start, create a laser sender, redirect the light flow using mirrors and win by sending it into a laser receiver !
Colors and 3D rotation are available.
This is the basis, but I highly suggest you to try and see how huge the possibilities are.
This plugin will allow you to create simple puzzles to difficult and complex ones!

## Download

> To download the **latest version**, go to [our website download page](https://lasers-enigma.eu/en/the-plugin/download/) or simply follow [this link](https://lasers-enigma.eu/download-latest.php).
> 
> You can also download **archived or beta versions** from [our artifactory](https://repository.lasers-enigma.eu/r/eu/lasersenigma/LasersEnigma/).


## Installation

* Download [the plugin jar file](https://lasers-enigma.eu/en/the-plugin/download/).
* Download the following required dependencies :
  * [NoteBlockAPI](https://www.spigotmc.org/resources/noteblockapi.19287/) - used for music blocks
  * [WorldEdit](https://dev.bukkit.org/projects/worldedit/files) - used for /lasers copy/paste/load/save
  * [LightAPI](https://www.spigotmc.org/resources/lightapi-fork.48247/) - used to allow lasers to illuminate places
* If you wish, download the following optional dependencies :
  * [DungeonsXL](https://erethon.de/repo/de/erethon/dungeonsxl/dungeonsxl-dist/)
* Simply put the jar files into the "plugin" folder of your server.
* Restart your server.
* (Optional) modify the [configuration](http://wiki.lasers-enigma.eu/Config.yml) in order to fit your needs and restart your server again.
* (Optional) add the [permissions](http://wiki.lasers-enigma.eu/Permissions) to the player groups inside the permission plugin you use.

## Commands

### /lasers

 Gives you all the items you will need to create enigms.
 In addition to your shortcut bar items, you will have inventory switcher items in your survival inventory.
* **permission:** `lasers.edit`

#### /lasers \<copy|paste|load [schematic-file-name]|save [schematic-file-name]>
- `/lasers <copy|paste>`: copy and paste areas you selected using the worldedit //wand.
- `/lasers <save|load> <schematic-file-name>`: save or load a schematic as/from a file.leschem placed inside the plugin folder. The schematic will be pushed inside the clipboard (so you need to use copy before save and paste after load commands).

* **permission:** `lasers.admin`

### /lasersreload

Reloads the configuration files
* **permission:** `lasers.admin`

## Permissions

|Permission      |Commands                                                     |
|----------------|-------------------------------------------------------------|
|`lasers.*`      | All commands                                                |
|`lasers.edit`   |`/lasers`                                                    |
|`lasers.admin`  |`/lasersreload /lasers <...> [...]`   |

## Documentation

[**Wiki**](http://wiki.lasers-enigma.eu/Main_Page)

## Use the API

* [Getting started](http://wiki.lasers-enigma.eu/Getting_started)
* [Javadoc](https://lasers-enigma.eu/javadoc)
* [API usage example](https://github.com/lasers-enigma/sample-api-usage)

## Contributing

* [Contributing](http://wiki.lasers-enigma.eu/Contributing)

## License

This plugin is open source (licensed under the Creative Commons Attribution-NonCommercial 4.0 International License).

Feel free to use, share, modify, share modifications, ... ! If you don't mind, telling me what your doing with it would please me but that's really optionnal.

The only restrictions is that you have to quote the author and not use it for commercial uses (You can still have a shop on your server but people who pay must not have more abilities related to this plugin than people who don't pay).

This is a human-readable summary of (and not a substitute for) the license.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

## Website

* [Lasers-enigma.eu](http://lasers-enigma.eu)

## Author note

Hey, I'm Benjamin (alias bZx, or bZx_ on minecraft). I'm proud and happy to share this plugin with you. I hope you will enjoy using it!
I'm far from being rich, but I still don't want to make you pay to use this plugin.
So if you want to pay me a drink or donate towards the plugin, that would help support the development of this plugin and would be highly appreciated!
Have a beautiful day!

## Donate

* [Patreon](https://www.patreon.com/bZx)

## Contact & Support

* Discord: [Discord](https://discord.gg/CdgWPPk) <=== Use it before contacting me by mail !
* Mail:    [contact@lasers-enigma.eu](mailto:contact@lasers-enigma.eu)

## Links

* [GIT Repository](https://gitlab.com/lasersenigma/lasersenigma)
* [plugin download](https://repository.lasers-enigma.eu/r/eu/lasersenigma/LasersEnigma/)
* [Twitter](https://twitter.com/LasersEnigma)
* [Facebook](https://www.facebook.com/Lasers-Enigma-860972544104940/)